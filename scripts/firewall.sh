#!/bin/bash
#

sudo ufw disable
sudo ufw reset
sudo ufw logging low
sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow ssh
sudo ufw allow 9000/tcp
sudo ufw allow 8888/udp
sudo ufw allow 7/tcp
sudo ufw allow out 53/tcp
sudo ufw allow out 53,67,68/udp
sudo ufw allow out 8888/udp
sudo ufw allow netbios-ns
sudo ufw allow netbios-dgm
sudo ufw allow netbios-ssn
sudo ufw allow 10000/tcp
sudo ufw enable
sudo ufw status verbose
