create table progress_status (
  id				        varchar(255) not null,
  file_name		            clob,
  percentage				double,
  total_file_count          bigint,
  current_file_count        bigint,
  log	                    clob,
  finished					int,
  error						int,
  error_stack				clob,
  constraint pk_progress primary key (id))
;
