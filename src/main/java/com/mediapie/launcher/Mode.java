package com.mediapie.launcher;

public enum Mode {
	SERVER,
	CLIENT,
	CLIENT_SERVER
}
