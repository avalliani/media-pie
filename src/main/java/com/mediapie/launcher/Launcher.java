package com.mediapie.launcher;

import java.util.*;
import java.net.*;
import java.io.*;
import org.apache.commons.lang3.math.NumberUtils;
import com.mediapie.util.*;
import com.mediapie.server.RootService;
import com.mediapie.server.RootServiceImpl;
import com.mediapie.client.ui.ServiceHelper;
import com.mediapie.client.ui.MainUI;
import com.mediapie.server.Persistance;
import com.mediapie.server.Server;
import com.mediapie.server.ProcessScheduler;
import com.mediapie.server.LDAPUserManager;
import com.mediapie.server.LinuxUserManager;
import com.mediapie.server.DatabaseUserManager;
import com.mediapie.server.UserManagerFactory;
import com.mediapie.discovery.DiscoveryServer;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

public class Launcher {
	static org.apache.commons.logging.Log log = Logger.getLog(Launcher.class);
	
	static Mode launcherMode;
			
	public Launcher(Mode mode) {
		launcherMode = mode;
		
		switch(mode) {
			case CLIENT:
			{
				URL logUrl = getClass().getResource("/logs/client/log4j.xml");
				DOMConfigurator.configure(logUrl);
				
				if (log.isInfoEnabled())
					log.info("Launching User Interface");

				Persistance.configureEbean(mode);
				ProcessScheduler.startScheduler();				
				
				ServiceHelper.setImplementation(new RootServiceImpl());
				(new MainUI()).main(null);
				break;
			}
			case SERVER:
			{				
				URL logUrl = getClass().getResource("/logs/server/log4j.xml");
				DOMConfigurator.configure(logUrl);
				
				if (log.isInfoEnabled())
					log.info("Launching Server");
				
				Persistance.configureEbean(mode);
				ProcessScheduler.startScheduler();
				
				new Server();
				new DiscoveryServer();
				
				break;
			}
		}
	}

	public static Mode getMode() {
		return launcherMode;
	}
	
	public static void main(String[] args) {
		AuthMode authMode = null;
		
		if (args == null || args.length == 0) {
			new Launcher(Mode.CLIENT);
		} else if (args.length > 2) {
			log.error("Required one or two parameter only. Mode: 1-> SERVER, 2-> CLIENT, 3-> CLIENT_SERVER and authentication: 1->OS or 2->DB");
		} else {
			//check for auth mode
			if (args.length > 1) {
				String authModeString = args[1];
				if (NumberUtils.isNumber(authModeString)) {
					Integer authModeNumber = NumberUtils.createInteger(authModeString);
					if (authModeNumber != 1 && authModeNumber != 2) {
						log.error("Authentication Mode has to be 1-> OS, 2-> DB");
						System.exit(-1);
					} else {
						switch(authModeNumber) {
							case 1:
							{
								authMode = AuthMode.OS_LINUX;
								UserManagerFactory.setInstance(new LDAPUserManager());								
								break;
							}
							case 2:
							{
								authMode = AuthMode.DB;
								UserManagerFactory.setInstance(new DatabaseUserManager());								
								break;
							}									
						}												
					}
				} else {
					log.error("Not a valid auth number. Required 1->OS or 2->DB");
				}
			}

			//check for client/server mode
			String modeString = args[0];
			if (NumberUtils.isNumber(modeString)) {
				Integer mode = NumberUtils.createInteger(modeString);
				if (mode > 3) {
					log.error("Mode cannot only be 1-> SERVER, 2-> CLIENT, 3-> CLIENT_SERVER");
				} else {
					switch(mode) {
						case 1:
						{
							new Launcher(Mode.SERVER);
							break;
						}
						case 2:
						{
							new Launcher(Mode.CLIENT);
							break;
						}
						case 3:
						{
							new Launcher(Mode.CLIENT_SERVER);
							break;
						}
					}					
				}
			} else {
				log.error("Not a valid number");
			}
		}		
	}
}
