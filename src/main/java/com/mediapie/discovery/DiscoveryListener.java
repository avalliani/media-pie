package com.mediapie.discovery;

import java.util.*;

public interface DiscoveryListener {
	public void receivedPing(final DiscoverySettings server);
}