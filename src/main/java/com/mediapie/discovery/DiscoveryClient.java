package com.mediapie.discovery;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import javax.xml.ws.Endpoint;
import org.apache.cxf.jaxws.*;
import org.apache.cxf.interceptor.*;

public class DiscoveryClient {
	static org.apache.commons.logging.Log log = Logger.getLog(DiscoveryClient.class);
	MulticastSocket socket;
	public static int connectTimeout = 60000;
	public static int discoveryTimeout = 60000;
	List<DiscoverySettings> servers = new ArrayList<DiscoverySettings>();
	DiscoveryListener listener = null;
	
	public DiscoveryClient() {
		this(null);
	}
	
	public DiscoveryClient(DiscoveryListener listener) {
		try {
			this.listener = listener;
			socket = new MulticastSocket();
			socket.setSoTimeout(discoveryTimeout);
			InetAddress group = InetAddress.getByName (DiscoveryServer.MULTI_GROUP);
			socket.joinGroup (group);			
		} catch(SocketException se) {
			throw new MediaPieException(se);
		} catch(IOException ie) {
			throw new MediaPieException(ie);
		}
	}

	public List<DiscoverySettings> getServers() { return servers;}
	
	public void discover() {
		try {
			byte[] sendBytes = (DiscoveryUtils.wrap(DiscoveryServer.PING)).getBytes();

			if (log.isInfoEnabled())
				log.info("Discovering server.. sending " + new String(sendBytes));

			DatagramPacket sendGram = new DatagramPacket(
				sendBytes,
				sendBytes.length,
				InetAddress.getByName("255.255.255.255"),
				DiscoveryServer.MULTI_PORT);
			socket.send(sendGram);

			//multiple servers will send responses here... will timeout
			//shortly
			
			while(true) {
				byte[] receiveBytes = new byte[100];
				DatagramPacket datagramPacket = new DatagramPacket(receiveBytes, receiveBytes.length);
				socket.receive(datagramPacket);
				byte[] received = datagramPacket.getData();

				String raw = new String(received, "UTF-8");
				String value = DiscoveryUtils.unwrap(raw);

				if (log.isInfoEnabled()) {
					log.info("Received Multicast Request: " + value +
							 " Raw " + raw +
							 " from " + datagramPacket.getAddress().getHostAddress());
				}

				if (!DiscoveryServer.UNKNOWN.equals(value)) {
					DiscoverySettings settings = (DiscoverySettings) JsonUtils.from(value, DiscoverySettings.class);
					List<String> ips = settings.getIps();

					for (String ip : ips) {
						boolean reachable = InetAddress.getByName(ip).isReachable(connectTimeout);
						if (reachable) {
							settings.setIp(ip);
							servers.add(settings);
							if (listener != null)
								listener.receivedPing(settings);
							break;
						}
					}
				} else {
					log.error("Unknown message received");
				}
			}
			
		} catch(SocketTimeoutException  ste) {
			throw new MediaPieException(ste);
		} catch(UnknownHostException uhe) {
			throw new MediaPieException(uhe);
		} catch(IOException ie) {
			throw new MediaPieException(ie);
		}
	}
}

