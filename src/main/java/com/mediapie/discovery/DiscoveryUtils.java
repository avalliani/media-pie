package com.mediapie.discovery;

import java.util.*;

public class DiscoveryUtils {

	static String startToken = "<message>";
	static String endToken = "</message>";
	public static String wrap(String message) {
		return startToken + message + endToken;
	}

	public static String unwrap(String message) {
		if (message == null) return null;
		
		int start = message.indexOf(startToken);
		int end = message.indexOf(endToken);

		if(start < 0 || end < 0 || end < start)
			return null;

		String val = message.substring(start + startToken.length(), end);
		
		return val;
	}
	
}