package com.mediapie.discovery;

import java.util.*;

public class DiscoverySettings implements java.io.Serializable {
	List<String> ips;
	String ip;
	int port;
	String name;
	
	public DiscoverySettings(String name, int port, List<String> ips) {
		setPort(port);
		setIps(ips);
		setName(name);
	}
	
	public List<String> getIps() {return ips;}
	public void setIps(List<String> ips) {this.ips = ips;}
	
	public int getPort() {return port;}
	public void setPort(int port) {this.port = port;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	public String getIp() {return ip;}
	public void setIp(String ip) {this.ip = ip;}

	public String toString() {
		return name + "->" + ip + ":" + port;
	}
}