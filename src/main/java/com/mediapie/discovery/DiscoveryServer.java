package com.mediapie.discovery;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.server.Server;
import javax.xml.ws.Endpoint;
import org.apache.cxf.jaxws.*;
import org.apache.cxf.interceptor.*;

public class DiscoveryServer extends Thread {
	static org.apache.commons.logging.Log log = Logger.getLog(DiscoveryServer.class);

	public static int MULTI_PORT = 8888;
	public static int port = 9000;
	public static String PING = "mediapie-getaddress";
	public static String UNKNOWN = "UNKNOWN";
	public static String MULTI_GROUP = "239.255.42.99";
	
	public DiscoveryServer() {
		super("Discovery Server");

		port = Server.getPort();
		
		if (log.isInfoEnabled())
			log.info("Starting Discovery Server");

		MulticastSocket s = null;

		try {
			s = new MulticastSocket (MULTI_PORT);

			InetAddress group = InetAddress.getByName (MULTI_GROUP);
			s.joinGroup (group);			
		} catch(SocketException e) {
			log.error(e);
			System.exit(-1);
		} catch(IOException ie) {
			log.error(ie);
			System.exit(-1);
		}

		while(true) {
			try {
				byte[] receiveBytes = new byte[100];
				DatagramPacket receiveDatagram = new DatagramPacket(receiveBytes, receiveBytes.length);
				s.receive(receiveDatagram);
			
				byte[] received = receiveDatagram.getData();
				String raw = new String(received, "UTF-8");
				String value = DiscoveryUtils.unwrap(raw);
				if (log.isInfoEnabled()) {
					log.info("Received Multicast Request: " + value +
							 " Raw " + raw +
							 " from " + receiveDatagram.getAddress().getHostAddress());
				}
				
				if (value.equals(PING)) {
					List<String> ips = IPUtils.getIPAddresses();
					String name = NetworkUtils.getHostName();
					DiscoverySettings settings = new DiscoverySettings(name, port, ips);
					String json = JsonUtils.to(settings);
					if (log.isInfoEnabled())
						log.info("Sending server ips: " + json);
					byte[] sendBytes = (DiscoveryUtils.wrap(json)).getBytes();
					DatagramPacket datagramPacket = new DatagramPacket(sendBytes, sendBytes.length, receiveDatagram.getAddress(), receiveDatagram.getPort());
					s.send(datagramPacket);
				} else {
					if (log.isInfoEnabled())
						log.info("Sending unknown request: " + UNKNOWN);
					byte[] sendBytes = (DiscoveryUtils.wrap(UNKNOWN)).getBytes();
					DatagramPacket datagramPacket = new DatagramPacket(sendBytes, sendBytes.length, receiveDatagram.getAddress(), receiveDatagram.getPort());
					s.send(datagramPacket);					
				}				
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

	public void run()  {
		new DiscoveryServer();
	}	
}

