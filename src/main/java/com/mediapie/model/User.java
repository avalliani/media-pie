package com.mediapie.model;

import com.avaje.ebean.*;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;
import static java.sql.Types.*;


/**
 * Managed by Ebean
 */
@Entity
public class User
{
	@Id
	String id;

	@Lob		
	String password;

	public String getId() { return id;}
	public void setId(String id) { this.id = id;}

	public String getPassword() { return password;}
	public void setPassword(String password) { this.password = password;}
}
