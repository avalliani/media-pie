package com.mediapie.model;

import com.avaje.ebean.*;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;
import static java.sql.Types.*;


/**
 * Managed by Ebean
 */
@Entity
public class Configuration
{
	@Id
	String id;
	
	String guid;

	String accessKey;

	String secretKey;

	Integer frequency;	
			
	@Lob	
	String encryptionKey;

	String targetDirectory;

	String sourceDirectory;
	
	public String getId() { return id;}
	
	public String getGuid() { return guid;}

	public String getAccessKey() { return accessKey;}

	public String getSecretKey() { return secretKey;}

	public String getEncryptionKey() { return encryptionKey;}

	public String getTargetDirectory() { return targetDirectory;}

	public String getSourceDirectory() { return sourceDirectory;}
	
	public void setId(String id) { this.id = id;}

	public void setGuid(String guid) { this.guid = guid;}

	public void setAccessKey(String accessKey) { this.accessKey = accessKey;}

	public void setSecretKey(String secretKey) { this.secretKey = secretKey;}
	
	public void setEncryptionKey(String encryptionKey) { this.encryptionKey = encryptionKey;}

	public void setTargetDirectory(String targetDirectory) { this.targetDirectory = targetDirectory;}

	public void setSourceDirectory(String sourceDirectory) { this.sourceDirectory = sourceDirectory;}

	public Integer getFrequency() {return frequency;}

	public void setFrequency(Integer frequency) {this.frequency = frequency;}
}
