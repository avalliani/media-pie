package com.mediapie.model;

import java.io.*;
import java.util.*;

public enum Schedule {
	NEVER (0, "Never"),
	RUN_ONCE(1, "Run Once"),
	EVERY_ONE_HOUR (2, "Every 1 Hour"),
	EVERY_MORNING (3, "On Morning Every Day"),
	EVERY_MIDNIGHT (4, "On Mid-Night Every Day"),
	EVERY_WEEKEND (5, "Every Weekend");

	private final int scheduleValue;
	private final String scheduleText;
	
	Schedule(int value, String text) {
		this.scheduleValue = value;
		this.scheduleText = text;
	}

	public String toString() {
		return scheduleText;
	}

	public Integer getValue() {
		return scheduleValue;
	}
	
	public static Schedule byValue(Integer value) {
		if (value == null) return null;
		for(Schedule schedule : values()) {
			if (schedule.getValue().equals(value))
				return schedule;
		}

		return null;
	}
}

