package com.mediapie.model;

import com.avaje.ebean.*;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.*;
import javax.persistence.*;
import static java.sql.Types.*;


/**
 * Managed by Ebean
 */
@Entity
public class Directory
{
	@Id
	Integer id;
	
	String name;

	String cloudName;
	
	@Version
	Long version;
	
	public Integer getId() { return id;}
	
	public String getName() { return name;}

	public String getCloudName() { return cloudName;}
	
	public Long getVersion() { return version;}

	
	public void setId(Integer id) { this.id = id;}

	public void setName(String name) { this.name = name;}

	public void setCloudName(String cloudName) { this.cloudName = cloudName;}
	
	public void setVersion(Long version) { this.version = version;}

}
