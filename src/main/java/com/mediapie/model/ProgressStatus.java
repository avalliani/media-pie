package com.mediapie.model;

import com.avaje.ebean.*;
import java.util.*;


public class ProgressStatus
{
	String id;

	Double percentage;
	
	String fileName;

	String log;
	
	Integer totalFileCount;
	
	Integer currentFileCount;
	
	Boolean finished;

	Boolean error;

	String errorStack;

	String errorMessage;
	
	public String getId() {return id;}
	public void setId(String id) {this.id = id;}

	public Double getPercentage() {return percentage;}
	public void setPercentage(Double percentage) {this.percentage = percentage;}

	public String getFileName() {return fileName;}
	public void setFileName(String fileName) {this.fileName = fileName;}

	public Integer getTotalFileCount() {return totalFileCount;}
	public void setTotalFileCount(Integer totalFileCount) {this.totalFileCount = totalFileCount;}

	public Integer getCurrentFileCount() {return currentFileCount;}
	public void setCurrentFileCount(Integer currentFileCount) {this.currentFileCount = currentFileCount;}

	public Boolean getFinished() {return finished;}
	public void setFinished(Boolean finished) {this.finished = finished;}

	public String getLog() {return log;}
	public void setLog(String log) {this.log = log;}

	public Boolean getError() {return error;}
	public void setError(Boolean error) {this.error = error;}

	public String getErrorStack() {return errorStack;}
	public void setErrorStack(String errorStack) {this.errorStack = errorStack;}

	public String getErrorMessage() {return errorMessage;}
	public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
}