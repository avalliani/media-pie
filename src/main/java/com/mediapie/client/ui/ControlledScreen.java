package com.mediapie.client.ui;

public interface ControlledScreen {

	//This method will allow the injection of the Parent ScreenPane
	public void setScreenParent(ScreensController screenPage);
	public void setInitialFocus();	
	public void onDisplay();	
	public void onLoad();	
	public boolean isLoaded();	
}