package com.mediapie.client.ui;

import javafx.application.*;
import javafx.beans.value.*;
import javafx.scene.input.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.cloud.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class RestoreDirectoryController implements Initializable , ControlledScreen {

	static org.apache.commons.logging.Log log = Logger.getLog(RestoreDirectoryController.class);
	
	@FXML
	private Pane pContent;
	
	@FXML
	private TextField bucketName;
	
	@FXML
	private TextField accessKey;

	@FXML
	private TextField secretKey;

	@FXML
	private TextField encryptionKey;

	@FXML
	private TextField targetDirectory;
	
	@FXML
	private ComboBox awsDirectoryComboBox;
	
	ScreensController myController;

	private boolean listPulled = false;

	boolean isLoaded = false;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		try {
			ChangeListener listener = new ChangeListener<String>() {
				@Override
				public void changed(final ObservableValue<? extends String> observable, final String oldValue, final String newValue) {
					listPulled = false;
					awsDirectoryComboBox.getSelectionModel().clearSelection();
					awsDirectoryComboBox.getItems().clear();				
				}
			};		

			bucketName.textProperty().addListener(listener);
			accessKey.textProperty().addListener(listener);
			secretKey.textProperty().addListener(listener);
			encryptionKey.textProperty().addListener(listener);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	public void onDisplay() {
		initialLoad();
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
				accessKey.requestFocus();
			}
		});		
	}
	
	public String getBucketName() {
		return bucketName.getText();
	}
	
	public String getAccessKey() {
		return accessKey.getText();
	}

	public String getTargetDirectory() {
		return targetDirectory.getText();
	}
	
	public String getSecretKey() {
		return secretKey.getText();
	}

	public String getEncryptionKey() {
		return encryptionKey.getText();
	}

	public void clear() {
		bucketName.setText("");;
		accessKey.setText("");;
		secretKey.setText("");
		encryptionKey.setText("");
		targetDirectory.setText("");

		awsDirectoryComboBox.getSelectionModel().clearSelection();		
		awsDirectoryComboBox.setValue(null);						
	}
	
	public void initialLoad() {
		clear();

		new Thread() {
			public void run() {
				try {
					final Configuration config = ServiceHelper.getConfiguration();

					Platform.runLater(new Runnable() {
						public void run() {
							try {
								if (config != null) {
									bucketName.setText(config.getGuid());
									accessKey.setText(config.getAccessKey());
									secretKey.setText(config.getSecretKey());
									encryptionKey.setText(config.getEncryptionKey());
									targetDirectory.setText(config.getTargetDirectory());
								}
							} catch(Exception e) {
								DialogHelper.showUnknownExceptionAsync(this, e);
							}
						}
					});
					
				} catch(Exception e) {
					DialogHelper.showUnknownExceptionAsync(this, e);
				}							
			}
		}.start();						
	}

	@FXML
	private void clickDirectoryAction(MouseEvent event) {
		try {
			if (!listPulled) {
				validateAwsTokensAndPull();
			}
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	

	@FXML
	private void handleNextButtonAction(ActionEvent event) {
		try {
			boolean dir = checkRequiredTextField(targetDirectory);
			boolean bucketNameNotNull = checkRequiredTextField(bucketName);
			boolean sourceDirectoryValid = checkRequiredComboField(awsDirectoryComboBox);

			if (!sourceDirectoryValid) {
				setRed(awsDirectoryComboBox);
			}

			if (dir == false || !FileUtils.createDirectory(targetDirectory.getText())) {
				setRed(targetDirectory);
			} else if (sourceDirectoryValid && bucketNameNotNull) {
				removeRed(targetDirectory);
				removeRed(awsDirectoryComboBox);

				final String targetDirectoryText = targetDirectory.getText();
				final String cloudDirectoryText = awsDirectoryComboBox.getValue().toString();
				
				new Thread() {
					public void run() {
						saveAndCheckConnection(targetDirectoryText, cloudDirectoryText);
					}
				}.start();				
			}
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleCancelButtonAction(ActionEvent event) {		
		try {
			myController.setScreen(MainUI.screen1ID);		
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	
	@FXML
	private void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen1ID);		
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	void saveAndCheckConnection(final String targetDirectory, final String cloudDirectory) {
		ProgressPopup.show("Checking Connection ...");
		
		//save directory
		ServiceHelper.saveRestoreDirectories(targetDirectory,
											 cloudDirectory);

		try {
			ServiceHelper.checkConnection();
			ServiceHelper.startDownload();

			Platform.runLater(new Runnable() {
				public void run() {
					myController.setScreen(MainUI.screen2BID);		
				}
			});

			ProgressPopup.hide();			
		} catch(MediaPieException ce) {
			ProgressPopup.hide();			
			DialogHelper.showExceptionAsync("Connection Issue", "Error connecting to Amazon. Please check your keys.", ce);
		} catch(Exception e) {
			ProgressPopup.hide();			
			DialogHelper.showUnknownExceptionAsync(this, e);
		}
	}
	
	void validateAwsTokensAndPull() {
		boolean bucketNameNotNull = checkRequiredTextField(bucketName);
		boolean accessKeyNotNull = checkRequiredTextField(accessKey);
		boolean secretKeyNotNull = checkRequiredTextField(secretKey);
		boolean encryptionKeyNotNull = checkRequiredTextField(encryptionKey);

		if (bucketNameNotNull && accessKeyNotNull && secretKeyNotNull && encryptionKeyNotNull) {
			final String accessKeyText = accessKey.getText();
			final String secretKeyText = secretKey.getText();
			final String encryptionKeyText = encryptionKey.getText();
			final String bucketNameText = bucketName.getText();
			final ComboBox directories = awsDirectoryComboBox;
			
			new Thread("Pulling cloud folders") {
				public void run() {
					saveConnectAndPull(accessKeyText, secretKeyText, encryptionKeyText, bucketNameText, directories);
				}
			}.start();
		}
	}

	void saveConnectAndPull(final String accessKey,
						    final String secretKey,
						    final String encryptionKey,
						    final String bucketName,
					        final ComboBox directoryCombo) {

		ProgressPopup.show("Checking Connection ...");

		try {
			ServiceHelper.saveConfiguration(accessKey,
											secretKey,
											encryptionKey,
											bucketName);

			try {
				ServiceHelper.checkConnection();
				final List<String> directories = ServiceHelper.getCloudDirectories(4);

				Platform.runLater(new Runnable() {
					public void run() {
						directoryCombo.getItems().addAll(directories);
						listPulled = true;						
					}
				});
				
				ProgressPopup.hide();				

			} catch(MediaPieException ce) {
				ProgressPopup.hide();				
				DialogHelper.showExceptionAsync("Connection Issue", "Error connecting to Amazon. Please check your keys.", ce);
			}
		} catch(Exception e) {
			ProgressPopup.hide();			
			DialogHelper.showUnknownExceptionAsync(this, e);
		}

		ProgressPopup.hide();		
	}
	
	boolean checkRequiredTextField(TextField field) {
		String text = field.getText();
		if (text == null || "".equals(text.trim())) {
			setRed(field);
			return false;
		} else {
			removeRed(field);
				
			return true;
		}
	}

	boolean checkRequiredComboField(ComboBox field) {
		Object text = field.getValue();
		if (text == null || "".equals(text.toString().trim())) {
			setRed(field);
			return false;
		} else {
			removeRed(field);

			return true;
		}
	}
	
	private void setRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();

		if(!styleClass.contains("tferror")) {
			styleClass.add("tferror");
		}
	}

	private void setRed(ComboBox tf) {
		ObservableList<String> styleClass = tf.getStyleClass();

		if(!styleClass.contains("tferror")) {
			styleClass.add("tferror");
		}
	}
	
	private void removeRed(ComboBox tf) {
		ObservableList<String> styleClass = tf.getStyleClass();
		styleClass.removeAll(Collections.singleton("tferror"));
	}	

	private void removeRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();
		styleClass.removeAll(Collections.singleton("tferror"));
	}	
}