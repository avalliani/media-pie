package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.discovery.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class LoginController implements Initializable , ControlledScreen {
	
	static org.apache.commons.logging.Log log = Logger.getLog(LoginController.class);	

	@FXML
	protected ProgressBar progress;

	@FXML
	private CheckBox changePassword;
	
	@FXML
	private TextField username;

	@FXML
	private PasswordField password;

	
	ScreensController myController;

	boolean isLoaded = false;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void onDisplay() {
		//discover the clients if any
		(new Thread() {
			public void run() {
			}
		}).start();
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}
	
	
	@FXML
	private void handleLoginButtonAction(ActionEvent event) {
		boolean usernameNotNull = checkRequiredTextField(username);
		boolean passwordNotNull = checkRequiredTextField(password);

		if (usernameNotNull && passwordNotNull) {
			final String usernameValue = username.getText();
			final String passwordValue = password.getText();

			new Thread("Login") {
				public void run() {
					connect(usernameValue, passwordValue);
				}
			}.start();
		}
	}

	private void connect(final String username,
						 final String password) {

		ProgressPopup.show("Logging In ...");
		
		try {
			ServiceSettings.init(username, password);
			final boolean running = ServiceHelper.isRunning();

			Platform.runLater(new Runnable() {
				public void run() {
					if (changePassword.isSelected()) {
						myController.setScreen(MainUI.screen0DID);										
					} else {
						//check if process already running and then redirect
						if (running) {
							myController.setScreen(MainUI.screen3AID);					
						} else {
							myController.setScreen(MainUI.screen1ID);
						}
					}										
				}
			});					
		} catch(javax.xml.ws.soap.SOAPFaultException se) {
			ProgressPopup.hide();
			
			log.error("Error in logging in or initial connection:", se);

			javax.xml.soap.SOAPFault fault = se.getFault();
			String code = (fault != null) ? fault.getFaultCode() : "UNKNOWN";
			String detail = (fault != null) ? fault.getFaultString() : "UNKNOWN";
			if (detail.indexOf("The security token could not be authenticated or authorized") >= 0) {
				if (log.isDebugEnabled())
					log.debug("Found exception on login:" + detail);
				
				DialogHelper.showErrorAsync("Invalid Login",
									   "Invalid username and password",
									   "Please provide a correct username and password");
			} else {
				DialogHelper.showExceptionAsync("Unknow error logging in",
					detail,
					se);
			}							
		} catch(javax.xml.ws.WebServiceException ws) {
			ProgressPopup.hide();
			String message = ws.getMessage();

			if (message != null && message.indexOf("Could not send Message") >= 0) {
				DialogHelper.showErrorAsync("Connection Failed",
									   "Unable to connect",
									   "Please check your server connection");
			} else {
				DialogHelper.showUnknownExceptionAsync(this, ws);
			}			
		} catch(Exception e) {
			ProgressPopup.hide();			
			DialogHelper.showUnknownExceptionAsync(this, e);
		}

		ProgressPopup.hide();		
	}
	
	@FXML
	private void handleCancelButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen0AID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen0BID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	boolean checkRequiredTextField(TextField field) {
		String text = field.getText();
		if (text == null || "".equals(text.trim())) {
			setRed(field);
			return false;
		} else {
			removeRed(field);
			return true;
		}
	}

	private void setRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();

		if(!styleClass.contains("tferror")) {
			styleClass.add("tferror");
		}
	}


	private void removeRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();
		styleClass.removeAll(Collections.singleton("tferror"));
	}	
}