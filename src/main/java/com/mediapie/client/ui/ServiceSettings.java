package com.mediapie.client.ui;

import java.net.*;
import java.io.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.mediapie.util.*;
import com.mediapie.server.RootService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import javax.xml.ws.BindingProvider;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.bus.spring.SpringBusFactory;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JStaxOutInterceptor;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

public class ServiceSettings {
	static org.apache.commons.logging.Log log = Logger.getLog(ServiceSettings.class);
	static boolean initialized = false;
	
	private static final QName SERVICE_NAME 
			= new QName("http://server.mediapie.com/", "RootService");
	private static final QName PORT_NAME 
			= new QName("http://server.mediapie.com/", "RootServicePort");
	
	public static String SERVER_IP;
	public static int SERVER_PORT;
	public static String USERNAME;

	public static void reinit(final String username, final String password) {
		initialized = false;
		init(username, password);
	}

	public static void setInitialized(boolean val) {
		initialized = val;
	}
	
	public static void init(final String username, final String password) {
		
		if (initialized)
			return;
		
		SpringBusFactory bf = new SpringBusFactory();
		URL busFile = ServiceSettings.class.getResource("/cxf-client.xml");
		Bus bus = bf.createBus(busFile.toString());
		BusFactory.setDefaultBus(bus);
		
		Service service = Service.create(SERVICE_NAME);
		
		// Endpoint Address
		String endpointAddress = "https://" + SERVER_IP + ":" + SERVER_PORT + "/" + "RootService";

		if(log.isInfoEnabled())
			log.info("Setting endpoint:" + endpointAddress);
		
		// Add a port to the Service
		service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING, endpointAddress);
		
		RootService serviceImpl = service.getPort(RootService.class);

		//add the login auth
		Client client = ClientProxy.getClient(serviceImpl);
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("action", "UsernameToken");
		props.put("user", username);
		props.put("passwordType", "PasswordText");
		((BindingProvider)serviceImpl).getRequestContext().put("password", password);
		
		//props.put("passwordCallbackRef", new com.mediapie.client.ui.ClientPasswordCallback());
		WSS4JOutInterceptor wss4jOut = new WSS4JOutInterceptor(props);

		client.getOutInterceptors().add(wss4jOut);
		client.getOutInterceptors().add(new LoggingOutInterceptor());
		client.getInInterceptors().add(new LoggingInInterceptor());

		//placeholder... the service will reject based on headers
		serviceImpl.login(username, password);
		
		ServiceHelper.setImplementation(serviceImpl);

		USERNAME = username;
		initialized = true;		
	}	
	
}