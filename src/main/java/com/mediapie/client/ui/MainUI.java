package com.mediapie.client.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Screen;
import javafx.scene.Parent;
import javafx.stage.StageStyle;
import javafx.stage.Modality;
import javafx.fxml.FXMLLoader;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.animation.*;
import javafx.util.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

import com.mediapie.client.*;
import com.mediapie.util.*;

public class MainUI extends Application {
	static org.apache.commons.logging.Log log = Logger.getLog(MainUI.class);
	
	public static String screen0ID = "splash";
	public static String screen0File = "splash.fxml";

	public static String screen0AID = "detect";
	public static String screen0AFile = "detectServer.fxml";

	public static String screen0BID = "discoveryResults";
	public static String screen0BFile = "discoveryResults.fxml";

	public static String screen0CID = "login";
	public static String screen0CFile = "login.fxml";

	public static String screen0DID = "changePassword";
	public static String screen0DFile = "changePassword.fxml";
	
	public static String screen1ID = "backupOrRestore";
	public static String screen1File = "backupOrRestore.fxml";
	
	public static String screen1AID = "main";
	public static String screen1AFile = "awsInfo.fxml";
	
	public static String screen2AID = "directory";
	public static String screen2AFile = "directoryInfo.fxml";
	
	public static String screen3AID = "progress";
	public static String screen3AFile = "progressInfo.fxml";

	public static String screen1BID = "restore";
	public static String screen1BFile = "restoreDirectory.fxml";

	public static String screen2BID = "progressDownload";
	public static String screen2BFile = "downloadProgressInfo.fxml";
	public static Stage staticPrimaryStage = null;
	public static boolean runningInTray = false;
	
	public static void main(String[] args) {
		launch(args);
	}

	public void stop() {
		if (log.isInfoEnabled())
			log.info("Stopping Stage");
	}
	
	public void stopProcess() {
		if (log.isInfoEnabled())
			log.info("Stopping application");

		try {
			super.stop();
		} catch(Exception e) {
			log.error(e);
		}
	}

	public void exitProcess() {
		if (log.isInfoEnabled())
			log.info("Stopping application");

		try {
			super.stop();
		} catch(Exception e) {
			log.error(e);
		}

		System.exit(0);
	}

	public void openWindow() {
		Platform.runLater(new Runnable() {
			public void run() {
				staticPrimaryStage.show();
				//staticPrimaryStage.setIconified(false);
			}
		});		
	}

	public void hideWindow() {
		Platform.runLater(new Runnable() {
			public void run() {
				staticPrimaryStage.hide();
				//staticPrimaryStage.setIconified(true);
				runInSystemTray();													
			}
		});		
	}
	
	public static Stage getPrimaryStage() {
		return staticPrimaryStage;
	}
	
	public void start(final Stage primaryStage) {
		double monitorScreenWidth = Screen.getPrimary().getBounds().getWidth();
		double monitorScreenHeight = Screen.getPrimary().getBounds().getHeight();
		double scale =  monitorScreenWidth*1.0/monitorScreenHeight;
		final double screenWidth = 1019.0/2560 * monitorScreenWidth;
		final double screenHeight = 766.0/1600 * monitorScreenHeight;
		
		try {
			staticPrimaryStage = primaryStage;
			
			//configure stage actions
			Platform.setImplicitExit(false);

			primaryStage.setOnCloseRequest(new javafx.event.EventHandler<javafx.stage.WindowEvent>() {
				@Override
				public void handle(javafx.stage.WindowEvent event) {
					event.consume();
					hideWindow();
				}
			});
			
			final Stage stage = new Stage();
			SplashController splash = loadSplashScreen(stage);

			EventHandler onFinished = new EventHandler<ActionEvent>() {
				public void handle(ActionEvent t) {
					primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("app.png")));				
					primaryStage.initStyle(StageStyle.DECORATED);
					primaryStage.setResizable(false);

					ScreensController mainContainer = new ScreensController();

					//for discovery and remote login
					mainContainer.loadScreen(MainUI.screen0AID, MainUI.screen0AFile);
					mainContainer.loadScreen(MainUI.screen0BID, MainUI.screen0BFile);
					mainContainer.loadScreen(MainUI.screen0CID, MainUI.screen0CFile);
					mainContainer.loadScreen(MainUI.screen0DID, MainUI.screen0DFile);
					
					//for backup
					mainContainer.loadScreen(MainUI.screen1ID, MainUI.screen1File);
					mainContainer.loadScreen(MainUI.screen1AID, MainUI.screen1AFile);
					mainContainer.loadScreen(MainUI.screen2AID, MainUI.screen2AFile);
					mainContainer.loadScreen(MainUI.screen3AID, MainUI.screen3AFile);

					//for restore
					mainContainer.loadScreen(MainUI.screen1BID, MainUI.screen1BFile);
					mainContainer.loadScreen(MainUI.screen2BID, MainUI.screen2BFile);

					mainContainer.setScreen(MainUI.screen0AID);

					Group root = new Group();
					root.getChildren().addAll(mainContainer);
					Scene scene = new Scene(root);//, screenWidth, screenHeight);
					scene.getStylesheets().add(getClass().getResource("styling.css").toExternalForm());			
					primaryStage.setTitle("Media Pie");
					primaryStage.setScene(scene);
					primaryStage.show();
					stage.close();
				}
			};

			PauseTransition pauseTransition = new PauseTransition(Duration.millis(1000));
			FadeTransition fadeTransition 
					= new FadeTransition(Duration.millis(1000), splash.getPane());
			fadeTransition.setFromValue(0.0);
			fadeTransition.setToValue(1.0);

			SequentialTransition seqT = new SequentialTransition (fadeTransition, pauseTransition);
			seqT.setOnFinished(onFinished);
			seqT.play();				
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	
	public SplashController loadSplashScreen(Stage stage)
			throws IOException {
		stage.initModality(Modality.WINDOW_MODAL);		
		stage.setTitle("Media Pie");
		stage.initStyle(StageStyle.UNDECORATED);

		stage.getIcons().add(new Image(getClass().getResourceAsStream("app.png")));

		// Load the root layout from the fxml file
		FXMLLoader loader = new FXMLLoader(MainUI.class.getResource(screen0File));
		Parent rootLayout = (Parent) loader.load();
		Resizer.resize(rootLayout);		
		Scene scene = new Scene(rootLayout);
		scene.getStylesheets().add(getClass().getResource("styling.css").toExternalForm());			
		stage.setScene(scene);

		SplashController controller = (SplashController) loader.getController();
		stage.show();			
		controller.onDisplay();

		return controller;
	}

	public void runInSystemTray() {
		TrayIcon trayIcon = null;
		if (log.isDebugEnabled())
			log.debug("Running in system tray");

		if(runningInTray) return;
		
		if (SystemTray.isSupported()) {
			// get the SystemTray instance
			SystemTray tray = SystemTray.getSystemTray();
			
			// create a action listener to listen for default action executed on the tray icon
			ActionListener listener = new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// execute default action of the application
					// ...
				}
			};

			ActionListener openListener = new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					openWindow();
				}
			};

			ActionListener stopListener = new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					stopProcess();
				}
			};


			ActionListener exitListener = new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					exitProcess();					
				}
			};
			
			// create a popup menu
			PopupMenu popup = new PopupMenu();

			// create menu item for the default action
			MenuItem openItem = new MenuItem("Open");
			MenuItem stopItem = new MenuItem("Stop");
			MenuItem exitItem = new MenuItem("Exit");
			openItem.addActionListener(openListener);
			stopItem.addActionListener(stopListener);
			exitItem.addActionListener(exitListener);
			popup.add(openItem);
			popup.add(stopItem);
			popup.add(exitItem);
			
			// construct a TrayIcon
			java.awt.Image image = Toolkit.getDefaultToolkit().createImage(getClass().getResource("app.png"));
			trayIcon = new TrayIcon(image, "Media Pie", popup);
			trayIcon.setPopupMenu(popup);
			trayIcon.setImageAutoSize(true);
			
			//trayIcon.addActionListener(listener);
			
			// ...
			// add the tray image
			try {
				tray.add(trayIcon);
			} catch (AWTException e) {
				log.error("POP UP Menu Error", e);
			}
		} else {
		}
		
		// ...
		// some time later
		// the application state has changed - update the image
		//if (trayIcon != null) {
		//	trayIcon.setImage(updatedImage);
		//}

		runningInTray = true;		
	}
	
}