package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.mediapie.server.Persistance;
import com.avaje.ebean.*;

public class DetectServerController implements Initializable , ControlledScreen {
	
	ScreensController myController;

	boolean isLoaded = false;

	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onDisplay() {
	}

	public void onLoad() {
		Platform.runLater(new Runnable() {
			public void run() {
				initialLoad();
			}
		});		
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}
	
	@FXML
	private void handleDetectButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen0BID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}						
	}

	@FXML
	private void handleNoDetectButtonAction(ActionEvent event) {		
		try {
			ServiceHelper.setImplementationToLocal();
			myController.setScreen(MainUI.screen1ID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}	
}