package com.mediapie.client.ui;

import javafx.application.Application;
import javafx.beans.value.*;
import javafx.concurrent.Worker;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.*;
import org.controlsfx.dialog.*;
import org.controlsfx.control.action.*;
import javafx.application.Platform;
import com.mediapie.*;
import com.mediapie.util.*;

public class DialogHelper  {
	static org.apache.commons.logging.Log log = Logger.getLog(DialogHelper.class);

	public static boolean ERROR = false;
	
	public static void showErrorAsync(final String title,
									  final String message,
									  final String error) {
		Platform.runLater(new Runnable() {
			public void run() {
				showError(title, message, error);
			}
		});		
	}
	
	public static void showError(String title,
								 String message,
								 String error) {
		
		log.error("showExceptionA1:" + message + " error=" + error);

		ERROR = true;

		final Stage stage = new Stage();

		Action response = Dialogs.create()
						  .owner(null)
						  .title(title)
						  .masthead(message)
						  .message(error)
						  .showError();

	}

	public static void showExceptionAsync(final String title,
										  final String message,
										  final Throwable exception) {
		Platform.runLater(new Runnable() {
			public void run() {
				showException(title, message, exception);
			}
		});		
	}
	
	
	public static void showException(String title,
									 String message,
									 Throwable exception) {
		log.error("showExceptionA:" + message, exception);
		
		ERROR = true;

		final Stage stage = new Stage();

		Action response = Dialogs.create()
						  .owner(null)
						  .title(title)
						  .masthead(message)
						  .message(exception.getMessage())
						  .showException(exception);
		
	}
	
	public static void showUnknownExceptionOutside(final Object o, final Throwable exception) {
		log.error("showUnknownExceptionOutsideB", exception);
		
		ERROR = true;

		Platform.runLater(new Runnable() {
			public void run() {
				showUnknownException(o, exception);
			}
		});		
	}

	public static void showUnknownExceptionAsync(
		final Object o,
		final Throwable exception) {
		
		Platform.runLater(new Runnable() {
			public void run() {
				showUnknownException(o, exception);
			}
		});		
	}
	
	public static void showUnknownException(Object o,
											Throwable exception) {
		
		log.error("showUnknownExceptionC for " + ((o != null) ? o.getClass().getName() : " Null Object"), exception);
		
		ERROR = true;

		final Stage stage = new Stage();

		String message = (exception.getMessage() == null) ? "Unknown message" : exception.getMessage();

		Action response = Dialogs.create()
						  .owner(null)
						  .title("Unknown Exception")
						  .masthead("In class:" + o.getClass().getName())
						  .message(message)
						  .showException(exception);
		
		if (log.isDebugEnabled())
			log.debug("Did show the exception dialog");
	}

	public static void showUnknownExceptionAsync(
		final Object o,
		final String message,
		final String exceptionStack) {

		Platform.runLater(new Runnable() {
			public void run() {
				showUnknownException(o, message, exceptionStack);
			}
		});				
	}
	
	public static void showUnknownException(Object o,
											String message,
											String exceptionStack) {
		log.error("showUnknownExceptionD for " + ((o != null) ? o.getClass().getName() : " Null Object") + " - " + exceptionStack);

		ERROR = true;
		
		MediaPieException e = new MediaPieException(message, new Exception(exceptionStack));
		showUnknownException(o, e);
	}		
}