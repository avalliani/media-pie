package com.mediapie.client.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Modality;
import javafx.fxml.FXMLLoader;
import javafx.animation.*;
import javafx.util.*;
import java.io.*;
import com.mediapie.client.*;
import com.mediapie.util.*;

public class RemoteDirectoryChooser {
	static org.apache.commons.logging.Log log = Logger.getLog(RemoteDirectoryChooser.class);

	public static MediaFile show() {
		Stage stage = new Stage();
		stage.getIcons().add(new Image(RemoteDirectoryChooser.class.getResourceAsStream("app.png")));

		ScreensController container = new ScreensController();
		container.loadScreen("remoteDirectoryChooser", "remoteDirectoryChooser.fxml");
		container.setScreen("remoteDirectoryChooser");

		DirectoryChooserController controller = (DirectoryChooserController) container.getScreenController("remoteDirectoryChooser");
		
		Group root = new Group();
		root.getChildren().addAll(container);
		Scene scene = new Scene(root);
		stage.initModality(Modality.WINDOW_MODAL);
		scene.getStylesheets().add(RemoteDirectoryChooser.class.getResource("styling.css").toExternalForm());
		stage.setTitle("Choose Directory");
		stage.setScene(scene);
		stage.initOwner(MainUI.getPrimaryStage());
		stage.showAndWait();

		MediaFile file = controller.getCurrentSelection();
		
		return file;
	}
}