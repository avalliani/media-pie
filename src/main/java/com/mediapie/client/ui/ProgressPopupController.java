package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.discovery.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class ProgressPopupController implements Initializable , ControlledScreen {
	
	static org.apache.commons.logging.Log log = Logger.getLog(ProgressPopupController.class);	

	@FXML
	protected ProgressBar progress;

	@FXML
	protected Label message;

	ScreensController myController;

	boolean isLoaded = false;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void onDisplay() {
		//discover the clients if any
		(new Thread() {
			public void run() {
				handleProgress();				
			}
		}).start();
	}

	public void setMessage(String messageText) {
		message.setText(messageText);
	}
	
	public void setProgress(double p) {
		progress.setProgress(p);
	}

	public double getProgress() {
		return progress.getProgress();
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}

	void handleProgress() {
		Platform.runLater(new Runnable() {
			public void run() {
				setProgress(0.0);

				final int delay = 1000; // delay for 1 sec.
				final int interval = 1000;
				final int max = 60000;
				final int period = max/interval; // repeat every sec.
				final Timer timer1 = new Timer();   
				final Timer timer2 = new Timer();   

				timer1.scheduleAtFixedRate(new TimerTask()  {  
					public void run() {
						setProgress(getProgress() + (1.0/period));
					}   
				}, delay, interval);

				timer2.schedule(new TimerTask()  {  
					public void run() {						
						setProgress(0.0);
					}   
				}, max);										
			}
		});		
	}	
}