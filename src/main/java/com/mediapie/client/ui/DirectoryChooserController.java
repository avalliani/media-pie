package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.control.cell.*;
import javafx.beans.value.*;
import javafx.util.*;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.fxml.*;
import javafx.scene.control.TableColumn.*;
import javafx.beans.property.*;
import javafx.scene.control.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.discovery.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import org.apache.commons.lang3.time.DateFormatUtils;

public class DirectoryChooserController implements Initializable , ControlledScreen {
	
	static org.apache.commons.logging.Log log = Logger.getLog(LoginController.class);	

	@FXML
	private Pane pContent;

	@FXML
	private TextField currentFolder;
	
	@FXML
	protected TreeView folderTreeView;

	@FXML
	private TreeTableView folderTableView;	
	
	ScreensController myController;

	boolean isLoaded = false;

	MediaFile currentSelection;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void onDisplay() {
		Platform.runLater(new Runnable() {
			public void run() {
				loadDirectories();				
			}
		});		
	}

	public MediaFile getCurrentSelection() {
		return currentSelection;
	}
	
	void loadDirectories() {
		try {

			//
			// set up the left tree hierarchy
			//
			TreeItem<MediaFile> rootItem = new TreeItem<MediaFile> (new MediaFile("My Computer"));

			List<MediaFile> rootDirectories = ServiceHelper.getFoldersRoots();
			rootItem.setExpanded(true);

			for(MediaFile dir : rootDirectories) {
				TreeItem<MediaFile> item = new TreeItem<MediaFile> (dir);
				rootItem.getChildren().add(item);
			}
			
			folderTreeView.setRoot(rootItem);

			/*
			folderTreeView.setCellFactory(new Callback<TreeView<MediaFile>,TreeCell<String>>(){
				@Override
				public TreeCell<String> call(TreeView<MediaFile> p) {
					return new TreeCellCustom();
				}
			});
			*/
			
			//
			// setup the table
			//
			TreeTableColumn<MediaFile,String> fileNameCol = new TreeTableColumn<MediaFile, String>("Folder Name");
			TreeTableColumn<MediaFile,String> lastModifiedCol = new TreeTableColumn<MediaFile, String>("Modified");
			double tableWidth = folderTreeView.getPrefWidth();
			double fileNameColWidth = tableWidth * 0.7;
			fileNameCol.setMinWidth(fileNameColWidth);
			lastModifiedCol.setMinWidth(tableWidth - fileNameColWidth);

			folderTableView.getColumns().setAll(fileNameCol, lastModifiedCol);

			fileNameCol.setCellValueFactory(new Callback<javafx.scene.control.TreeTableColumn.CellDataFeatures<MediaFile, String>, ObservableValue<String>>() {
				public ObservableValue<String> call(
													javafx.scene.control.TreeTableColumn.CellDataFeatures<MediaFile, String> f) {
					String nameString = f.getValue().getValue().getName();
					return new SimpleStringProperty(nameString);				
				}
			});

			lastModifiedCol.setCellValueFactory(new Callback<javafx.scene.control.TreeTableColumn.CellDataFeatures<MediaFile, String>, ObservableValue<String>>() {
				public ObservableValue<String> call(javafx.scene.control.TreeTableColumn.CellDataFeatures<MediaFile, String> f) {
					MediaFile mf = f.getValue().getValue();
					Long millis = mf.lastModified();
					if (millis != null) {
						String dateString = DateFormatUtils.format(millis, "MM/dd/yyyy hh:mm aaa");
						return new SimpleStringProperty(dateString);
					} else {
						return new SimpleStringProperty("");
					}
				}
			});

			EventHandler doubleClickTable = new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
						if(mouseEvent.getClickCount() == 2){
							if (currentSelection != null) {
								updateFolderTableView(currentSelection);
							}
						}
					}
				}
			};
			
			ChangeListener treeListener = new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue,
				Object newValue) {

					if (log.isDebugEnabled()) {
						log.debug("Change of folder selected");
					}

					TreeItem<MediaFile> selectedItem = (TreeItem<MediaFile>) newValue;
					if (selectedItem != null) {
						MediaFile selectedFile = (MediaFile) selectedItem.getValue();

						//ignore root node click
						if (selectedFile.getAbsolutePath() != null)
							updateFolderTableView(selectedFile);
					}
				}
			};

			ChangeListener tableListener = new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue,
				Object newValue) {

					if (log.isDebugEnabled()) {
						log.debug("Change selected");
					}

					TreeItem<MediaFile> selectedItem = (TreeItem<MediaFile>) newValue;
					if (selectedItem != null) {
						MediaFile selectedFile = (MediaFile) selectedItem.getValue();
						currentSelection = selectedFile;
						currentFolder.setText(currentSelection.getAbsolutePath());
					}
				}
			};
			
			folderTreeView.getSelectionModel().selectedItemProperty().addListener(treeListener);
			folderTableView.getSelectionModel().selectedItemProperty().addListener(tableListener);		
			folderTableView.setOnMouseClicked(doubleClickTable);

			if(rootItem.getChildren().size() > 0) {
				boolean mediaLocationFound = false;
				
				// if mediapie server scroll to /media folder
				if (ServiceHelper.isConnectedRemotely()) {
					MediaFile rootDir = rootDirectories.get(0);
					List<MediaFile> rootSubDirectories = ServiceHelper.getFolders(rootDir.getAbsolutePath());
					if (rootSubDirectories != null) {
						for (MediaFile subDirectory : rootSubDirectories) {
							if ("/media".equals(subDirectory.getAbsolutePath())) {
								mediaLocationFound = true;
								updateFolderTableView(subDirectory);
								break;
							}
						}
					}
				}

				if (!mediaLocationFound)
					folderTreeView.scrollTo(0);
			}
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}
	}

	void updateFolderTableView(MediaFile selectedFile) {
		try {
			currentSelection = selectedFile;
			currentFolder.setText(currentSelection.getAbsolutePath());		
			List<MediaFile> directories = ServiceHelper.getFolders(selectedFile.getAbsolutePath());
			TreeItem<MediaFile> rootSubListItem = new TreeItem<MediaFile> (selectedFile);

			if (directories != null) {
				String upDirectoryString = FileUtils.getUpDirectory(selectedFile.getAbsolutePath());
				MediaFile mf = new MediaFile("..");
				mf.setAbsolutePath(upDirectoryString);
				TreeItem<MediaFile> upItem = new TreeItem<MediaFile>(mf);
				rootSubListItem.getChildren().add(upItem);
				for (MediaFile dir : directories) {
					TreeItem<MediaFile> item = new TreeItem<MediaFile> (dir);
					rootSubListItem.getChildren().add(item);
				}
			}
			
			folderTableView.setRoot(rootSubListItem);
			folderTableView.setShowRoot(false);
			rootSubListItem.setExpanded(true);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}
	
	@FXML
	private void handleSelectFolderButtonAction(ActionEvent event) {
		try {
			if (currentSelection != null) {
				Stage stage = (Stage) pContent.getScene().getWindow();
				stage.close();
			}				
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	
	@FXML
	private void handleRefreshButtonAction(ActionEvent event) {
		loadDirectories();
	}
	
	@FXML
	private void handleCancelButtonAction(ActionEvent event) {
		try {
			currentSelection = null;
			Stage stage = (Stage) pContent.getScene().getWindow();
			stage.close();
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	public class TreeCellCustom extends TreeCell<String> {
		EventHandler handler;
		
		public TreeCellCustom() {
			super();
			setOnMouseClicked(getHandler());			
		}

		EventHandler getHandler() {			
			//add mouse listener
			handler = new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent mouseEvent) {
						if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
							if(mouseEvent.getClickCount() <= 2){
							}
						}
					}
				};
			return handler;
		}
	}
}