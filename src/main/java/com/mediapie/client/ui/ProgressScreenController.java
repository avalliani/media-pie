package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.io.*;
import java.util.*;
import java.net.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class ProgressScreenController implements Initializable , ControlledScreen  {
	static org.apache.commons.logging.Log logger = Logger.getLog(ProgressScreenController.class);
	
	@FXML
	protected Pane pContent;

	@FXML
	protected Button cancelBtn;		

	@FXML
	protected Button backBtn;		
	
	@FXML
	protected Label fileName;

	@FXML
	protected Label percentage;

	@FXML
	protected TextArea log;

	@FXML
	protected ProgressBar progress;
	
	protected ScreensController myController;

	long lastStatusUpdate = -1;

	ProgressThread progressThread = null;

	boolean isLoaded = false;
	
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onLoad() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		

		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void monitorProgress(ProgressScreenController controller) {
		if (progressThread == null) {
			progressThread = new ProgressThread(controller);
			progressThread.start();
		}
	}
	
	public void setLog(String text) {
		log.setText(text);
	}
	
	public void setPercentage(Double percent) {		
		percentage.setText("" + ((percent == null) ? 0.0 : percent));
	}

	public void setFileName(String name) {
		fileName.setText(name);
	}

	public void setProgress(double p) {
		progress.setProgress(p);
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}

	public ScreensController getScreenParent(){
		return myController;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {				
			}
		});		
	}

	public void start() {
		//ServiceHelper.startUpload();
		DialogHelper.ERROR = false;
	}
	
	public void onDisplay() {
		//need to check if the process is already running first...
		monitorProgress(this);

		Platform.runLater(new Runnable() {
			public void run() {
				try {
					if (logger.isDebugEnabled())
						logger.debug("OnDisplay - called");
					if (!ServiceHelper.isRunning()) {

						if (logger.isDebugEnabled())
							logger.debug("Service not running - will start a new process");

						clear();
						start();
						backBtn.setDisable(true);
						cancelBtn.setText("Cancel");			
					} else {
						if (logger.isDebugEnabled())
							logger.debug("Service is still running");

						backBtn.setDisable(true);
						cancelBtn.setText("Cancel");			
					}
				} catch(Exception e) {
					DialogHelper.showUnknownException(this, e);
				}							
			}
		});				
	}

	public void clear() {
		setPercentage(0.0);
		setFileName("");
		setLog("");
		setProgress(0.0);
	}

	public void updateStatus(final ProgressStatus status) {
		boolean update = true;
		
		if (update) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					try {
						setPercentage(status.getPercentage());
						setFileName(status.getFileName());
						setLog(status.getLog());
						setProgress((status.getPercentage() == null) ? 0 : status.getPercentage()/100.0);

						if (Boolean.TRUE.equals(status.getFinished())) {
							cancelBtn.setText("Done");
							backBtn.setDisable(false);
						}

						if (Boolean.TRUE.equals(status.getError())) {
							if (!DialogHelper.ERROR) {
								DialogHelper.showUnknownException(this, status.getErrorMessage(), status.getErrorStack());
								cancelBtn.setText("Done");
								backBtn.setDisable(false);								
							}
						}													
					} catch(Exception e) {
						DialogHelper.showUnknownException(this, e);
					}			
				}
			});
		}
	}
	
	@FXML
	protected void handleCancelButtonAction(ActionEvent event) {
		ProgressPopup.show("Cancelling process  ...");
		try {
			new Thread("Cancel process") {
				public void run() {
					try {
						ServiceHelper.stopProcess();

						Platform.runLater(new Runnable() {
							public void run() {				
								myController.setScreen(MainUI.screen1ID);
							}
						});
						
						ProgressPopup.hide();						
					} catch(Exception e) {
						ProgressPopup.hide();						
						DialogHelper.showUnknownExceptionAsync(this, e);
					}
				};
			}.start();
			
		} catch(Exception e) {
			ProgressPopup.hide();									
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	protected void handleBackButtonAction(ActionEvent event) {	
		try {
			myController.setScreen(MainUI.screen2AID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}	
}