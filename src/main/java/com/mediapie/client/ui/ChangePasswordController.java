package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.discovery.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class ChangePasswordController implements Initializable , ControlledScreen {
	
	static org.apache.commons.logging.Log log = Logger.getLog(ChangePasswordController.class);	

	@FXML
	protected ProgressBar progress;

	@FXML
	private PasswordField oldPassword;
	
	@FXML
	private PasswordField newPassword;

	@FXML
	private PasswordField reenterNewPassword;
	
	ScreensController myController;

	boolean isLoaded = false;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void onDisplay() {
		//discover the clients if any
		(new Thread() {
			public void run() {
			}
		}).start();
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}
	
	
	@FXML
	private void handleChangePasswordButtonAction(ActionEvent event) {
		boolean oldPasswordNotNull = checkRequiredTextField(oldPassword);
		boolean newPasswordNotNull = checkRequiredTextField(newPassword);
		boolean reenterNewPasswordNotNull = checkRequiredTextField(reenterNewPassword);

		if (oldPasswordNotNull && newPasswordNotNull && reenterNewPasswordNotNull) {
			final String oldPasswordValue = oldPassword.getText();
			final String newPasswordValue = newPassword.getText();
			final String reenterNewPasswordValue = reenterNewPassword.getText();
			final String username = ServiceSettings.USERNAME;
			
			if (!newPasswordValue.equals(reenterNewPasswordValue)) {
				setRed(newPassword);
				setRed(reenterNewPassword);
			} else {
				new Thread("Change Password") {
					public void run() {
						changePassword(username,
									   oldPasswordValue, newPasswordValue);
					}
				}.start();				
			}
		}
	}

	void changePassword(final String username,
						final String oldPassword,
						final String newPassword) {

		ProgressPopup.show("Changing Password ...");
		
		try {
			boolean changed = ServiceHelper.changePassword(
				username,
				oldPassword, newPassword);

			if (!changed) {
				ProgressPopup.hide();			
				DialogHelper.showErrorAsync("Password not changed",
									   "Please check if old password is correct",
									   "Please provide the correct old password");

			} else {
				ServiceSettings.reinit(username, newPassword);
				final boolean isRunning = ServiceHelper.isRunning();

				Platform.runLater(new Runnable() {
					public void run() {
						if (isRunning) {
							myController.setScreen(MainUI.screen3AID);					
						} else {
							myController.setScreen(MainUI.screen1ID);
						}						
					}
				});								
			}
		} catch(javax.xml.ws.soap.SOAPFaultException se) {
			ProgressPopup.hide();			

			log.error("Error in logging in or initial connection:", se);

			javax.xml.soap.SOAPFault fault = se.getFault();
			String code = (fault != null) ? fault.getFaultCode() : "UNKNOWN";
			String detail = (fault != null) ? fault.getFaultString() : "UNKNOWN";
			if (detail.indexOf("The security token could not be authenticated or authorized") >= 0) {
				DialogHelper.showErrorAsync("Invalid Login",
									   "Invalid username and password",
									   "Please provide a correct username and password");
			} else {
				DialogHelper.showExceptionAsync("Unknow error logging in",
					detail,
					se);
			}							
		} catch(javax.xml.ws.WebServiceException ws) {
			ProgressPopup.hide();			
			String message = ws.getMessage();

			if (message != null && message.indexOf("Could not send Message") >= 0) {
				DialogHelper.showErrorAsync("Connection Failed",
									   "Unable to connect",
									   "Please check your server connection");
			} else {
				DialogHelper.showUnknownExceptionAsync(this, ws);
			}			
		} catch(Exception e) {
			ProgressPopup.hide();						
			DialogHelper.showUnknownExceptionAsync(this, e);
		}

		ProgressPopup.hide();					
	}
	
	@FXML
	private void handleCancelButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen0CID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen0CID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	boolean checkRequiredTextField(TextField field) {
		String text = field.getText();
		if (text == null || "".equals(text.trim())) {
			setRed(field);
			return false;
		} else {
			removeRed(field);
			return true;
		}
	}

	private void setRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();

		if(!styleClass.contains("tferror")) {
			styleClass.add("tferror");
		}
	}


	private void removeRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();
		styleClass.removeAll(Collections.singleton("tferror"));
	}	
}