package com.mediapie.client.ui;

import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.wss4j.common.ext.*;

//import org.apache.ws.security.WSPasswordCallback;
import com.mediapie.util.*;

public class ClientPasswordCallback implements CallbackHandler {
	static org.apache.commons.logging.Log log = Logger.getLog(ClientPasswordCallback.class);

	public void handle(Callback[] callbacks) throws IOException, 
	UnsupportedCallbackException {
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];
		
		if ("joe".equals(pc.getIdentifier())) {
			pc.setPassword("joespassword");
		}	 // else {...} - can add more users, access DB, etc.
	}
}