package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import javafx.stage.DirectoryChooser;
import javafx.fxml.*;
import java.io.*;
import java.util.*;
import java.net.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class DirectoryScreenController implements Initializable , ControlledScreen  {
	static org.apache.commons.logging.Log log = Logger.getLog(DirectoryScreenController.class);

	@FXML
	private Pane pContent;
	
	@FXML
	private TextField dir1;

	@FXML
	private TextField dir2;

	@FXML
	private TextField dir3;

	@FXML
	private TextField dir4;

	@FXML
	private TextField dir5;

	@FXML
	private ComboBox cdir1;

	@FXML
	private ComboBox cdir2;

	@FXML
	private ComboBox cdir3;

	@FXML
	private ComboBox cdir4;

	@FXML
	private ComboBox cdir5;

	@FXML
	private ComboBox scheduleBackupComboBox;
	
	@FXML
	private Button bdir1;

	@FXML
	private Button bdir2;

	@FXML
	private Button bdir3;

	@FXML
	private Button bdir4;

	@FXML
	private Button bdir5;

	boolean isLoaded = false;
	
	ScreensController myController;

	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		List<Schedule> schedules = Arrays.asList(Schedule.values());		
		
		scheduleBackupComboBox.getItems().addAll(schedules);
	}

	public void wireDirectoryButtons(final boolean remoteConnection) {

		if (log.isDebugEnabled())
			log.debug("Remote connection for Client:" + remoteConnection);
		
		//chooser.setTitle("Select directory");
		final Button[] buttons = new Button[]{bdir1, bdir2, bdir3, bdir4, bdir5};
		final TextField[] fields = new TextField[]{dir1, dir2, dir3, dir4, dir5};
		for (int i=0; i<buttons.length; i++) {
			final int val = i;
			buttons[i].setOnAction(new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent e) {
					if (!remoteConnection) {
						ProgressPopup.show("Opening directory ...");

						//add directory chooser
						final DirectoryChooser chooser = new DirectoryChooser();
						File file = chooser.showDialog(null);
						ProgressPopup.hide();
						
						if (file != null)
							fields[val].setText(file.getAbsolutePath());
					} else {
						MediaFile file = RemoteDirectoryChooser.show();
						if (file != null)
							fields[val].setText(file.getAbsolutePath());
					}
				}
			});
		}
	}
	
	public void onLoad() {
		isLoaded = true;				
	}

	public boolean isLoaded() {
		return isLoaded;
	}

	public void onDisplay() {
		if (log.isDebugEnabled())
			log.debug("DirectoryScreenController.onDisplay called");
		
		new Thread("Directory Controller Thread - onDisplay") {
			public void run() {
				loadSettings();
			}
		}.start();							
	}

	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {

		Platform.runLater(new Runnable() {
			public void run() {				
				dir1.requestFocus();
			}
		});		
	}

	public void clear() {
		if (log.isDebugEnabled())
			log.debug("Clearing fields for directories");
		
		final TextField[] fields = new TextField[]{dir1, dir2, dir3, dir4, dir5};

		for (int i=0; i < fields.length; i++) {
			fields[i].setText("");
		}

		scheduleBackupComboBox.getSelectionModel().clearSelection();		
		scheduleBackupComboBox.setValue(null);		
	}
	
	public String getDir1() {
		return dir1.getText();
	}

	public String getDir2() {
		return dir2.getText();
	}
	
	public String getDir3() {
		return dir3.getText();
	}
	
	public String getDir4() {
		return dir4.getText();
	}

	public String getDir5() {
		return dir5.getText();
	}

	public String getCDir1() {
		if (cdir1.getValue() == null) return null;
		return cdir1.getValue().toString();
	}

	public String getCDir2() {
		if (cdir2.getValue() == null) return null;
		return cdir2.getValue().toString();
	}

	public String getCDir3() {
		if (cdir3.getValue() == null) return null;
		return cdir3.getValue().toString();
	}

	public String getCDir4() {
		if (cdir4.getValue() == null) return null;
		return cdir4.getValue().toString();
	}

	public String getCDir5() {
		if (cdir5.getValue() == null) return null;
		return cdir5.getValue().toString();
	}
	
	public void loadSettings() {
		ProgressPopup.show("Getting settings ...");
		
		try {
			if (log.isDebugEnabled())
				log.debug("Starting DirectoryScreen.loadSettings");
			
			final List<Directory> dirs = ServiceHelper.getBackupDirectories();
			final Configuration config = ServiceHelper.getConfiguration();
			final List<String> directories = ServiceHelper.getCloudDirectories(1);
			final boolean remoteConnection = ServiceHelper.isConnectedRemotely();

			if (log.isDebugEnabled()) {
				log.debug("Remote connection:" + remoteConnection);				
			}
			
			ProgressPopup.hide();
			
			Platform.runLater(new Runnable() {
				public void run() {
					
					try {
						clear();

						if (log.isDebugEnabled())
							log.debug("Setting directories");
						
						if (dirs != null) {
							for (Directory dir : dirs) {
								if (dir.getId() != null) {
									switch(dir.getId()) {
										case 1: {
													dir1.setText(dir.getName());
													break;
												}
										case 2: {
													dir2.setText(dir.getName());
													break;
												}
										case 3: {
													dir3.setText(dir.getName());
													break;
												}
										case 4: {
													dir4.setText(dir.getName());
													break;
												}
										case 5: {
													dir5.setText(dir.getName());
													break;
												}
									}
								}
							}
						}

						if (log.isDebugEnabled())
							log.debug("Setting frequency schedule");

						Integer frequency = config.getFrequency();
						if (frequency != null) {
							Schedule frequencyScheduler = Schedule.byValue(frequency);
							scheduleBackupComboBox.setValue(frequencyScheduler);
						}

						if (log.isDebugEnabled())
							log.debug("wiring directory buttons");

						wireDirectoryButtons(remoteConnection);

						final ComboBox[] combos = new ComboBox[]{cdir1, cdir2, cdir3, cdir4, cdir5};

						for (int i=0; i < combos.length; i++) {
							if (log.isDebugEnabled()) {
								log.debug("Adding to the combo:" + ((directories != null) ? directories.size() : 0));
							}
							combos[i].getSelectionModel().clearSelection();
							combos[i].getItems().clear();				
								//combos[i].getItems().addAll(directories);
						}							
						
					} catch(Exception e) {
						ProgressPopup.hide();			
						DialogHelper.showUnknownException(this, e);
					}
				}
			});								
		} catch(Exception e) {
			ProgressPopup.hide();			
			DialogHelper.showUnknownExceptionAsync(this, e);
		}

		if (log.isDebugEnabled())
			log.debug("Ending DirectoryScreen.loadSettings");		
	}
	
	@FXML
	private void handleNextButtonAction(ActionEvent event) {
		ProgressPopup.show("Saving  ...");
		
		try {
			boolean cdir1 = checkValidFolder(false, dir1);
			boolean cdir2 = checkValidFolder(true, dir2);
			boolean cdir3 = checkValidFolder(true, dir3);
			boolean cdir4 = checkValidFolder(true, dir4);
			boolean cdir5 = checkValidFolder(true, dir5);
			final Schedule schedule = (Schedule) scheduleBackupComboBox.getValue();
			
			if (cdir1 && cdir2 && cdir3 && cdir4 && cdir5) {
				final String[] dirs = new String[]{getDir1(), getDir2(), getDir3(), getDir4(), getDir5()};
				final String[] clouddirs = new String[]{getCDir1(), getCDir2(), getCDir3(), getCDir4(), getCDir5()};

				new Thread("Directory Controller Thread - onSubmit") {
					public void run() {
						try {
							ServiceHelper.saveBackupDirectories(dirs, clouddirs);

							if (schedule != null) {
								ServiceHelper.saveScheduleFrequency(schedule.getValue());
							}

							//starting the upload process
							if (!ServiceHelper.isRunning()) {
								if (log.isDebugEnabled())
									log.debug("Service not running - will start a new process");
								
								ServiceHelper.startUpload();								
							} else {
								if (log.isDebugEnabled())
									log.debug("Service already running - will not start a new process");
							}
							
							Platform.runLater(new Runnable() {
								public void run() {				
									//move to the next screen
									myController.setScreen(MainUI.screen3AID);			
								}
							});		

							ProgressPopup.hide();
							
						} catch(Exception e) {
							ProgressPopup.hide();
							DialogHelper.showUnknownException(this, e);
						}
					}
				}.start();									
			}

			ProgressPopup.hide();			
		} catch(Exception e) {
			ProgressPopup.hide();
			DialogHelper.showUnknownException(this, e);
		}			
	}


	/*
	@FXML
	private void handleCancelButtonAction(ActionEvent event) {	
		Platform.exit();
	}
	*/
	
	@FXML
	private void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen1AID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	
	boolean checkValidFolder(boolean ignoreIfEmpty, TextField field) {
		String text = field.getText();
		boolean validDirectory = ServiceHelper.checkValidFolder(text);

		if ((text == null || "".equals(text.trim())) && ignoreIfEmpty)
			return true;

		if (text == null || "".equals(text.trim()) || !validDirectory) {
			setRed(field);
			return false;
		} else {
			removeRed(field);
			return true;
		}
	}

	private void setRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();

		if(!styleClass.contains("tferror")) {
			styleClass.add("tferror");
		}
	}


	private void removeRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();
		styleClass.removeAll(Collections.singleton("tferror"));
	}
}