package com.mediapie.client.ui;

import java.io.*;
import java.util.*;
import com.mediapie.util.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.collections.*;
import javafx.stage.*;
import javafx.scene.text.*;

public class Resizer {
	static org.apache.commons.logging.Log log = Logger.getLog(Resizer.class);

	final static double FONT = 24;
	
	public static void resize(Parent parent) {
		double height = Screen.getPrimary().getBounds().getHeight();
		double width = Screen.getPrimary().getBounds().getWidth();

		//height = 800;
		//width = 1600;
		
		double scaleX = width/2560.0;
		double scaleY = height/1600.0;

		//all font
		double originalFont = FONT;
		double adjustedFont = originalFont * scaleX * 0.6;
		double buttonSmallFont = 18;
		double adjustedButtonSmallFont = buttonSmallFont * scaleX * 0.6;
		
		parent.setStyle("-fx-font-size: " + adjustedFont + "pt;");
		resize(scaleX, scaleY, parent);
	}
	
	public static void resize(final double xScale, double yScale, Node parent) {
		if (parent instanceof Pane) {
			Pane pane = (Pane) parent;
			ObservableList<Node> children = pane.getChildren();
			for (Node node : children) {
				resize(xScale, yScale, node);
			}
			pane.setPrefWidth(pane.getPrefWidth() * xScale);
			pane.setPrefHeight(pane.getPrefHeight() * yScale);
			pane.setLayoutX(pane.getLayoutX() * xScale);
			pane.setLayoutY(pane.getLayoutY() * yScale);

			if (pane instanceof GridPane) {
				GridPane gridPane = (GridPane) pane;
				ObservableList<ColumnConstraints> columns = gridPane.getColumnConstraints();
				ObservableList<RowConstraints> rows = gridPane.getRowConstraints();
				for (ColumnConstraints column : columns) {
					column.setPrefWidth(column.getPrefWidth() * xScale);					
					column.setMinWidth(column.getMinWidth() * xScale);					
				}

				for (RowConstraints row : rows) {
					row.setPrefHeight(row.getPrefHeight() * yScale);
					row.setMinHeight(row.getMinHeight() * yScale);
				}				
			}
		} else if (parent instanceof Control) {
			Control control = (Control) parent;
			control.setPrefWidth(control.getPrefWidth() * xScale);
			control.setPrefHeight(control.getPrefHeight() * yScale);
			control.setLayoutX(control.getLayoutX() * xScale);
			control.setLayoutY(control.getLayoutY() * yScale);
			if (control instanceof Labeled) {
				Labeled label = (Labeled) control;
				Font font = label.getFont();
				double size = font.getSize();
				if (size != FONT) {
					double sizeAdjusted = size * xScale;
					label.setFont(new Font(font.getName(), sizeAdjusted));
				}
			}
		}
	}
}