package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class AwsScreenController implements Initializable , ControlledScreen {
	static org.apache.commons.logging.Log log = Logger.getLog(AwsScreenController.class);
	
	@FXML
	private Pane pContent;
	
	@FXML
	private TextField bucketName;
	
	@FXML
	private TextField accessKey;

	@FXML
	private TextField secretKey;

	@FXML
	private TextField encryptionKey;

	ScreensController myController;

	boolean isLoaded = false;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onDisplay() {
		Platform.runLater(new Runnable() {
			public void run() {
				initialLoad();				
			}
		});		
	}

	public void onLoad() {
		Platform.runLater(new Runnable() {
			public void run() {
				//initialLoad();				
			}
		});		
		
		isLoaded = true;
	}

	public void clear() {
		bucketName.setText("");;
		accessKey.setText("");;
		secretKey.setText("");
		encryptionKey.setText("");

	}
	
	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
				accessKey.requestFocus();
			}
		});		
	}
	
	public String getBucketName() {
		return bucketName.getText();
	}
	
	public String getAccessKey() {
		return accessKey.getText();
	}

	public String getSecretKey() {
		return secretKey.getText();
	}

	public String getEncryptionKey() {
		return encryptionKey.getText();
	}

	public void initialLoad() {
		clear();		
		ProgressPopup.show("Getting information  ...");
		try {
			new Thread("Aws onDisplay") {
				public void run() {
					try {
						final Configuration config = ServiceHelper.getConfiguration();

						Platform.runLater(new Runnable() {
							public void run() {				
								if (config != null) {
									accessKey.setText(config.getAccessKey());
									secretKey.setText(config.getSecretKey());
									encryptionKey.setText(config.getEncryptionKey());
									bucketName.setText(config.getGuid());
								}
							}
						});

						ProgressPopup.hide();						
					} catch(Exception e) {
						ProgressPopup.hide();						
						DialogHelper.showUnknownException(this, e);
					}
				}
			}.start();
		} catch(Exception e) {
			ProgressPopup.hide();						
			DialogHelper.showUnknownException(this, e);
		}
	}
	
	@FXML
	private void handleSubmitButtonAction(ActionEvent event) {
		try {
			boolean accessKeyNotNull = checkRequiredTextField(accessKey);
			boolean secretKeyNotNull = checkRequiredTextField(secretKey);
			boolean encryptionKeyNotNull = checkRequiredTextField(encryptionKey);
			boolean bucketNameNotNull = checkRequiredTextField(bucketName);
			boolean encryptionKeyValid = ServiceHelper.validEncryptionKey(encryptionKey.getText());

			if (!encryptionKeyValid) {
				DialogHelper.showError("Invalid Key", "Required 256 bit encryption",
									   " Please upgrade JVM to allow larger key using" +
									   " http://www.oracle.com/technetwork/java/javase/downloads/jce-7-download-432124.html");
			} else {			
				if (bucketNameNotNull && accessKeyNotNull && secretKeyNotNull && encryptionKeyNotNull) {
					final String accessKeyValue = accessKey.getText();
					final String secretKeyValue = secretKey.getText();
					final String encryptionKeyValue = encryptionKey.getText();
					final String bucketNameValue = bucketName.getText();
					
					new Thread("Save Configuration and Test Cloud Connect") {
						public void run() {
							saveAndConnect(accessKeyValue,
										   secretKeyValue,
										   encryptionKeyValue,
										   bucketNameValue);
						}
					}.start();									
				}
			}
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	void saveAndConnect(final String accessKey,
						final String secretKey,
						final String encryptionKey,
						final String bucketName) {

		ProgressPopup.show("Checking Connection ...");
		
		try {
			ServiceHelper.saveConfiguration(accessKey,
				secretKey,
				encryptionKey,
				bucketName);

			try {
				ServiceHelper.checkConnection();

				Platform.runLater(new Runnable() {
					public void run() {
						myController.setScreen(MainUI.screen2AID);
					}
				});								

				ProgressPopup.hide();						
			} catch(MediaPieException ce) {
				ProgressPopup.hide();				
				DialogHelper.showExceptionAsync("Connection Issue", "Error connecting to Amazon. Please check your keys.", ce);
			}
		} catch(Exception e) {
			ProgressPopup.hide();			
			DialogHelper.showUnknownExceptionAsync(this, e);
		}

		ProgressPopup.hide();		
	}
	
	@FXML
	private void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen1ID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleNewKeyButtonAction(ActionEvent event) {
		new Thread("Save Configuration and Test Cloud Connect") {
			public void run() {
				getNewKey();
			}
		}.start();											
	}
	
	void getNewKey() {
		ProgressPopup.show("Getting new key ...");
		
		try {
			final String key = ServiceHelper.generateEncryptionKey();
			Platform.runLater(new Runnable() {
				public void run() {
					encryptionKey.setText(key);					
				}
			});
			ProgressPopup.hide();									
		} catch(Exception e) {
			ProgressPopup.hide();									
			DialogHelper.showUnknownExceptionAsync(this, e);
		}

		ProgressPopup.hide();								
	}

	@FXML
	private void handleAwsLink(ActionEvent event) {		
		try {
			String httpLink = "http://aws.amazon.com/s3";
			java.awt.Desktop.getDesktop().browse(new URI(httpLink));
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}	
	
	boolean checkRequiredTextField(TextField field) {
		String text = field.getText();
		if (text == null || "".equals(text.trim())) {
			setRed(field);
			return false;
		} else {
			removeRed(field);
			return true;
		}
	}

	private void setRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();

		if(!styleClass.contains("tferror")) {
			styleClass.add("tferror");
		}
	}


	private void removeRed(TextField tf) {
		ObservableList<String> styleClass = tf.getStyleClass();
		styleClass.removeAll(Collections.singleton("tferror"));
	}	
}