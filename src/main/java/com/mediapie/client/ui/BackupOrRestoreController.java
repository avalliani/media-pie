package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class BackupOrRestoreController implements Initializable , ControlledScreen {
	
	@FXML
	private Button backup;

	@FXML
	private Button restore;
	
	boolean isLoaded = false;

	ScreensController myController;

	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onDisplay() {
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}
	
	@FXML
	private void handleRestoreButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen1BID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}						
	}

	@FXML
	private void handleBackupButtonAction(ActionEvent event) {		
		try {
			myController.setScreen(MainUI.screen1AID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleChangeConnectionLink(ActionEvent event) {		
		try {
			myController.setScreen(MainUI.screen0BID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleUpgradeMediaServer(ActionEvent event) {		
		try {
			if (ServiceHelper.isConnectedRemotely()) {
				String httpLink = "https://" + ServiceSettings.SERVER_IP + ":10000";
				java.awt.Desktop.getDesktop().browse(new URI(httpLink));
			}			
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}	
}