package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import javafx.beans.value.*;
import java.net.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.discovery.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class DiscoveryResultsController implements Initializable , ControlledScreen {
	
	static org.apache.commons.logging.Log log = Logger.getLog(DiscoveryResultsController.class);	

	@FXML
	protected ProgressBar progress;

	@FXML
	protected ListView servers;

	@FXML
	protected Button submitBtn;
	
	ScreensController myController;

	boolean isLoaded = false;
	
	/**
     * Initializes the controller class.
     */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onLoad() {
		isLoaded = true;
	}

	public boolean isLoaded() {
		return isLoaded;
	}
	
	public void onDisplay() {
		//discover the clients if any
		(new Thread() {
			public void run() {
				handleDiscoveryProgress();
				handleDiscovery();
			}
		}).start();
	}

	public void setProgress(double p) {
		progress.setProgress(p);
	}

	public double getProgress() {
		return progress.getProgress();
	}
	
	public void setScreenParent(ScreensController screenParent){
		myController = screenParent;
	}
	
	public void setInitialFocus() {
		Platform.runLater(new Runnable() {
			public void run() {
			}
		});		
	}
	
	public void initialLoad() {
	}

	void handleDiscoveryProgress() {
		Platform.runLater(new Runnable() {
			public void run() {
				setProgress(0.0);
				submitBtn.setDisable(true);				
				servers.getItems().clear();

				final int delay = 1000; // delay for 1 sec.
				final int interval = 1000;
				final int max = DiscoveryClient.discoveryTimeout;
				final int period = max/interval; // repeat every sec.
				final Timer timer1 = new Timer();   
				final Timer timer2 = new Timer();   

				timer1.scheduleAtFixedRate(new TimerTask()  {  
					public void run() {
						setProgress(getProgress() + (1.0/period));
					}   
				}, delay, interval);

				timer2.schedule(new TimerTask()  {  
					public void run() {
						timer1.cancel();
						setProgress(1.0);
					}   
				}, max);										
			}
		});		
	}
	
	void handleDiscovery() {

		servers.getSelectionModel().selectedItemProperty().addListener(
			new ChangeListener<DiscoverySettings>() {
			public void changed(ObservableValue<? extends DiscoverySettings> ov, 
								DiscoverySettings old_val, DiscoverySettings new_val) {

				ServiceSettings.SERVER_IP = new_val.getIp();
				ServiceSettings.SERVER_PORT = new_val.getPort();
				ServiceSettings.setInitialized(false);
				
				if (log.isDebugEnabled())
					log.debug("Selected " + ServiceSettings.SERVER_IP + " port:" + ServiceSettings.SERVER_PORT);
			}
		});		
		final DiscoveryListener listener = new DiscoveryListener() {
			public void receivedPing(final DiscoverySettings serverResponse) {
				final String ip = serverResponse.getIp();
				final int port = serverResponse.getPort();
				final String name = serverResponse.getName();
				if (log.isInfoEnabled())
					log.info("Found server:" + ip + ":" + port + " name:" + name);

				final boolean found = (ip != null);
				
				Platform.runLater(new Runnable() {
					public void run() {
						if (ip != null) {
							//ObservableList<DiscoverySettings> items =FXCollections.observableArrayList (serverResponse);
							servers.getItems().add(serverResponse);
							servers.getSelectionModel().select(0);
							submitBtn.setDisable(false);				
						} else {
							submitBtn.setDisable(true);				
						}
					}
				});							
			}
		};

		new Thread() {
			public void run() {
				try {
					DiscoveryClient client = new DiscoveryClient(listener);
					client.discover();
				} catch(Exception e) {
					log.warn(e);
				}
			}
		}.start();
	}
	
	@FXML
	private void handleRetryButtonAction(ActionEvent event) {
		try {
			onDisplay();
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	
	@FXML
	private void handleSubmitButtonAction(ActionEvent event) {
		try {						
			myController.setScreen(MainUI.screen0CID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	@FXML
	private void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen0AID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}
	
}