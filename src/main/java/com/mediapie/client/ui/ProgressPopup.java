package com.mediapie.client.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Modality;
import javafx.fxml.FXMLLoader;
import javafx.animation.*;
import javafx.util.*;
import java.io.*;
import com.mediapie.client.*;
import com.mediapie.util.*;

public class ProgressPopup {
	static org.apache.commons.logging.Log log = Logger.getLog(ProgressPopup.class);
	static Stage stage;
	
	public static void show(final String message) {
		Platform.runLater(new Runnable() {
			public void run() {
				stage = new Stage();
				stage.centerOnScreen();				
				stage.initStyle(StageStyle.UNDECORATED);

				//stage.getIcons().add(new Image(RemoteDirectoryChooser.class.getResourceAsStream("app.png")));

				ScreensController container = new ScreensController();
				container.loadScreen("progressPopup", "progressPopup.fxml");
				container.setScreen("progressPopup");

				ProgressPopupController controller = (ProgressPopupController) container.getScreenController("progressPopup");
				controller.setMessage(message);

				Group root = new Group();
				root.getChildren().addAll(container);
				Scene scene = new Scene(root);
				stage.initModality(Modality.WINDOW_MODAL);
				scene.getStylesheets().add(RemoteDirectoryChooser.class.getResource("styling.css").toExternalForm());
				stage.setTitle("Progress");
				stage.setScene(scene);
				stage.initOwner(MainUI.getPrimaryStage());
				stage.show();
			}
		});					
		
	}

	public static void hide() {
		Platform.runLater(new Runnable() {
			public void run() {
				if (stage != null) {
					stage.close();
					stage = null;
				}
			}
		});					
	}
}