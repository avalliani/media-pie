package com.mediapie.client.ui;

import java.io.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.mediapie.util.*;
import com.mediapie.server.*;

public class ServiceHelper {
	static org.apache.commons.logging.Log log = Logger.getLog(ServiceHelper.class);
	static RootService service = new RootServiceImpl();

	public static void setImplementationToLocal() {
		service = new RootServiceImpl();
	}
	
	public static void setImplementation(RootService serviceImpl) {
		if (log.isDebugEnabled()) {
			log.debug("Setting implementation of RootService class to " + ((serviceImpl != null) ? serviceImpl.getClass().getName() : "NULL"));
		}
		service = serviceImpl;
	}

	public static boolean isConnectedRemotely() {
		if (service == null || service instanceof RootServiceImpl)
			return false;

		return true;
	}
	
	public static String getImplementationClass() {
		return (service != null) ? service.getClass().getName() : "NULL";
	}
	
	public static Configuration getConfiguration() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getConfiguration begin call - " + getImplementationClass());
		
		Configuration config = service.getConfiguration();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getConfiguration end call - " + getImplementationClass());
		
		return config;
	}

	public static void startUpload() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.startUpload begin call - " + getImplementationClass());
		
		service.startUpload();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.startUpload end call - "  + getImplementationClass());		
	}

	public static void startDownload() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.startDownload begin call - " + getImplementationClass());
		
		service.startDownload();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.startDownload end call - " + getImplementationClass());
		
	}

	public static Boolean isRunning() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.isRunning begin call - " + getImplementationClass());
		
		Boolean ret = service.isRunning();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.isRunning end call - " + getImplementationClass());

		return ret;
		
	}

	public static void stopProcess() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.stopProcess begin call - " + getImplementationClass());
		
		service.stopProcess();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.stopProcess end call - " + getImplementationClass());
		
	}

	public static void saveScheduleFrequency(Integer frequency) {

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveScheduleFrequency begin call - " + getImplementationClass());

		service.saveScheduleFrequency(frequency);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveScheduleFrequency end call -" + getImplementationClass());		
	}
	
	public static void saveConfiguration(String accessKey,
										 String secretKey,
										 String encryptionKey,
										 String bucketName) {

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveConfiguration begin call - " + getImplementationClass());
		
		service.saveConfiguration(accessKey, secretKey, encryptionKey, bucketName);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveConfiguration end call -" + getImplementationClass());		
	}

	public static void saveRestoreDirectories(String targetDirectory, String sourceDirectory) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveRestoreDirectories begin call - " + getImplementationClass());

		service.saveRestoreDirectories(targetDirectory, sourceDirectory);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveRestoreDirectories end call - " + getImplementationClass());
		
	}
	
	public static void checkConnection() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.checkConnection begin call - " + getImplementationClass());
		
		service.checkConnection();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.checkConnection end call - " + getImplementationClass());
		
	}

	public static List<String> getCloudDirectories(int level) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getCloudDirectories begin call - " + getImplementationClass());

		List<String> ret = service.getCloudDirectories(level);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getCloudDirectories end call - " + getImplementationClass());

		return ret;
		
	}

	public static List<Directory> getBackupDirectories() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getBackupDirectories begin call - " + getImplementationClass());
		
		List<Directory> ret =  service.getBackupDirectories();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getBackupDirectories end call - " + getImplementationClass());

		return ret;
		
	}

	public static void saveBackupDirectories(String[] dirs, String[] cloudDirs) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveBackupDirectories begin call - " + getImplementationClass());
		
		service.saveBackupDirectories(dirs, cloudDirs);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.saveBackupDirectories end call -" + getImplementationClass());		
	}
	
	public static ProgressStatus getProgress() throws MediaPieException {

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getProgress begin call - " + getImplementationClass());
		
		ProgressStatus status = service.getProgress();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getProgress end call - " + getImplementationClass());

		return status;
	}

	public static boolean checkValidFolder(String folder) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.checkValidFolder begin call - " + getImplementationClass());

		boolean ret = service.checkValidFolder(folder);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.checkValidFolder end call - " + getImplementationClass());

		return ret;
	}

	public static boolean validEncryptionKey(String key) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.validEncryptionKey begin call - " + getImplementationClass());

		boolean ret = service.validEncryptionKey(key);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.validEncryptionKey end call - " + getImplementationClass());

		return ret;
	}

	public static String generateEncryptionKey() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.generateEncryptionKey begin call - " + getImplementationClass());

		String ret = service.generateEncryptionKey();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.generateEncryptionKey end call - " + getImplementationClass());

		return ret;
	}	

	public static boolean login(String username, String password) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.login begin call - " + getImplementationClass());

		boolean ret = service.login(username, password);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.login end call - " + getImplementationClass());

		return ret;
	}

	public static boolean changePassword(String username, String oldPassword, String newPassword) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.change password begin call - " + getImplementationClass());

		boolean ret = service.changePassword(username, oldPassword, newPassword);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.change password end call - " + getImplementationClass());

		return ret;
	}


	public static List<MediaFile> getFoldersRoots() throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getFoldersRoots begin call - " + getImplementationClass());

		List<MediaFile> files = service.getFoldersRoots();

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getFoldersRoots end call - " + getImplementationClass());

		return files;
	}


	public static List<MediaFile> getFolders(String path) throws MediaPieException {
		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getFolders begin call - " + getImplementationClass());

		List<MediaFile> files = service.getFolders(path);

		if (log.isDebugEnabled())
			log.debug("Service ServiceHelper.getFolders end call - " + getImplementationClass());

		return files;
	}
	
}