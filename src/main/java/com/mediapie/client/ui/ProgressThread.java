package com.mediapie.client.ui;

import java.io.*;
import java.util.*;
import com.mediapie.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.mediapie.util.*;

public class ProgressThread extends Thread {
	static org.apache.commons.logging.Log log = Logger.getLog(ProgressThread.class);
	ProgressScreenController controller;
	int interval = 5000;
	boolean stop = false;
	
	public ProgressThread(ProgressScreenController controller) {
		super("Client Progress Thread");
		this.controller = controller;
	}

	public void signalStop() {
		stop = true;
	}
	
	public void run() {
		while(!stop) {
			try {
				ProgressStatus status = ServiceHelper.getProgress();
				controller.updateStatus(status);

				//check to see if the progress window is available
				if (com.mediapie.server.Process.isRunning()) {
					ScreensController parent = controller.getScreenParent();
					if (parent != null) {
						ControlledScreen current = parent.getCurrentScreen();
						if (current == null || !(current instanceof ProgressScreenController)) {
							parent.setScreen(MainUI.screen3AID);
						}
					}
				}
			} catch(Exception e) {
				log.error(e);
			}

			try {
				sleep(interval);
			} catch(InterruptedException ie) {
				break;
			}
		}
	}
}