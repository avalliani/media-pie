package com.mediapie.client.ui;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.fxml.*;
import java.io.*;
import java.util.*;
import java.net.*;
import com.mediapie.util.*;
import com.mediapie.client.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;

public class DownloadProgressScreenController extends ProgressScreenController implements Initializable , ControlledScreen  {
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		monitorProgress(this);
	}

	/*
	@FXML
	protected void handleCancelButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen1ID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}
	}
	*/
	
	@FXML
	protected void handleBackButtonAction(ActionEvent event) {
		try {
			myController.setScreen(MainUI.screen1BID);
		} catch(Exception e) {
			DialogHelper.showUnknownException(this, e);
		}			
	}

	public void start() {
		DialogHelper.ERROR = false;				
	}	
}