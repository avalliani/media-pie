package com.mediapie;

import org.apache.commons.lang3.exception.ContextedException;
import org.apache.commons.lang3.exception.ContextedRuntimeException;

public class MediaPieException extends ContextedRuntimeException
{
    public MediaPieException(String name, Exception e)
    {
		super(name, e);
    }

	public MediaPieException(String name)
	{
		super(name);
	}
	
    public MediaPieException(Exception e)
    {
		super(e);
    }
}