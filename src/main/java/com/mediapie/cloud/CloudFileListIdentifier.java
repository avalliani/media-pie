package com.mediapie.cloud;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public class CloudFileListIdentifier {
	String prefix;

	public String getPrefix() { return prefix;}
	public CloudFileListIdentifier withPrefix(String prefix) {
		this.prefix = prefix;
		return this;
	}
}

