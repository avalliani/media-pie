package com.mediapie.cloud;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public abstract class CloudSetup {
	boolean disableRetry = false;

	public boolean getDisableRetry() {return disableRetry;}
	public void setDisableRetry(boolean disableRetry) {this.disableRetry = disableRetry;}
}

