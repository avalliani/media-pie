package com.mediapie.cloud;

import org.apache.commons.lang3.exception.ContextedException;
import org.apache.commons.lang3.exception.ContextedRuntimeException;

public abstract class CloudException extends ContextedRuntimeException
{
    public CloudException(String name, Exception e)
    {
		super(name, e);
    }

	public CloudException(Exception e)
	{
		super(e);
	}
	
	public abstract boolean isRetryable();
	public abstract boolean isFileSpecific();	
	public abstract boolean isAborted();	
}