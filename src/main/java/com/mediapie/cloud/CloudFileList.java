package com.mediapie.cloud;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public abstract class CloudFileList {
	protected List<CloudFile> coll = new ArrayList<CloudFile>();

	public List<CloudFile> getFiles() {
		return coll;
	}

	public List<CloudFile> filterByDirectory(String directory) {
		List<CloudFile> ret = new ArrayList<CloudFile>();

		for (CloudFile cloudFile : coll) {			
			if (cloudFile.getFilePath().startsWith(directory))
				ret.add(cloudFile);
		}

		return ret;
	}
	
	public List<CloudFile> minus(List<String> files) {
		HashSet<String> processed = new HashSet<String>(files);
		List<CloudFile> ret = new ArrayList<CloudFile>();
		
		for (CloudFile cloudFile : coll) {
			if (!processed.contains(cloudFile.getFilePath()))
				ret.add(cloudFile);
		}

		return ret;
	}

	public List<String> getDirectories() {
		Set<String> unique = new HashSet<String>();
		
		for (CloudFile file : coll) {
			unique.add(FilePaths.getPathOnly(file.getFilePath()));
		}

		return new ArrayList<String>(unique);
	}
	
}
