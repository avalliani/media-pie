package com.mediapie.cloud.s3;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;


public class S3CloudFile extends CloudFile {
	public S3CloudFile(String key, Long size, Date lastModified) {
		super.lastModified = lastModified;
		super.size = size;
		super.filePath = convertToLocal(key);
		setDirectory(false);
	}

	public S3CloudFile(String key, Long size, Date lastModified, Boolean isDirectory) {
		super.lastModified = lastModified;
		super.size = size;
		super.filePath = convertToLocal(key);
		super.isDirectory = isDirectory;
	}
	
	String convertToLocal(String key) {
		return key;
	}

	public String getKey() {return filePath;}	
}
