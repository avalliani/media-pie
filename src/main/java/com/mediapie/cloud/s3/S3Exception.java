package com.mediapie.cloud.s3;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.amazonaws.*;

public class S3Exception extends CloudException {
	static String fileSpecificMessage = "has a different length than the expected";
	static org.apache.commons.logging.Log log = Logger.getLog(S3Exception.class);
	
	public S3Exception(String message, Exception e) {
		super(message + "[" + e.getMessage() + "]", e);
	}
	
	public S3Exception(Exception ae) {
		super(ae);
	}
	
	public boolean isRetryable() {
		Throwable cause = getCause();
		if (cause != null && cause instanceof AmazonClientException) {
			if (cause instanceof AmazonServiceException) {
				//check for auth related errors
				AmazonServiceException ase = (AmazonServiceException) cause;
				String error = ase.getErrorCode();
				if (log.isDebugEnabled())
					log.debug("Found the following exception from AWS:" + error + ase.getMessage());
				
				if (("InvalidAccessKeyId").equals(error) ||
				    ("SignatureDoesNotMatch").equals(error))
					return false;
			}
			return ((AmazonClientException)getCause()).isRetryable();
		}
		else {
			return false;
		}
	}

	public boolean isFileSpecific() {
		Throwable cause = getCause();
		if (cause instanceof AmazonClientException) {
			AmazonClientException clientException = (AmazonClientException) cause;
			String message = clientException.getMessage();
			if (message != null && message.indexOf(fileSpecificMessage) > 0)
				return true;
			if (clientException.getCause() instanceof java.io.FileNotFoundException)
				return true;
		}

		return false;
	}

	public boolean isAborted() {
		Throwable cause = getCause();
		if (cause instanceof com.amazonaws.AbortedException)
			return true;
		else
			return false;
	}
	
}

