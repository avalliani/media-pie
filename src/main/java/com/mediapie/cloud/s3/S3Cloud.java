package com.mediapie.cloud.s3;

import java.io.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.amazonaws.*;
import com.amazonaws.auth.*;
import com.amazonaws.services.s3.*;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.retry.*;

public class S3Cloud extends Cloud {
    AmazonS3Client client;
    S3CloudSetup setup;
	HashSet<String> folders = new HashSet<String>();
	static org.apache.commons.logging.Log log = Logger.getLog(S3Cloud.class);
	
	public S3Cloud() {}

	public S3Cloud(CloudSetup setup) throws CloudException {
		initialize(setup);
	}

	public Integer exactEncryptionBits() {
		return 256;
	}
	
	public void checkConnection(CloudSetup cloudSetup) throws CloudException {
		cloudSetup.setDisableRetry(true);

		//initialize without retry
		initialize(cloudSetup);
		
		S3FileList fileList = new S3FileList();
		final String bucketName = setup.getBucketName();
		
		Void res = (new S3ExceptionHandler<Void>() {
			public Void executeWithRetry() throws S3Exception {
				try {
					client.listObjects(bucketName);
					return null;
				} catch(AmazonServiceException ase) {
					throw new S3Exception("Error checkConnection",ase);
				} catch(AmazonClientException ae) {
					throw new S3Exception("Error checkConnection",ae);
				} catch(Exception e) {
					throw new S3Exception(e);
				}
			}
		}).execute(0, 0); //no retries and timeout		
	}
	
    public void initialize(CloudSetup setup) throws CloudException {
		S3CloudSetup s3Setup = (S3CloudSetup) setup;
		this.setup = s3Setup;
		
		ClientConfiguration clientConfiguration = new ClientConfiguration();
		if (!setup.getDisableRetry())
			clientConfiguration.withRetryPolicy(PredefinedRetryPolicies.getDefaultRetryPolicyWithCustomMaxRetries(3));		

		try {
			BasicAWSCredentials credentials = new BasicAWSCredentials(s3Setup.getAccessKey(), s3Setup.getSecretKey());
			if (s3Setup.getEncryptionKey() == null || "".equals(s3Setup.getEncryptionKey().trim())) {
				client = new AmazonS3Client(credentials, clientConfiguration);				
			} else {
				// Construct an instance of AmazonS3EncryptionClient
				EncryptionMaterials encryptionMaterials = new EncryptionMaterials(EncryptionUtils.getSymmetricKey(s3Setup.getEncryptionKey()));
				client = new AmazonS3EncryptionClient(credentials, encryptionMaterials, clientConfiguration, new CryptoConfiguration());				
			}

			final String s3BucketName = s3Setup.getBucketName();

			Bucket res = (new S3ExceptionHandler<Bucket>() {
				public Bucket executeWithRetry() throws S3Exception {
					try {
						if (!client.doesBucketExist(s3BucketName)) {
							return client.createBucket(s3BucketName);
						} else {
							return null;
						}
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error creating bucket " + s3BucketName,ase);
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error creating bucket " + s3BucketName,ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();		
						
		} catch(AmazonServiceException ase) {
			throw new S3Exception(ase);
		} catch(AmazonClientException ae ) {
			throw new S3Exception(ae);			
		} 
	}

	public CloudFileList getFileList() throws CloudException {
		return getFileList(null, null);
	}
	
	public CloudFileList getFileList(CloudLogger logger) throws CloudException {
		return getFileList(null, logger);
	}

	public CloudFileList getDirectories(CloudLogger logger) throws CloudException {
		return getDirectories(Integer.MAX_VALUE, logger);
	}
	
	public CloudFileList getDirectories(int level, CloudLogger logger) throws CloudException {
		S3FileList fileList = new S3FileList();
		
		List<String> dirs = getDirectoriesInternal(logger);
		
		if (dirs != null) {
			for (String dir : dirs) {
				if (logger != null) {
					if (logger.isStopSignaled())
						throw new S3Exception(new InterruptedException("interrupted directory list"));

					logger.log("directory", dir);
				}


				int dirLevel = org.apache.commons.lang3.StringUtils.countMatches(dir, "/");

				if (dirLevel <= level) {
					fileList.add(dir,
								 null, null);
				}
			}
		}

		return fileList;
	}
	
	protected List<String> getDirectoriesInternal(CloudLogger logger) throws CloudException {

		try {
			final ListObjectsRequest list = new ListObjectsRequest()
											.withBucketName(setup.getBucketName())
											.withDelimiter(".directory");
			
			ObjectListing objects = (new S3ExceptionHandler<ObjectListing>() {
				public ObjectListing executeWithRetry() throws S3Exception {
					try {
						return client.listObjects(list);
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error listing objects",ase);
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error listing objects ", ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();		

			List<String> prefixes = objects.getCommonPrefixes();

			return prefixes;
		} catch(AmazonServiceException ase) {
			throw new S3Exception(ase);
		} catch(AmazonClientException ae ) {
			throw new S3Exception(ae);			
		}
	}
	
	public CloudFileList getFileList(CloudFileListIdentifier identifier,
									 CloudLogger logger) throws CloudException {
		S3FileList fileList = new S3FileList();

		try {
			final CloudFileListIdentifier finalIdentifier = identifier;
			final String bucketName = setup.getBucketName();
			
			ObjectListing objects = (new S3ExceptionHandler<ObjectListing>() {
				public ObjectListing executeWithRetry() throws S3Exception {
					try {
						if (finalIdentifier != null) {
							ListObjectsRequest list = new ListObjectsRequest()
								.withBucketName(bucketName)
								.withPrefix(finalIdentifier.getPrefix());				
							return client.listObjects(list);
						} else {
							return client.listObjects(bucketName);
						}
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error listing objects",ase);						
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error listing objects ", ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();		
						
			if (logger != null)
				logger.log("log", "Reading file list from Cloud");
			
			do {
				for (S3ObjectSummary objectSummary : objects.getObjectSummaries()) {
					final S3ObjectSummary finalObjectSummary = objectSummary;
					
					ObjectMetadata meta = (new S3ExceptionHandler<ObjectMetadata>() {
						public ObjectMetadata executeWithRetry() throws S3Exception {
							try {
								return client.getObjectMetadata(bucketName, finalObjectSummary.getKey());
							} catch(AmazonServiceException ase) {
								throw new S3Exception("Error getting metadata",ase);								
							} catch(AmazonClientException ae) {
								throw new S3Exception("Error getting metadata ", ae);
							} catch(Exception e) {
								throw new S3Exception(e);
							}
						}
					}).execute();	

					if (log.isDebugEnabled())
						log.debug("Original Key:" + objectSummary.getKey());
					
					if (logger != null) {
						if (logger.isStopSignaled())
							throw new S3Exception(new InterruptedException("interrupted file list"));

						logger.log("file", objectSummary.getKey());
					}

					Boolean isDirectory = false;
					final String originalKey = objectSummary.getKey();
					String key = objectSummary.getKey();
					
					if (key.endsWith(".directory")) {
						isDirectory = true;
						key = key.substring(0, key.indexOf(".directory"));
					}

					if (!key.endsWith("/") || isDirectory) {
						fileList.add(key,
									 getMetaAsLong(originalKey, meta, "unencryptedsize"),
									 getMetaAsDate(originalKey, meta, "datemodified"),
									 isDirectory);

						if (log.isDebugEnabled())
							log.debug("Modified Key: " + key + "\t" +
										   getMetaAsLong(originalKey, meta, "unencryptedsize") + "\t" +
										   getMetaAsDate(originalKey, meta, "datemodified") + "\t" + isDirectory);
					}
				}

				final ObjectListing argObjects = objects;
				
				objects = (new S3ExceptionHandler<ObjectListing>() {
					public ObjectListing executeWithRetry() throws S3Exception {
						try {
							return client.listNextBatchOfObjects(argObjects);
						} catch(AmazonServiceException ase) {
							throw new S3Exception("Error getting metadata",ase);							
						} catch(AmazonClientException ae) {
							throw new S3Exception("Error getting metadata ", ae);
						} catch(Exception e) {
							throw new S3Exception(e);
						}
					}
				}).execute();
		
			} while (objects.isTruncated());
		} catch(AmazonServiceException ase) {
			throw new S3Exception(ase);
		} catch(AmazonClientException ae ) {
			throw new S3Exception(ae);			
		}
		
		return fileList;		
	}

	public Long getMetaAsLong(String key, ObjectMetadata meta, String metaKey) {
		String val = meta.getUserMetaDataOf(metaKey);
		try {
			return Long.parseLong(val);
		} catch (Exception e) {
			throw new S3Exception ("Key:" + key + " Expecting long for " + metaKey + " found " + val, e);
		}							   
	}

	public Date getMetaAsDate(String key, ObjectMetadata meta, String metaKey) {
		String val = meta.getUserMetaDataOf(metaKey);
		try {
			return new Date(Long.parseLong(val));
		} catch (Exception e) {
			throw new S3Exception ("Key:" + key + " Expecting date(long) for " + metaKey + " found " + val, e);
		}							   
	}
	
	public void upload(final File file) throws CloudException {
		
		final String key = FilePaths.normalize(file.getAbsolutePath());
		
		try {
			createDirectoryForFile(file);
			
			final AmazonS3Client finalClient = client;
			final String bucketName = setup.getBucketName();
			
			PutObjectResult res = (new S3ExceptionHandler<PutObjectResult>() {
				public PutObjectResult executeWithRetry() throws S3Exception {
					try {
						ObjectMetadata meta = new ObjectMetadata();
						meta.addUserMetadata("datemodified", Long.toString(file.lastModified()));
						meta.addUserMetadata("unencryptedsize", Long.toString(file.length()));

						PutObjectRequest put = new PutObjectRequest(bucketName, key, file);
						put.setMetadata(meta);
						
						return client.putObject(put);
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error uploading file: " + ((file != null) ? file.getAbsolutePath() : "null"), ase);
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error uploading file: " + ((file != null) ? file.getAbsolutePath() : "null"),
							ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();
			
			//PutObjectResult res = s3Helper.putObject(client, put);
		} catch(AmazonServiceException ase) {
			throw new S3Exception("Error uploading file: " + ((file != null) ? file.getAbsolutePath() : "null"), ase);
		} catch(AmazonClientException ae) {
			throw new S3Exception("Error uploading file: " + ((file != null) ? file.getAbsolutePath() : "null"), ae);
		}
	}

	public void createDirectoryForFile(File dir) throws CloudException {
		String key = FilePaths.normalize(dir.getAbsolutePath());
		String folder = FilePaths.getPathOnly(key);

		if (log.isDebugEnabled())
			log.debug("Cloud creating directory:" + folder + " for " + dir.getAbsolutePath() + " Key:" + key);
		
		createFolderRecursive(folder, dir.lastModified());
	}
	
	public void createDirectory(CloudFile dir) throws CloudException {
		createDirectory(dir.getFilePath(),
						(dir.getLastModified() != null) ? dir.getLastModified().getTime():null);
	}
	
	public void createDirectory(File dir) throws CloudException {
		createDirectory(dir.getAbsolutePath(), dir.lastModified());
	}

	public void createDirectory(String path, Long lastModified) throws CloudException {
		String key = FilePaths.normalize(path);
		if (!key.endsWith("/"))
			key = key + "/";

		String folder = FilePaths.getPathOnly(key);

		if (log.isDebugEnabled())
			log.debug("Cloud creating directory:" + folder + " for " + path + " Key:" + key);

		createFolderRecursive(folder, lastModified);
	}
	
	public void renameDirectory(CloudFile from, CloudFile to) throws CloudException {
		createDirectory(to);
		deleteDirectory(from);
	}
	
	public void deleteDirectory(CloudFile dir) throws CloudException {
		String filePath = dir.getFilePath();
		String modifiedFilePath = filePath;
		
		if (!filePath.endsWith(".directory")) {
			if(!filePath.endsWith("/"))
				modifiedFilePath = modifiedFilePath + "/";
			modifiedFilePath = modifiedFilePath + ".directory";
		}
		
		dir.setFilePath(modifiedFilePath);

		deleteFile(dir);
	}
	
	protected void createFolderRecursive(String folder, Long lastModified) throws CloudException {
		String temp = folder;
		int start = 0;
		while(temp.indexOf("/", start) >= 0) {
			start = temp.indexOf("/", start);
			String name = temp.substring(0, start + 1);
			createFolder(name, lastModified);
			start = start + 1;
		}
	}
	
	protected void createFolder(final String folder, final Long lastModified) throws CloudException {
		try {
			if (folders.contains(folder))
				return;
			
			final String directory = folder + ".directory";
			final String bucketName = setup.getBucketName();
			
			if (log.isDebugEnabled())
				log.debug("Storing directory:" + directory);
						
			PutObjectResult res = (new S3ExceptionHandler<PutObjectResult>() {
				public PutObjectResult executeWithRetry() throws S3Exception {
					try {
						InputStream is = new ByteArrayInputStream(directory.getBytes());			
						Long length = Long.valueOf(directory.getBytes().length);

						ObjectMetadata metadata = new ObjectMetadata();
						metadata.setContentLength(length);
						metadata.addUserMetadata("datemodified", Long.toString(lastModified));
						metadata.addUserMetadata("unencryptedsize", length.toString());

						final PutObjectRequest put = new PutObjectRequest(bucketName, directory, is, metadata);
						
						return client.putObject(put);
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error putObject ", ase);
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error putObject ", ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();		
			
			folders.add(folder);			
		} catch(AmazonServiceException ase) {
			throw new S3Exception(ase);
		} catch(AmazonClientException ae) {
			throw new S3Exception(ae);
		}
	}	
			
	public void download(CloudFile file, File path) throws CloudException {
		try {
			if (log.isDebugEnabled())
				log.debug("S3 Downloading file with key:" + file.getFilePath());

			final String bucketName = setup.getBucketName();
			final String filePath = file.getFilePath();
			
			S3Object downloadedObject = (new S3ExceptionHandler<S3Object>() {
				public S3Object executeWithRetry() throws S3Exception {
					try {
						return client.getObject(bucketName, filePath);						
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error getObject " + filePath, ase);
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error getObject " + filePath, ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();
			
			java.io.InputStream stream = downloadedObject.getObjectContent();
			org.apache.commons.io.IOUtils.copy(downloadedObject.getObjectContent(), new FileOutputStream(path));
		} catch(AmazonServiceException ase) {
			throw new S3Exception(ase);
		} catch(AmazonClientException ae) {
			throw new S3Exception(ae);			
		} catch(FileNotFoundException fe) {
			throw new S3Exception(fe);
		} catch(IOException ie) {
			throw new S3Exception(ie);
		}
	}
	
	public void deleteFile(CloudFile file) throws CloudException {
		try {
			String key = file.getFilePath();
			
			if (file instanceof S3CloudFile)
				key = ((S3CloudFile) file).getKey();
			
			if (log.isDebugEnabled())
				log.debug("S3 Deleting file:" + key);

			final String bucketName = setup.getBucketName();
			final String finalKey = key;
			
			(new S3ExceptionHandler<Void>() {
				public Void executeWithRetry() throws S3Exception {
					try {
						DeleteObjectRequest deleteRequest = new DeleteObjectRequest(bucketName, finalKey);
						client.deleteObject(deleteRequest);

						return null;
					} catch(AmazonServiceException ase) {
						throw new S3Exception("Error deleteObject " + finalKey, ase);
					} catch(AmazonClientException ae) {
						throw new S3Exception("Error deleteObject " + finalKey, ae);
					} catch(Exception e) {
						throw new S3Exception(e);
					}
				}
			}).execute();
		} catch(AmazonServiceException ase) {
			throw new S3Exception(ase);			
		} catch(AmazonClientException ae) {
			throw new S3Exception(ae);			
		}		
	}
}

