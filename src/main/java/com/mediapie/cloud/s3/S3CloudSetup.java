package com.mediapie.cloud.s3;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class S3CloudSetup extends CloudSetup {
	public static final String ACCESS_KEY = "AccessKey";
	public static final String SECRET_KEY = "SecretKey";
	public static final String ENCRYPTION_KEY = "EncryptionKey";
	public static final String BUCKET = "Bucket";
	
	String accessKey;
    String secretKey;
	String encryptionKey;
    String bucketName;

	public S3CloudSetup() {}

	public S3CloudSetup(Properties props) {
		accessKey = props.getProperty(ACCESS_KEY);
		secretKey = props.getProperty(SECRET_KEY);
		bucketName = props.getProperty(BUCKET);
		encryptionKey = props.getProperty(ENCRYPTION_KEY);
	}
	
    public void init(String accessKey, String secretKey, String encryptionKey, String bucketName) {
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.bucketName = bucketName;
		this.encryptionKey = encryptionKey;
	}

	public String getAccessKey () { return accessKey; }
	public String getSecretKey() { return secretKey; }
	public String getEncryptionKey() { return encryptionKey; }
	public String getBucketName() { return bucketName; }

	public void setAccessKey (String accessKey) {this.accessKey = accessKey;}
	public void setSecretKey(String secretKey) {this.secretKey = secretKey;}
	public void setEncryptionKey(String encryptionKey) {this.encryptionKey = encryptionKey;}
	public void setBucketName(String bucketName) {this.bucketName = bucketName;}	
}

