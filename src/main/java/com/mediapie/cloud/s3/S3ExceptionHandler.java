package com.mediapie.cloud.s3;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.amazonaws.*;

public abstract class S3ExceptionHandler<T> extends CloudExceptionHandler<T> {
	public T execute() throws CloudException {
		return execute(defaultRetryAttempts, defaultRetryTimeout);
	}
}

