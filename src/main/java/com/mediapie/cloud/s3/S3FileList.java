package com.mediapie.cloud.s3;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class S3FileList extends CloudFileList {
    
    public void add (S3CloudFile item) {
		coll.add(item);
    }

    public void add (String key, Long size, Date lastModified) {
		coll.add(new S3CloudFile(key, size, lastModified));
	}

	public void add (String key, Long size, Date lastModified, Boolean isDirectory) {
		coll.add(new S3CloudFile(key, size, lastModified, isDirectory));
	}
	
}


