package com.mediapie.cloud;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public abstract class CloudLogger {
	boolean stopped = false;
	
	public void stop () { stopped = true; }				

	public boolean isStopSignaled () { return stopped;}				
	
	public abstract void log(String category, String log);	
}

