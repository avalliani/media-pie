package com.mediapie.cloud;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public abstract class CloudExceptionHandler<T> {
	protected final int defaultRetryAttempts = 3;
	protected final long defaultRetryTimeout = 30000;
	static org.apache.commons.logging.Log log = Logger.getLog(CloudExceptionHandler.class);
	
	public abstract T executeWithRetry() throws CloudException;

	public T execute() throws CloudException {
		return execute(defaultRetryAttempts, defaultRetryTimeout);
	}
	
	public T execute(int retryAttempts, long retryTimeout) throws CloudException {
		T result = null;
		int totalAttempts = retryAttempts + 1;
		
		for (int i=0; i < totalAttempts; i++ ) {
			try {
				result = executeWithRetry();
			} catch(CloudException ce) {
				if (!ce.isAborted())
					log.error("Cloud Exception " + this.getClass().getName(), ce);
				else {
					if (log.isInfoEnabled())
						log.info("Aborted " + ce.getMessage());
				}
				
				if (!ce.isRetryable() || i == (totalAttempts - 1))
					throw ce;
				else {
					try {
						if (log.isDebugEnabled())
							log.debug("Sleep for " + retryTimeout + " on " + this.getClass().getName());
						Thread.currentThread().sleep(retryTimeout);
					} catch(InterruptedException ie) {break;}
				}
			}
		}

		return result;		
	}
}

