package com.mediapie.cloud;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public class CloudFile {
	protected String filePath;
	protected Long size;
	protected Date lastModified;
	protected Boolean isDirectory;

	public static CloudFile createDirectory(String filePath, Date lastModified) {
		CloudFile file = new CloudFile();
		file.setFilePath(filePath);
		file.setDirectory(true);
		file.setLastModified(lastModified);

		return file;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public Boolean isDirectory() {
		return isDirectory;
	}

	public void setDirectory(Boolean isDirectory) {
		this.isDirectory = isDirectory;		
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;		
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;		
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;		
	}	
}
