package com.mediapie.cloud;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;

public abstract class Cloud {
	public abstract void checkConnection(CloudSetup setup) throws CloudException;
	public abstract void initialize(CloudSetup setup) throws CloudException;
	public abstract CloudFileList getDirectories(int level, CloudLogger logger) throws CloudException;	
	public abstract CloudFileList getDirectories(CloudLogger logger) throws CloudException;	
	public abstract CloudFileList getFileList() throws CloudException;	
	public abstract CloudFileList getFileList(CloudLogger logger) throws CloudException;	
	public abstract CloudFileList getFileList(CloudFileListIdentifier identifier, CloudLogger logger) throws CloudException;        
	public abstract void createDirectory(File dir) throws CloudException;
	public abstract void renameDirectory(CloudFile from, CloudFile to) throws CloudException;
	public abstract void deleteDirectory(CloudFile dir) throws CloudException;
	public abstract void upload(final File file) throws CloudException;
	public abstract void download(CloudFile file, File path) throws CloudException;
	public abstract void deleteFile(CloudFile file) throws CloudException;
	public abstract Integer exactEncryptionBits();
}

