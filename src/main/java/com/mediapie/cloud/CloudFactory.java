package com.mediapie.cloud;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.s3.*;

public class CloudFactory {
	public static Cloud newInstance(Properties props) throws CloudException {
		return new S3Cloud(new S3CloudSetup(props));
	}

	public static Cloud newInstance() throws CloudException {
		return new S3Cloud();
	}

	public static CloudSetup getCloudSetup(Properties props) {
		return new S3CloudSetup(props);
	}
}

