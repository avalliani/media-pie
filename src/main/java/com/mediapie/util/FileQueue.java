package com.mediapie.util;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

public class FileQueue<File> {

	LinkedBlockingQueue<File> queue = new LinkedBlockingQueue<File>();
	List<File> listOfFiles = new ArrayList<File>();	
    boolean closed = false;
    
    public FileQueue() {
    }

    public LinkedBlockingQueue<File> getQueue() {
		return queue;	
    }

	public void add(File file) {
		queue.add(file);
		listOfFiles.add(file);
	}
	
    public void closeQueue() {
		closed = true;
    }

    public boolean isClosed() {
		return closed;
	}

	public List<File> getFileList() {
		return listOfFiles;
	}
}