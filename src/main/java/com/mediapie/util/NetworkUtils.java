package com.mediapie.util;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.ExecuteException;

public class NetworkUtils {
	public static String getHostName() {
		String hostname = null;
						  
		try {
			hostname = getHostNameWindows();
			
			if (hostname != null) {
				return hostname;
			} else {
				hostname = getHostNameEnv();
				if (hostname != null) {
					return hostname;
				} else {
					hostname = getHostNameCommandLine();

					if (hostname != null) {
						return hostname;
					}
				}
			}
		} catch(Exception e) {
			hostname = getHostNameNIC();
			if (hostname != null)
				return hostname;				
		}

		return null;
	}

	public static String getHostNameEnv() {
		return System.getenv("HOSTNAME");
	}
	
	public static String getHostNameWindows() {
		if (System.getProperty("os.name").startsWith("Windows")) {
				// Windows will always set the 'COMPUTERNAME' variable
			return System.getenv("COMPUTERNAME");
		}

		return null;
	}
	
	public static String getHostNameNIC() {
		try {
			String hostname = InetAddress.getLocalHost().getHostName();
			if (hostname != null)
				return hostname;				
		} catch(Exception e2) {
			return null;
		}

		return null;
	}

	public static String getHostNameCommandLine() {
		try {
			ByteArrayOutputStream stdout = new ByteArrayOutputStream();
			PumpStreamHandler psh = new PumpStreamHandler(stdout);
			CommandLine cl = CommandLine.parse("hostname");
			DefaultExecutor exec = new DefaultExecutor();
			exec.setStreamHandler(psh);
			exec.execute(cl);

			String ret = stdout.toString();

			return ret;
		} catch(ExecuteException ee ) {
			return null;
		} catch(IOException ie) {
			return null;
		}
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("Windows:" + getHostNameWindows());
		System.out.println("Env:" + getHostNameEnv());
		System.out.println("NIC:" + getHostNameNIC());
		System.out.println("CommandLine:" + getHostNameCommandLine());
	}
}