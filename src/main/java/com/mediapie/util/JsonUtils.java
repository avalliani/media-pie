package com.mediapie.util;

import java.io.*;
import java.net.*;
import java.util.*;

public class JsonUtils {
	public static Object from(String json, Class clazz) {
		return (new com.google.gson.Gson()).fromJson(json, clazz);
	}

	public static String to(Object obj) {
		return (new com.google.gson.Gson()).toJson(obj);
	}
}