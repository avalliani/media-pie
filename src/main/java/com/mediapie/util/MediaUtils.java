package com.mediapie.util;

import java.io.*;
import java.util.*;

public class MediaUtils {
	static org.apache.commons.logging.Log log = Logger.getLog(MediaUtils.class);

	public static List<MediaFile> convertFiles(List<File> files, boolean isRoot) {
		List<MediaFile> list = new ArrayList<MediaFile>();
		if (files != null) {
			for (File f : files) {
				MediaFile mf = new MediaFile();
				mf.setName(f.getName());
				mf.setAbsolutePath(f.getAbsolutePath());
				mf.setLastModified(f.lastModified());
				mf.setIsRoot(isRoot);
				mf.setIsDirectory(f.isDirectory());
				list.add(mf);
			}
		}

		return list;
	}
}