package com.mediapie.util;

import org.apache.commons.logging.Log; 
import org.apache.commons.logging.LogFactory;

public class Logger {
	
	public static Log getLog(Class clazz) {
		return LogFactory.getLog(clazz);
	}

	public static Log getLog(String name) {
		return LogFactory.getLog(name);
	}
	
}

