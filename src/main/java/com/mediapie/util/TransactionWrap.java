package com.mediapie.util;

import java.util.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import com.avaje.ebean.*;

public abstract class TransactionWrap
{
	public abstract void run(Transaction t); 
}
