package com.mediapie.util;

import java.io.*;
import java.net.*;
import java.util.*;

public class MathUtils {

	public static double round(double v, int digits) {
		return org.apache.commons.math3.util.Precision.round(v, digits);
	}
}