package com.mediapie.util;

import java.io.*;
import java.util.*;

public class FileUtils {
	static org.apache.commons.logging.Log log = Logger.getLog(FileUtils.class);

	public static void listFiles(final List<? extends DirectoryAndFilter> directoryAndFilters,
								 FileQueue queue,
								 StopSignal stop,
								 boolean includeDirs) {

		for (DirectoryAndFilter directoryAndFilter : directoryAndFilters) {
			File directory = directoryAndFilter.getDirectory();
			FileFilter filter = directoryAndFilter.getFilter();

			if (log.isDebugEnabled())
				log.debug("Scanning directory:" + directory.getAbsolutePath());
			
			listFiles(directory, filter, queue, stop, includeDirs);

			//add the base path
			String path = FilePaths.normalizeOriginal(directory.getAbsolutePath());
			List<String> internalPaths = FilePaths.getRecursivePaths(path);
			for (String internalPath : internalPaths) {
				File file = new File(internalPath);
				if (log.isDebugEnabled())
					log.debug(Thread.currentThread().getName() + "-Adding base directory to queue:" + file.getAbsolutePath());
				queue.add(file);
			}
		}
	}
	
    public static void listFiles(final File directory,
								 final FileFilter filter,
								 FileQueue queue,
								 StopSignal stop,
								 boolean includeDirs) {
	
		final File[] found = directory.listFiles((FileFilter) filter);

		if (stop.signalled())
			return;
		
		if (found != null) {
			for (final File file : found) {
				if (stop.signalled())
					return;
				
				if (file.isDirectory()) {
					if (includeDirs) {
						if (log.isDebugEnabled())
							log.debug(Thread.currentThread().getName() + "-Adding directory to queue:" + file.getAbsolutePath());
						queue.add(file);
					}
					listFiles(file, filter, queue, stop, includeDirs);
				} else {
					if (log.isDebugEnabled())
						log.debug(Thread.currentThread().getName() + "-Adding file to queue:" + file.getAbsolutePath());
					queue.add(file);
				}
			}
		}	
	}

	public static boolean folderExists(String fileName) {
		if (fileName == null) return false;
		
		File file = new File(fileName);
		
		return file.isDirectory();		
	}

	public static boolean createDirectory(String directory) {
		try {
			org.apache.commons.io.FileUtils.forceMkdir(new File(directory));
			return true;
		} catch(IOException ie) {
			return false;
		}
	}

	public static boolean createDirectoryFile(String file) {
		try {
			//split the path
			String path = FilePaths.getPathOnly(file);
			org.apache.commons.io.FileUtils.forceMkdir(new File(path));
			return true;
		} catch(IOException ie) {
			return false;
		}
	}
	
	public static String getHomeDirectory() {
		return System.getProperty("user.home");
	}

	public static List<File> getRootFolders() {
		File[] roots = File.listRoots();

		return Arrays.asList(roots);
	}

	public static List<File> getFolders(String path) {
		File filePath = new File(path);

		File[] dirList = filePath.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isDirectory();
			}
		});

		if (dirList == null)
			return new ArrayList<File>();
		else 
			return Arrays.asList(dirList);
	}

	public static String getUpDirectory	(String absolutePath) {		
		boolean endingSlash = (absolutePath.endsWith("/") || absolutePath.endsWith("\\"));
		boolean isWindows = absolutePath.endsWith("\\") || (absolutePath.lastIndexOf("\\") > 0);

		if (isWindows) {
			int index = (endingSlash) ? absolutePath.lastIndexOf("\\", absolutePath.length() - 2) : absolutePath.lastIndexOf("\\");
			if (index == -1) return absolutePath;
			return absolutePath.substring(0, index) + "\\";
		} else {
			int index = (endingSlash) ? absolutePath.lastIndexOf("/", absolutePath.length() - 2) : absolutePath.lastIndexOf("/");
			if (index == -1) return absolutePath;
			return absolutePath.substring(0, index) + "/";
		}		
	}
}