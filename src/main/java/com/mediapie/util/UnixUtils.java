package com.mediapie.util;

import java.io.*;
import java.util.*;

public class UnixUtils {
	public static void changePassword(String username, String newPassword)
			throws IOException {
		
		String[] commands = new String[]{"sudo", "-S", "passwd" , username};

		ProcessBuilder builder = new ProcessBuilder(commands);
		builder.redirectErrorStream(true);
		Process process = builder.start();

		OutputStream outputStream = process.getOutputStream();
		InputStream inputStream = process.getInputStream();

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));

		//need to write twice and reconfirm
		writer.write(newPassword);
		writer.write("\n");
		writer.write(newPassword);
		writer.write("\n");
		writer.flush();

		String input = readFromStream(inputStream);
		
		if (input != null)
			throw new IOException(input);		
	}

	static String readFromStream(InputStream is) throws IOException{
		byte[] buffer = new byte[1024];
		int length = 0;

		if (is.available() > 0) {
			length = is.read(buffer);
		}
		byte[] ret = new byte[length];
		System.arraycopy(buffer, 0, ret, 0, length);

		return new String(ret);
	}

}