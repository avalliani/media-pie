package com.mediapie.util;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import org.apache.commons.io.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class FilePaths {
	public static String normalize(String localFilePath) {
		String normalize = FilenameUtils.normalize(localFilePath, true);

		return normalize;		
	}

	public static String normalizeOriginal(String localFilePath) {
		String normalize = FilenameUtils.normalize(localFilePath);

		return normalize;		
	}

	//returns the root path after normalizing it
	public static String normalizeRootPath(String path) {
		String normalized = normalize(path);
		int i = normalized.indexOf("/");
		
		if (i > 0) {
			return normalized.substring(0, i);
		}

		return normalized;
	}
	
	public static String getPathOnly(String path) {
		return FilenameUtils.getFullPath(path);
	}

	public static String replaceFilePath(String originalFile, String targetPath) {
		String filename= FilenameUtils.getName(originalFile);
		String relativePath = FilenameUtils.getPath(originalFile);
		String relativeFileName = FilenameUtils.concat(relativePath, filename);
		
		String normalizeTarget = normalize(targetPath);

		return FilenameUtils.concat(normalizeTarget, relativeFileName); 		
	}

	public static List<String> getRecursivePaths(String folder) {
		List<String> ret = new ArrayList<String>();
		ret.add(folder);
		
		String temp = folder;
		int start = 0;
		while(temp.indexOf(File.separator, start) >= 0) {
			start = temp.indexOf(File.separator, start);
			String name = temp.substring(0, start + 1);
			ret.add(name);
			start = start + 1;
		}

		return ret;
	}
}

