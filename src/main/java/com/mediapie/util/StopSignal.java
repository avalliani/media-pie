package com.mediapie.util;

import java.io.*;
import java.util.concurrent.*;
import org.apache.commons.io.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class StopSignal {
	boolean stop = false;

	public void stop() {
		stop = true;
	}

	public boolean signalled() { return stop;}
}

