package com.mediapie.util;

public class MediaFile implements java.io.Serializable
{
	String name;
	String absolutePath;
	Long lastModified;
	Boolean isDirectory;
	Boolean isRoot;
	
	public MediaFile() {};

	public MediaFile(String name) {
		this.name = name;
	}
	
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	public String getAbsolutePath() {return absolutePath;}
	public void setAbsolutePath(String absolutePath) {this.absolutePath = absolutePath;}
	public Long getLastModified() {return lastModified;}
	public Long lastModified() {return lastModified;}
	public void setLastModified(Long lastModified) {this.lastModified = lastModified;}
	public Boolean getIsDirectory() {return isDirectory;}
	public void setIsDirectory(Boolean isDirectory) {this.isDirectory = isDirectory;}
	public Boolean getIsRoot() {return isRoot;}
	public void setIsRoot(Boolean isRoot) {this.isRoot = isRoot;}

	public String toString() {
		if (Boolean.TRUE.equals(isRoot))
			return absolutePath;
		
		return name;
	}	
}
