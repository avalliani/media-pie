package com.mediapie.util;

import java.io.*;

public class DirectoryAndFilter {
	File directory;
	FileFilter filter;

	public DirectoryAndFilter() {}

	public DirectoryAndFilter(File directory, FileFilter filter) {
		this.directory = directory;
		this.filter = filter;
	}
	public void setDirectory(File directory) {this.directory = directory;}
	public void setFilter(FileFilter filter) {this.filter = filter;}
	public File getDirectory() { return directory; }
	public FileFilter getFilter() { return filter; }
}