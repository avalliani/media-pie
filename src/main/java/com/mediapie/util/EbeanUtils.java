package com.mediapie.util;

import java.util.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import com.avaje.ebean.*;

public class EbeanUtils
{
	static org.apache.commons.logging.Log log = Logger.getLog(EbeanUtils.class);
	
	public static void wrapTransaction(TxIsolation isolation,  TransactionWrap wrap) {
		Exception causedBy = null;
		Transaction t = null;

		try {
			t = Ebean.beginTransaction(isolation);
			wrap.run(t);
			Ebean.commitTransaction();
		} catch(Exception e) {
			log.error(e);
			causedBy = e;
		} finally {
			endTransaction(t, causedBy);
		}
	}

	public static void endTransaction(Transaction t, Exception causedBy) {
		try {
			Ebean.endTransaction();
		} catch(Exception f) {
			log.error(f);
			throw new TransactionWrapException(f, causedBy);
		}
		if (causedBy != null) {
			throw new TransactionWrapException(causedBy);
		}
	}

	public static void closeConnectionIfNecessary(Transaction t) {
		try {
			if (t != null) {
				Connection c = t.getConnection();
				if (c != null && !c.isClosed())
					c.close();
			}
		} catch(Exception e) {
			log.error("Error closing connection:", e);
		}
	}

	public static class TransactionWrapException extends
			org.apache.commons.lang3.exception.ContextedRuntimeException {

		Throwable t;
		
		public TransactionWrapException(Throwable e) {
			super(e);
			t = e;
		}

		public TransactionWrapException(Throwable e, Throwable causedby) {
			super(causedby);
			t = causedby;
		}

		public Throwable getCausedBy() {
			return t;
		}
	}

	public static class TransactionResult {
		public Object value;
	}
}
