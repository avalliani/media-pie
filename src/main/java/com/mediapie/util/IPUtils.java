package com.mediapie.util;

import java.io.*;
import java.net.*;
import java.util.*;

public class IPUtils {

	public static List<String> getIPAddresses() throws SocketException {
		List<String> ips = new ArrayList<String>();
		
		Enumeration e = NetworkInterface.getNetworkInterfaces();
		while(e.hasMoreElements())
		{
			NetworkInterface n = (NetworkInterface) e.nextElement();
			Enumeration ee = n.getInetAddresses();
			while (ee.hasMoreElements())
			{
				InetAddress i = (InetAddress) ee.nextElement();
				if (!i.isLoopbackAddress() && !i.isMulticastAddress() && i.isSiteLocalAddress()) {
					ips.add(i.getHostAddress());
					//System.out.println(i.getHostAddress() + " isAnyLocalAddress:" + i.isAnyLocalAddress() + " isSiteLocalAddress:" + i.isSiteLocalAddress());
				}
			}
		}

		return ips;
	}
	
	public static String getMacAddress() throws UnknownHostException, SocketException {
		InetAddress ip = InetAddress.getLocalHost();
		NetworkInterface network = NetworkInterface.getByInetAddress(ip);

		byte[] mac = network.getHardwareAddress();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
		}
		
		return sb.toString();
	}

	public static String getComputerName() throws UnknownHostException {
		String computerName = InetAddress.getLocalHost().getHostName();

		return computerName;
	}
}