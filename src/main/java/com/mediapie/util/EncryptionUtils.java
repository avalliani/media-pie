package com.mediapie.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.SecretKey;
import java.security.*;
import org.apache.commons.codec.binary.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.mindrot.jbcrypt.BCrypt;   
import jcifs.util.Hexdump;
import jcifs.util.MD4;

public class EncryptionUtils {

	private static final int IV_LENGTH=16;
	static org.apache.commons.logging.Log log = Logger.getLog(EncryptionUtils.class);

	public static String convertLDAPSHA(String text) throws NoSuchAlgorithmException {
		String label = "{SSHA}";
		String salt = generateSHASalt();

		MessageDigest sha = MessageDigest.getInstance("SHA");
		sha.update(text.getBytes());
		sha.update(salt.getBytes());
		byte[] pwhash = sha.digest();

		return label + new String(base64Encode(concatenate(pwhash, salt.getBytes())));
	}

	public static String convertLDAPMD4(String text)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		
		MD4 md4 = new MD4();
		byte[] bpass = text.getBytes("UTF-16LE");
		md4.engineUpdate(bpass, 0, bpass.length);        
		byte[] hashbytes = md4.engineDigest();
		String ntHash = Hexdump.toHexString(hashbytes, 0, hashbytes.length * 2);        
		return ntHash;
	}
	
	/* A helper - to reuse the stream code below - if a small String is to be encrypted */
	public static byte[] encrypt(String plainText, String password) throws Exception {
		ByteArrayInputStream bis = new ByteArrayInputStream(plainText.getBytes("UTF8"));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		encrypt(bis, bos, password);
		return bos.toByteArray();
	}


	public static byte[] decrypt(String cipherText, String password) throws Exception {
		byte[] cipherTextBytes = cipherText.getBytes();
		ByteArrayInputStream bis = new ByteArrayInputStream(cipherTextBytes);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		decrypt(bis, bos, password);		
		return bos.toByteArray();
	}


	public static void encrypt(InputStream in, OutputStream out, String password) throws Exception{

		SecureRandom r = new SecureRandom();
		byte[] iv = new byte[IV_LENGTH];
		r.nextBytes(iv);
		out.write(iv); //write IV as a prefix
		out.flush();
		//System.out.println(">>>>>>>>written"+Arrays.toString(iv));

		Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding"); //"DES/ECB/PKCS5Padding";"AES/CBC/PKCS5Padding"
		SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);    	

		out = new CipherOutputStream(out, cipher);
		byte[] buf = new byte[1024];
		int numRead = 0;
		while ((numRead = in.read(buf)) >= 0) {
			out.write(buf, 0, numRead);
		}
		out.close();
	}


	public static void decrypt(InputStream in, OutputStream out, String password) throws Exception{

		byte[] iv = new byte[IV_LENGTH];
		in.read(iv);
		//System.out.println(">>>>>>>>red"+Arrays.toString(iv));

		Cipher cipher = Cipher.getInstance("AES/CFB8/NoPadding"); //"DES/ECB/PKCS5Padding";"AES/CBC/PKCS5Padding"
		SecretKeySpec keySpec = new SecretKeySpec(password.getBytes(), "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);

		in = new CipherInputStream(in, cipher);
		byte[] buf = new byte[1024];
		int numRead = 0;
		while ((numRead = in.read(buf)) >= 0) {
			out.write(buf, 0, numRead);
		}
		out.close();
	}


	public static void copy(int mode, String inputFile, String outputFile, String password) throws Exception {

		BufferedInputStream is = new BufferedInputStream(new FileInputStream(inputFile));
		BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(outputFile));
		if(mode==Cipher.ENCRYPT_MODE){
			encrypt(is, os, password);
		}
		else if(mode==Cipher.DECRYPT_MODE){
			decrypt(is, os, password);
		}
		else throw new Exception("unknown mode");
		is.close();
		os.close();
	}

	public static String base64Encode(byte[] bytes) {
		return Base64.encodeBase64String(bytes);			
	}

	public static byte[] base64Decode(String string) {
		return Base64.decodeBase64(string);			
	}

	public static boolean validEncryptionKey(String key, Integer exact)
			throws NoSuchAlgorithmException {
		if (key == null) return false;
		
		byte[] bytes = base64Decode(key);
		
		int totalBits = bytes.length * 8;

		if (exact != null) {
			if (log.isDebugEnabled()) {
				log.debug("Required Encryption Exact " + exact + " Found:" + totalBits);
			}
			return (totalBits == exact);
		}
		
		int maxLengthBits = Cipher.getMaxAllowedKeyLength("AES");

		if (log.isDebugEnabled()) {
			log.debug("Found key: " + bytes + " with length:" + bytes.length + " with bits:" + totalBits + " with max allowed:" + maxLengthBits);
		}
		
		return totalBits <= maxLengthBits;		
	}
	
	public static String generateSecureKey(Integer exact) {
		SecureRandom random = new SecureRandom();
		int maxLengthBits = 256;
		
		try {
			maxLengthBits = Cipher.getMaxAllowedKeyLength("AES");
		} catch(NoSuchAlgorithmException ae) {
			log.error(ae);
		}

		if (exact != null) {
			if (log.isDebugEnabled()) {
				log.debug("Required Encryption Exact " + exact + " Found:" + maxLengthBits);
			}
			
			if (maxLengthBits < exact) {
				throw new IllegalArgumentException("Please upgrade JVM to install 256 bit encryption.");
			} else {
				maxLengthBits = exact;
			}
		}
		
		if (exact == null && maxLengthBits > 256)
			maxLengthBits = 256;

		byte bytes[] = new byte[(int)maxLengthBits/8];
		random.nextBytes(bytes);

		return base64Encode(bytes);		
	}

	public static SecretKey getSymmetricKey(String base64Key) {
		byte[] keyBytes = base64Decode(base64Key);
		if (log.isDebugEnabled())
			log.debug("Generated symmetric key with key size:" + keyBytes.length + " bytes:" + keyBytes);
		
		SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");

		return key;
	}

	public static String bcrypt(String password) {
		String salt = BCrypt.gensalt();
		return BCrypt.hashpw(password ,salt);
	}

	public static boolean bcryptCheck(String passwordHash, String password) {
		return BCrypt.checkpw(passwordHash, password);
	}

	public static String generateSHASalt() throws NoSuchAlgorithmException
	{
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt.toString();
	}

	private static byte[] concatenate(byte[] l, byte[] r) {
		byte[] b = new byte[l.length + r.length];
		System.arraycopy(l, 0, b, 0, l.length);
		System.arraycopy(r, 0, b, l.length, r.length);
		return b;
	}	
}