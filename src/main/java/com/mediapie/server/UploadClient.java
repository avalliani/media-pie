package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class UploadClient implements ProcessClient {
	
	int threads = 10;
	List<CloudDirectoryAndFilter> directories = new ArrayList<CloudDirectoryAndFilter>();
	Properties cloudProperties;
	CloudLogger cloudLogger = null;
	FileListReader fileListReader = null;
	UploadPool uploadPool = null;
	DeletePool deletePool = null;
	static org.apache.commons.logging.Log log = Logger.getLog(UploadClient.class);
			 
	public UploadClient(final int threads,
				  final List<CloudDirectoryAndFilter> directories,
				  final Properties cloudProperties) {

		this.threads = threads;
		this.directories = directories;
		this.cloudProperties = cloudProperties;

		cloudLogger = new CloudLogger() {
			public void log(String category, String log) {										
				if ("file".equals(category))
					Progress.setFileName("Cloud file:" + log );
				else
					Progress.writeLog(log);										
			}
		};
    }

	public void stop() {
		cloudLogger.stop();

		//cleanup
		try {
			if (fileListReader != null)
				fileListReader.stopReader();
		} catch(Exception e) {
			log.error("Error stopping file reader", e);
		}

		try {
			if (uploadPool != null)
				uploadPool.shutdown();
		} catch(Exception e) {
			log.error("Error stopping upload pool", e);
		}

		try {
			if (deletePool != null)
				deletePool.shutdown();
		} catch(Exception e) {
			log.error("Error stopping delete pool", e);
		}					
	}
	
	public void run() {
		
		try {
			Progress.reset();		

			//read the files on the cloud
			Cloud cloud = CloudFactory.newInstance(cloudProperties);
			
			//remap directories if required
			for (CloudDirectoryAndFilter dir : directories) {
				File localDir = dir.getDirectory();
				String cloudDir = dir.getCloudPath();
				if (cloudDir != null && !"".equals(cloudDir.trim())) {
					String localRootNormalized = FilePaths.normalizeRootPath(localDir.getAbsolutePath());
					String cloudRootNormalized = FilePaths.normalizeRootPath(cloudDir);

					if (!localRootNormalized.equals(cloudRootNormalized)) {
						//mapping is necessary

						if (log.isInfoEnabled())
							log.info("Remapping cloud path:" + cloudRootNormalized +
									 " to " + localRootNormalized);
						cloud.renameDirectory(CloudFile.createDirectory(cloudRootNormalized, new Date(localDir.lastModified())),
											  CloudFile.createDirectory(localRootNormalized, new Date(localDir.lastModified())));
					}
				}
			}
			
			CloudFileList cloudFileList = cloud.getFileList(cloudLogger);

			//for debug
			if (log.isDebugEnabled()) {
				List<CloudFile> cloudFiles = cloudFileList.getFiles();
				for(CloudFile file : cloudFiles) {
					log.debug("Received files from cloud:" + file.getFilePath());
				}
			}
			
			//process
			FileQueue queue = new FileQueue();
			fileListReader = new FileListReader(directories, queue);
			fileListReader.start();

			//start uploads
			uploadPool = new UploadPool(new PoolWatcher(), cloud, cloudFileList, queue, threads);
			uploadPool.start();

			//execution finished... cleanup cloud and delete files not
			//necessary
			List<String> fileList = uploadPool.getMatches();
			List<CloudFile> cloudDeleteFileList = cloudFileList.minus(fileList);
			LinkedBlockingQueue<CloudFile> deleteQueue = new LinkedBlockingQueue<CloudFile>(cloudDeleteFileList);

			deletePool = new DeletePool(new PoolWatcher(), cloud, deleteQueue, threads);
			deletePool.start();

			//we're done at this point
			//Progress.finish();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}

