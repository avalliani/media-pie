package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;

public class ProgressManager {
	public static void save(ProgressStatus status) {
		if (status.getId() != null)
			status.setId("main");
		Persistance.saveProgress(status);
	}

	public static void reset()  {
		Persistance.resetProgress();
	}

	public static ProgressStatus get() {
		ProgressStatus status = new ProgressStatus();

		status.setPercentage(Progress.getPercentage());
		status.setFileName(Progress.getFileName());
		status.setLog(Progress.getLog());
		status.setTotalFileCount(Progress.getTotalFileCount());
		status.setCurrentFileCount(Progress.getCurrentFileCount());
		status.setFinished(Progress.isFinished());
		status.setError(Progress.isError());			
		status.setErrorStack(Progress.getExceptionStack());
		status.setErrorMessage(Progress.getExceptionMessage());
		
		return status;
	}
}

