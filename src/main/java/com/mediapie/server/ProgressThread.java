package com.mediapie.server;

import java.io.*;
import java.util.*;
import com.mediapie.util.*;
import com.mediapie.model.*;

public class ProgressThread extends Thread {
	Notifier notifier;
	boolean stop = false;
	
	public ProgressThread () {
		super("Cloud Sync Progress");
		this.notifier = Progress.getNotifier();		
	}

	public void end() {
		this.stop = true;
		notifier.doNotify();
	}
	
	public void run() {
		while (!stop) {
			try {
				notifier.doWait();				
			} catch(InterruptedException ie) {
			}
		}
	}
}