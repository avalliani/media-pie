package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;

public interface UserManager {	
	//public void save(User user);

	public void resetPassword(String username, String oldPassword, String newPassword);
	
	public boolean authenticate(String username, String password);
}

