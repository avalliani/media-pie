package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class UploadPool extends Pool {

	FileQueue queue = null;
	Cloud cloud;
	CloudFileList cloudFileList;
	Match match;
	
	public UploadPool(PoolWatcher watcher,
					  Cloud cloud,
					  CloudFileList cloudFileList,
					  FileQueue queue,
					  int threads) {

		super(threads, watcher);
		
		this.queue = queue;
		this.cloud = cloud;
		this.cloudFileList = cloudFileList;
		match = new Match(cloudFileList);
	}

	public List<String> getMatches() {
		return match.getMatches();
	}
	
    public void doStart() throws Exception {
		while(true) {
			File file = (File) queue.getQueue().poll(5, TimeUnit.SECONDS);
			if (file != null) {
				execute(new FileHandler(watcher.getNotifier(), cloud, cloudFileList, file, match));
			}
			if (queue.isClosed() && queue.getQueue().size() == 0)
				break;
		}
    }
}
