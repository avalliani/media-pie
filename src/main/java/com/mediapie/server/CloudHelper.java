package com.mediapie.server;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import java.io.*;
import java.util.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import org.apache.commons.lang3.exception.*;

public class CloudHelper  {
	public static Properties getProperties() {
		
		Configuration config = Ebean.find(Configuration.class, "main");
		
		Properties props = new Properties();

		props.setProperty(com.mediapie.cloud.s3.S3CloudSetup.ACCESS_KEY,setEmptyIfNull(config.getAccessKey()));
		props.setProperty(com.mediapie.cloud.s3.S3CloudSetup.SECRET_KEY,setEmptyIfNull(config.getSecretKey()));		
		props.setProperty(com.mediapie.cloud.s3.S3CloudSetup.ENCRYPTION_KEY,setEmptyIfNull(config.getEncryptionKey()));
		props.setProperty(com.mediapie.cloud.s3.S3CloudSetup.BUCKET, setEmptyIfNull(config.getGuid()));

		return props;
	}

	static String setEmptyIfNull(String value) {
		if (value == null) return "";

		return value;
	}

	public static int exactEncryptionBits() {
		return CloudFactory.newInstance().exactEncryptionBits();
	}
	
	public static Cloud getInstance() {
		return CloudFactory.newInstance(getProperties());
	}

	public static void checkConnection() throws CloudException {
		CloudFactory.newInstance().checkConnection(CloudFactory.getCloudSetup(getProperties()));
	}
}