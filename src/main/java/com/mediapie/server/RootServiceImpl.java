package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;
import javax.jws.*;

@WebService(endpointInterface = "com.mediapie.server.RootService",
            serviceName = "RootService")
public class RootServiceImpl implements RootService {
	static org.apache.commons.logging.Log log = Logger.getLog(RootServiceImpl.class);
	
	@WebMethod()
	public void startUpload() {
		if (log.isDebugEnabled())
			log.debug("Service RootService.startUpload begin call");

		try {
			Process.startUpload();

			if (log.isDebugEnabled())
				log.debug("Service RootService.startUpload end call");
			
		} catch(Exception e) {
			log.error(e);			
			throw new MediaPieException(e);
		}				
	}

	@WebMethod()
	public void startDownload() {
		
		if (log.isDebugEnabled())
			log.debug("Service RootService.startDownload begin call");
		
		try {
			Process.startDownload();

			if (log.isDebugEnabled())
				log.debug("Service RootService.startDownload end call");			
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}				
	}

	@WebMethod()
	public Boolean isRunning()  {
		if (log.isDebugEnabled())
			log.debug("Service RootService.isRunning begin call");
		
		try {
			Boolean ret =  Process.isRunning();

			if (log.isDebugEnabled())
				log.debug("Service RootService.isRunning end call");

			return ret;
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}				
	}

	@WebMethod()
	public void stopProcess()  {
		if (log.isDebugEnabled())
			log.debug("Service RootService.stopProcess begin call");
		
		try {
			Process.stop();

			if (log.isDebugEnabled())
				log.debug("Service RootService.stopProcess end call");
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}				
	}
	
	@WebMethod()
	public Configuration getConfiguration()  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.getConfiguration begin call");
			
			String guid = Persistance.getGuidOrGenerateNew();
			Configuration config = Ebean.find(Configuration.class, "main");

			if (config != null &&
				  (config.getEncryptionKey() == null || "".equals(config.getEncryptionKey().trim()))) {
				//generate a secure key
				String key = EncryptionUtils.generateSecureKey(CloudHelper.exactEncryptionBits());
				config.setEncryptionKey(key);
				Ebean.save(config);			
			}

			if (log.isDebugEnabled())
				log.debug("Service RootService.getConfiguration end call");
			
			return config;
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}
	}

	@WebMethod()
	public void saveConfiguration(String accessKey,
								  String secretKey,
								  String encryptionkey,
								  String bucketName) {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.saveConfiguration begin call");
			
			String guid = Persistance.getGuidOrGenerateNew();
			Persistance.saveKeys(accessKey,
								 secretKey,
								 encryptionkey,
								 bucketName);

			if (log.isDebugEnabled())
				log.debug("Service RootService.saveConfiguration end call");
			
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}		
	}

	@WebMethod()
	public void saveRestoreDirectories(String targetDirectory, String sourceDirectory)  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.saveRestoreDirectories begin call");
			
			Configuration config = getConfiguration();

			if (config != null) {
				config.setTargetDirectory(targetDirectory);
				config.setSourceDirectory(sourceDirectory);
				Ebean.save(config);
				
				if (log.isDebugEnabled())
					log.debug("Service RootService.saveRestoreDirectories end call");
			}			
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}		
	}
	
	@WebMethod()
	public void checkConnection()  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.checkConnection begin call");
			
			CloudHelper.checkConnection();

			if (log.isDebugEnabled())
				log.debug("Service RootService.checkConnection end call");
			
		} catch(Exception ce) {
			log.error(ce);
			throw new MediaPieException(ce);
		}
	}

	@WebMethod()
	public List<String> getCloudDirectories(int level)  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.getCloudDirectories begin call");

			Cloud cloud = CloudHelper.getInstance();
			CloudFileList cloudFileList = cloud.getDirectories(level, null);
			List<String> directories = cloudFileList.getDirectories();

			if (log.isDebugEnabled())
				log.debug("Service RootService.getCloudDirectories end call");
			
			return directories;
		} catch(Exception ce) {
			log.error(ce);
			throw new MediaPieException(ce);
		}
	}

	@WebMethod()
	public List<Directory> getBackupDirectories()  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.getBackupDirectories begin call");
			
			List<Directory> dirs = Ebean.find(Directory.class).findList();

			if (log.isDebugEnabled())
				log.debug("Service RootService.getBackupDirectories end call");
			
			return dirs;
		} catch(Exception ce) {
			log.error(ce);
			throw new MediaPieException(ce);
		}
	}

	@WebMethod()
	public void saveBackupDirectories(String[] dirs, String[] cloudDirs)  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.saveBackupDirectories begin call");
			
			Persistance.saveDirectories(dirs, cloudDirs);

			if (log.isDebugEnabled())
				log.debug("Service RootService.saveBackupDirectories end call");
			
		} catch(Exception ce) {
			log.error(ce);
			throw new MediaPieException(ce);
		}
	}

	@WebMethod()
	public ProgressStatus getProgress()  {
		try {
			if (log.isDebugEnabled())
				log.debug("Service RootService.getProgress begin call");
			
			ProgressStatus status = ProgressManager.get();

			if (log.isDebugEnabled())
				log.debug("Service RootService.getProgress end call");
			
			return status;
		} catch(Exception ce) {
			log.error(ce);
			throw new MediaPieException(ce);
		}
	}

	@WebMethod()
	public boolean checkValidFolder(String folder) {
		if (log.isDebugEnabled())
			log.debug("Service RootService.checkValidFolder begin call");

		try {
			boolean ret = FileUtils.folderExists(folder);

			if (log.isDebugEnabled())
				log.debug("Service RootService.checkValidFolder end call");
			
			return ret;
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}				
	}

	@WebMethod()
	public boolean validEncryptionKey(String key)  {
		if (log.isDebugEnabled())
			log.debug("Service RootService.validEncryptionKey begin call");

		boolean ret = false;

		try {
			ret = EncryptionUtils.validEncryptionKey(key, CloudHelper.exactEncryptionBits());
		} catch(java.security.NoSuchAlgorithmException na) {
			log.error(na);
			throw new MediaPieException(na);
		}

		if (log.isDebugEnabled())
			log.debug("Service RootService.validEncryptionKey end call");

		return ret;
	}

	@WebMethod()
	public String generateEncryptionKey() {
		if (log.isDebugEnabled())
			log.debug("Service RootService.generateEncryptionKey begin call");
		
		String ret = null;

		try {
			ret = EncryptionUtils.generateSecureKey(CloudHelper.exactEncryptionBits());
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}

		if (log.isDebugEnabled())
			log.debug("Service RootService.generateEncryptionKey end call");

		return ret;		
	}	
	
	@WebMethod()
	public boolean login(String username, String password) {
		if (log.isDebugEnabled())
			log.debug("Service RootService.login begin call");

		boolean ret = false;
		
		try {
			ret = UserManagerFactory.getInstance().authenticate(username, password);
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}

		if (log.isDebugEnabled())
			log.debug("Service RootService.login end call");

		return ret;		
	}

	@WebMethod()
	public boolean changePassword(String username,
								  String oldPassword,
							      String newPassword) {
		if (log.isDebugEnabled())
			log.debug("Service RootService.changePassword begin call");

		try {
			UserManager userManager = UserManagerFactory.getInstance();
			boolean valid = userManager.authenticate(username, oldPassword);

			//authenticate first
			if (!valid)
				return false;
			
			//expire in memory caches
			SqlTokenValidator.expireCache();
			userManager.resetPassword(username, oldPassword, newPassword);
			
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}
		
		if (log.isDebugEnabled())
			log.debug("Service RootService.changePassword end call");

		return true;
	}

	@WebMethod()
	public List<MediaFile> getFoldersRoots() {
		if (log.isDebugEnabled())
			log.debug("Service RootService.getFoldersRoots begin call");
		List<MediaFile> list = new ArrayList<MediaFile>();
		try {
			list = MediaUtils.convertFiles(FileUtils.getRootFolders(), true);			
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}

		if (log.isDebugEnabled())
			log.debug("Service RootService.getFoldersRoots end call");
		
		return list;
	}

	@WebMethod()
	public List<MediaFile> getFolders(String path) {
		if (log.isDebugEnabled())
			log.debug("Service RootService.getFolders begin call for path='" + path + "'");

		List<MediaFile> list = new ArrayList<MediaFile>();

		try {
			list = MediaUtils.convertFiles(FileUtils.getFolders(path), false);
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}

		if (log.isDebugEnabled())
			log.debug("Service RootService.getFolders end call");

		return list;
	}

	@WebMethod()
	public void saveScheduleFrequency(Integer frequency) {
		if (log.isDebugEnabled())
			log.debug("Service RootService.saveScheduleFrequency begin call for frequency='" + frequency + "'");

		try {
			Configuration config = getConfiguration();
			config.setFrequency(frequency);
			Ebean.save(config);

			//update the scheduler
			ProcessScheduler.startScheduler();
			
		} catch(Exception e) {
			log.error(e);
			throw new MediaPieException(e);
		}

		if (log.isDebugEnabled())
			log.debug("Service RootService.saveScheduleFrequency end call");
	}
}

