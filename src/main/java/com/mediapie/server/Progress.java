package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class Progress {
	private static Double percentage;
	private static String fileName;
	private static CircularFifoQueue logQueue = new CircularFifoQueue(4);
	private static StringWriter logger = new StringWriter();
	private static Integer totalFileCount;
	private static Integer currentFileCount = 0;
	private static Integer errorFileCount = 0;
	private static Notifier notifier = new Notifier();
	private static boolean finished = false;
	private static boolean error = false;
	private static Throwable exception;
	static org.apache.commons.logging.Log log = Logger.getLog(Progress.class);
	
	public static void setFinished(boolean f) { finished = f;}
	public static void setTotalFileCount(int count) {totalFileCount = count;}
	public static void setCurrentFileCount(int count) {currentFileCount = count;}
	public static void setPercentage(double percent) {percentage = percent;}
	public static Boolean isError() { return error;}
	public static String getExceptionStack() {
		if (exception != null)
			return org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(exception);
		
		return null;
	}

	public static String getExceptionMessage() {
		if (exception != null)
			return exception.getMessage();

		return null;
	}
	
	public static void error(Throwable t) {
		error = true;
		finished = true;
		exception = t;
		notifier.doNotify();		
	}
	
	public static void setFileName(String name) {
		fileName = name;
		notifier.doNotify();		
	}

	public static void setNotifier(Notifier n) {notifier = n;}

	public static void reset() {
		//ProgressManager.reset();
		
		percentage = null;
		fileName = null;
		logger = new StringWriter();
		totalFileCount = null;
		currentFileCount = 0;
		finished = false;
		error = false;
		exception = null;
		
		notifier.doNotify();
	}
	
	public static Double getPercentage() {
		if (totalFileCount == null)
			return 0.0;
		
		if (totalFileCount != 0) {
			percentage = MathUtils.round(currentFileCount*100.0/totalFileCount, 0);
		} else
			percentage = 100.0;
		
		return percentage;
	}
	
	public static boolean isFinished() { return finished; }
	public static String getFileName() {
		return fileName;
	}

	public static String getLog() {
		Iterator iter = logQueue.iterator();
		StringBuffer buff = new StringBuffer();
		while(iter.hasNext()) {
			Object val = null;				
			try {
				val = iter.next();
			} catch(Exception e) {}

		if (val != null)
			buff.append(val.toString() + System.getProperty("line.separator"));
		}

		return buff.toString();
	}
	
	public static Integer getTotalFileCount() { return totalFileCount;}
	public static Integer getCurrentFileCount() { return currentFileCount;}
	public static Notifier getNotifier() { return notifier;}

	public static void markFinish() {
		finished = true;
		notifier.doNotify();
	}
	
	public static void incrementFileCount() {
		currentFileCount = currentFileCount + 1;
		notifier.doNotify();
	}

	public static void incrementErrorCount() {
		currentFileCount = currentFileCount + 1;
		errorFileCount = errorFileCount + 1;
		notifier.doNotify();
	}
	
	public static void writeLog(String logString) {
		Date dateTime = new Date();
		String threadName = Thread.currentThread().getName();
		String message = dateTime.toString() + " " + threadName + "-" + logString;

		synchronized(logQueue) {
			logQueue.add(message);
		}

		if (log.isInfoEnabled())
			log.info(message);
		
		//synchronized(logger) {
		//	logger.write(message + System.getProperty("line.separator"));
		//}
		notifier.doNotify();
	}
	public static Integer getErrorFileCount() {return errorFileCount;}
	public static void setErrorFileCount(Integer arg) {errorFileCount = arg;}
}

