package com.mediapie.server;

import java.io.*;
import java.util.*;
import com.mediapie.util.*;

public class FileListReader extends Thread {

    FileQueue queue;
	List<CloudDirectoryAndFilter> directories;
	StopSignal stopSignal = new StopSignal();
	boolean started = false;
	
    public FileListReader(List<CloudDirectoryAndFilter> directories, FileQueue queue) {
		super("FileListing-Thread");
		this.directories = directories;
		this.queue = queue;
    }

	public void stopReader() {
		stopSignal.stop();
	}
	
	public void run() {
		Progress.writeLog("Getting list of files to process.");
		FileUtils.listFiles(directories, queue, stopSignal, true);
		Progress.setTotalFileCount(queue.getFileList().size());
		queue.closeQueue();
    }
}