package com.mediapie.server;

import java.io.*;
import com.mediapie.util.*;

public class CloudDirectoryAndFilter extends DirectoryAndFilter{
	String cloudPath;
	
	public CloudDirectoryAndFilter() {}

	public CloudDirectoryAndFilter(File directory, FileFilter filter, String cloudPath) {
		setDirectory(directory);
		setFilter(filter);
		setCloudPath(cloudPath);
	}
	public void setCloudPath(String path) {this.cloudPath = path;}
	public String getCloudPath() { return cloudPath; }
}