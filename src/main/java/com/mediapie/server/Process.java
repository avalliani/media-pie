package com.mediapie.server;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import java.io.*;
import java.util.*;
import com.mediapie.util.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import org.apache.commons.lang3.exception.*;

public class Process  {
	static boolean running = false;
	static UploadClient uploadClient = null;
	static RestoreClient downloadClient = null;
	static org.apache.commons.logging.Log log = Logger.getLog(Process.class);
								 
	public static boolean isRunning() { return running;}

	public static void stop() {
		if (running) {
			if (uploadClient != null) {
				uploadClient.stop();
			}

			if (downloadClient != null) {
				downloadClient.stop();
			}
		}
	}
	
	public static void startDownload() {
		Thread thread = new Thread ("Process Thread") {
			public void run() {
				ProgressThread progress = null;
				
				try {
					progress = new ProgressThread();
					progress.start();

					RootService service = new RootServiceImpl();
					Configuration config = service.getConfiguration();
					String targetDirectory = config.getTargetDirectory();
					String sourceDirectory = config.getSourceDirectory();

					Properties props = CloudHelper.getProperties();

					downloadClient = new RestoreClient(
						10,
						new File(targetDirectory),
						sourceDirectory,
						props);
					
					downloadClient.run();
				} catch(Exception e) {
					log.error(e);
					Progress.writeLog(ExceptionUtils.getStackTrace(e));
				} finally {
					running = false;
					Progress.markFinish();
					progress.end();
				}
			}			
		};
		
		running = true;		
		thread.start();
	}

	public static void startUpload() {
		Thread thread = new Thread ("Process Thread") {
			public void run() {
				ProgressThread progress = null;

				try {
					progress = new ProgressThread();
					progress.start();

					RootService service = new RootServiceImpl();
					
					List<Directory> dirs = service.getBackupDirectories();
					List<CloudDirectoryAndFilter> directories = new ArrayList<CloudDirectoryAndFilter>();

					for(Directory dir : dirs) {
						if (dir != null && dir.getName() != null && !"".equals(dir.getName().trim())) {
							
							if (log.isDebugEnabled())
								log.debug("Adding directory and filter:" + dir.getName());
							
							directories.add(new CloudDirectoryAndFilter(new File(dir.getName()), null, dir.getCloudName()));
						}
					}

					Properties props = CloudHelper.getProperties();

					uploadClient = new UploadClient(10, directories, props);
					uploadClient.run();
				} catch(Exception e) {
					Progress.writeLog(ExceptionUtils.getStackTrace(e));
					log.error(e);
				} finally {
					running = false;
					Progress.markFinish();
					progress.end();
				}
			}			
		};

		running = true;		
		thread.start();
	}	
	
}