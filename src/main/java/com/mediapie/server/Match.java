package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class Match {
	CloudFileList cloudFiles;
	Map<String, CloudFile> cloudFileSet = new HashMap<String, CloudFile>();
	List<String> fileMatches = new ArrayList<String>();
	
	public Match(CloudFileList cloudFiles) {
		this.cloudFiles = cloudFiles;
		for (CloudFile cloudFile : cloudFiles.getFiles()) {
			cloudFileSet.put(cloudFile.getFilePath(), cloudFile); 
		}
	}

	public List<String> getMatches() {
		return fileMatches;
	}
	
	public Operation getOperation(File localFile) {
		String path = localFile.getAbsolutePath();
		String normalizedPath = FilePaths.normalize(path);
		if (localFile.isDirectory()) {
			if (!normalizedPath.endsWith("/"))
				normalizedPath = normalizedPath + "/";
		}
		
		CloudFile cloudFile = cloudFileSet.get(normalizedPath);

		if (cloudFile == null) {
			return Operation.ADD;
		} else {
			synchronized(fileMatches) {
				fileMatches.add(normalizedPath);
			}
			
			long localFileModified = localFile.lastModified();
			long localFileSize = localFile.length();

			long cloudFileModified = cloudFile.getLastModified().getTime();
			long cloudFileSize = cloudFile.getSize();

			if (localFileModified > cloudFileModified)
				return Operation.UPDATE;
			else
				return Operation.NOTHING;
		}
	}
}

