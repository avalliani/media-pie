package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class PoolWatcher extends Thread {
	Pool pool;
	PoolNotifier notifier;
	boolean terminating = false;
	
	public PoolWatcher() {
		super();
	}

	public void setPool(Pool pool) {
		this.pool = pool;
	}

	public PoolNotifier getNotifier() {
		return notifier;
	}
	
	public void setNotifier(PoolNotifier notifier) {
		this.notifier = notifier;
	}
	
	public void run() {
		while (true) {
			synchronized(notifier) {
				try {
					notifier.wait();
					Throwable t = notifier.getThrowable();

					if (!terminating) {
						new Thread(new Runnable() {
							public void run() {
								pool.shutdownNow();
							}
						}).start();

						//update status with error
						Progress.error(t);
					}
					
					terminating = true;					
				} catch(InterruptedException ie) {
					break;
				}
			}
		}
	}

}

