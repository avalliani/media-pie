package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class DownloadPool extends Pool {

	Cloud cloud;
	String sourceDirectory;
	File directory;
	CloudFileList cloudFileList;
	
	public DownloadPool(PoolWatcher watcher,
						Cloud cloud,
						CloudFileList cloudFileList,
						String sourceDirectory,
						File directory,
						int threads) {

		super(threads, watcher);
		
		this.cloud = cloud;
		this.cloudFileList = cloudFileList;
		this.sourceDirectory = sourceDirectory;
		this.directory = directory;
	}

	
	public void doStart() throws Exception {
		List<CloudFile> files = cloudFileList.filterByDirectory(sourceDirectory);
		Progress.setTotalFileCount(files.size());

		for (CloudFile file : files) {
			execute(new CloudDownloadFileHandler(watcher.getNotifier(), cloud, file, directory));			
		}
    }
}

