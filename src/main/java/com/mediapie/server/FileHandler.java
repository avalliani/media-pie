package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class FileHandler implements Runnable {
	File file;
	Cloud cloud;
	CloudFileList cloudFileList;
	Match match;
	PoolNotifier notifier;
	static org.apache.commons.logging.Log log = Logger.getLog(FileHandler.class);

	public FileHandler(PoolNotifier notifier,
					   Cloud cloud,
					   CloudFileList cloudFileList,
					   File file,
					   Match match) {
		
		this.file = file;
		this.cloud = cloud;
		this.cloudFileList = cloudFileList;
		this.match = match;
		this.notifier = notifier;		
	}

	public void run() {
		try {
			Operation op = match.getOperation(file);
			
			switch(op) {
				case ADD:
				{
					boolean isDirectory = file.isDirectory();
					Progress.setFileName(file.getAbsolutePath());
					Progress.writeLog("Adding file:" + file.getAbsolutePath());
					if (isDirectory) {
						if (log.isDebugEnabled())
							log.debug("Creating directory:" + file.getAbsolutePath());
						cloud.createDirectory(file);
					}
					else
						cloud.upload(file);
					Progress.incrementFileCount();

					if (log.isDebugEnabled())
						log.debug(Thread.currentThread().getName() +
									   "-Operation:" + op +
									   " - Processing file " + !isDirectory + " " + file.getAbsolutePath());				
					break;
				}
				case UPDATE:
				{
					Progress.setFileName(file.getAbsolutePath());
					Progress.writeLog("Updating file:" + file.getAbsolutePath());
					if (!file.isDirectory())
						cloud.upload(file);					
					Progress.incrementFileCount();

					if (log.isDebugEnabled())
						log.debug(Thread.currentThread().getName() +
									   "-Operation:" + op +
									   " - Processing file" + file.getAbsolutePath());				
					break;
				}
				case NOTHING:
				{
					Progress.setFileName(file.getAbsolutePath());
					Progress.writeLog("File unchanged:" + file.getAbsolutePath());
					Progress.incrementFileCount();					
				}
			}
		} catch(CloudException ce) {
			//it must not be a system specific issue but
			//a file specific issue. 
			if (!ce.isAborted())
				log.error(ce);
			
			if (ce.isFileSpecific()) {
				Progress.setFileName(file.getAbsolutePath());
				Progress.writeLog("File upload/download error:" + ce.getMessage());
				Progress.incrementErrorCount();
			} else {
				if (ce.isAborted()) {
					Progress.writeLog("File upload aborted:" + ce.getMessage());					
				}
				notifier.notifyThrowable(ce);
			}
		} catch(Throwable ce) {
			log.error(ce);
			notifier.notifyThrowable(ce);
		}
	}
}
