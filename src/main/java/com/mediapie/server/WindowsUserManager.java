package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;
import waffle.windows.auth.IWindowsIdentity;

public class WindowsUserManager implements UserManager {	
	public void resetPassword(String username, String oldPassword, String newPassword) {
		
	}

	public boolean authenticate(String username, String password) {
		WindowsAuthProviderImpl auth = new WindowsAuthProviderImpl();
		try {
			IWindowsIdentity identity = auth.logonUser(username, password);
		} catch(com.sun.jna.platform.win32.Win32Exception e) {
			return false;
		}

		return true;
	}
}

