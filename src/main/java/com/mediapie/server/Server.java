package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import javax.xml.ws.Endpoint;
import java.net.URL;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import org.apache.cxf.jaxws.*;
import org.apache.cxf.interceptor.*;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.bus.spring.SpringBusFactory;

public class Server {
	static org.apache.commons.logging.Log log = Logger.getLog(Server.class);
	public static int port = 9000;

	public static int getPort() {
		return port;
	}
	
	public Server() {
		if (log.isInfoEnabled())
			log.info("Starting Server");

		SpringBusFactory bf = new SpringBusFactory();
		URL busFile = Server.class.getResource("/server-config.xml");
		Bus bus = bf.createBus(busFile.toString());
		BusFactory.setDefaultBus(bus);
		
		//RootServiceImpl implementor = new RootServiceImpl();
		//String address = "https://localhost:" + port + "/RootService";
		//Endpoint.publish(address, implementor);		
	}
}

