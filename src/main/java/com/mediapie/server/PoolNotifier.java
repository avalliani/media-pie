package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class PoolNotifier {
	Throwable t;
	
	public PoolNotifier() {
	}

	public Throwable getThrowable() {
		return t;
	}

	public void notifyThrowable(Throwable t) {
		synchronized(this) {
			this.t = t;
			notify();
		}
	}
}

