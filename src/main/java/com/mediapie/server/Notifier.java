package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;

public class Notifier {
	static ProgressStatus status;
	
	public void doWait() throws InterruptedException{
		synchronized(this) {
			wait();
		}
	}

	public void doNotify() {
		synchronized(this) {
			/*
			if (status == null) {
				status = ProgressManager.get();
			}

			status.setPercentage(Progress.getPercentage());
			status.setFileName(Progress.getFileName());
			status.setLog(Progress.getLog());
			status.setTotalFileCount(Progress.getTotalFileCount());
			status.setCurrentFileCount(Progress.getCurrentFileCount());
			status.setFinished(Progress.isFinished());
			status.setError(Progress.isError());			
			status.setErrorStack(Progress.getExceptionStack());
			
			ProgressManager.save(status);
			*/
			notify();
		}
	}
}

