package com.mediapie.server;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.wss4j.common.ext.*;
//import org.apache.ws.security.WSPasswordCallback;
import com.mediapie.util.*;
import com.mediapie.*;

public class ServerPasswordCallback implements CallbackHandler {

	static org.apache.commons.logging.Log log = Logger.getLog(ServerPasswordCallback.class);
	
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {

		
		if (log.isDebugEnabled())
			log.debug("Received authentication request begin:" + callbacks.length);
		
		WSPasswordCallback pc = (WSPasswordCallback) callbacks[0];

		String username = pc.getIdentifier();
		String password = pc.getPassword();
		
		if (log.isDebugEnabled())
			log.debug("Server Auth: '" + pc.getIdentifier() + "' password:'" + password + "'");		

		//make the password the same to pass the same password test of
		//the validator
		pc.setPassword(password); 
	}
}