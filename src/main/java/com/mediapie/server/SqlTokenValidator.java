package com.mediapie.server;

import org.apache.commons.codec.binary.Base64;
import org.apache.wss4j.binding.wss10.AttributedString;
import org.apache.wss4j.binding.wss10.EncodedString;
import org.apache.wss4j.binding.wss10.PasswordString;
import org.apache.wss4j.binding.wss10.UsernameTokenType;
import org.apache.wss4j.binding.wsu10.AttributedDateTime;
import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.apache.wss4j.common.ext.WSSecurityException;
import org.apache.wss4j.stax.ext.WSSConstants;
import org.apache.wss4j.stax.ext.WSSUtils;
import org.apache.wss4j.stax.securityToken.UsernameSecurityToken;
import org.apache.wss4j.stax.securityToken.WSSecurityTokenConstants;
import org.apache.wss4j.stax.impl.securityToken.UsernameSecurityTokenImpl;
import org.apache.xml.security.stax.ext.XMLSecurityUtils;
import org.apache.xml.security.stax.securityToken.InboundSecurityToken;
import org.apache.wss4j.stax.validate.UsernameTokenValidatorImpl;
import org.apache.wss4j.stax.validate.TokenContext;
import com.mediapie.util.*;
import java.util.Map;
import java.util.HashMap;

public class SqlTokenValidator extends UsernameTokenValidatorImpl {
    
	static org.apache.commons.logging.Log log = Logger.getLog(SqlTokenValidator.class);
	static Map<String,String> cache = new HashMap<String,String>();
	static Long lastUpdated;
	static final long expiration = 5*60*100;
	
	/**
     * Verify a UsernameToken containing a plaintext password.
     */
	@Override
	protected void verifyPlaintextPassword(String username,
										   PasswordString passwordType,
										   TokenContext tokenContext
										  ) throws WSSecurityException {

		try {
			String password = passwordType.getValue();

			if (log.isDebugEnabled())
				log.debug("Custom validation of username='" + username + " and password='" + password + "'");

			if (existsPasswordPair(username, password)) {
				if (log.isDebugEnabled())
					log.debug("User '" + username + " found in cache. Auth successful");
				return;
			}
			else {
				boolean auth = false;

				try {
					if (log.isDebugEnabled())
						log.debug("User '" + username + " starting authentication");

					auth = UserManagerFactory.getInstance().authenticate(username, password);

					if (log.isDebugEnabled())
						log.debug("User '" + username + " authentication=" +  auth);

				} catch(Exception e) {
					log.error("Exception in authentication:", e);
					throw new WSSecurityException(WSSecurityException.ErrorCode.FAILED_AUTHENTICATION);		
				}

				if (!auth)
					throw new WSSecurityException(WSSecurityException.ErrorCode.FAILED_AUTHENTICATION);
				else {
					synchronized(cache) {
						cache.put(username, password);
						lastUpdated = System.currentTimeMillis();
					}
				}
			}
		} catch(Exception e) {
			log.error("Unknown exception in Security Module", e);
			throw new WSSecurityException(WSSecurityException.ErrorCode.FAILED_AUTHENTICATION);
		}
	}
	
	public static void expireCache() {
		synchronized(cache) {
			cache.clear();
		}
	}
	
	static boolean existsPasswordPair(final String username, final String password) {
		synchronized(cache) {
			String passwordCache = cache.get(username);
			return (withinExpiration() && passwordCache != null && passwordCache.equals(password));
		}
	}
	
	static boolean withinExpiration() {
		if (lastUpdated == null || (System.currentTimeMillis() - lastUpdated) > expiration) {
			return false;
		}

		return true;
	}
}
