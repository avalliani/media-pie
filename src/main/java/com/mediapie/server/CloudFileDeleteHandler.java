package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class CloudFileDeleteHandler implements Runnable {
	CloudFile file;
	Cloud cloud;
	PoolNotifier notifier;
	static org.apache.commons.logging.Log log = Logger.getLog(CloudFileDeleteHandler.class);
	
	public CloudFileDeleteHandler(PoolNotifier notifier,
								  Cloud cloud,
								  CloudFile file) {
		
		this.file = file;
		this.cloud = cloud;
		this.notifier = notifier;
	}

	public void run() {
		try {
			//delete files from cloud
			Progress.writeLog("Removing cloud file:" + file.getFilePath());

			if (file.isDirectory()) {
				if (log.isDebugEnabled())
					log.debug(Thread.currentThread().getName() +
								   "- Delete directory:" +
								   " - Processing file" + file.getFilePath());
				
				cloud.deleteDirectory(file);
			} else {
				if (log.isDebugEnabled())
					log.debug(Thread.currentThread().getName() +
								   "-Delete file:" +
								   " - Processing file" + file.getFilePath());
				
				cloud.deleteFile(file);
			}			
		} catch(CloudException ce) {
			//it must not be a system specific issue but
			//a file specific issue. 
			log.error(ce);

			if (ce.isFileSpecific()) {
				Progress.writeLog("Error deleting file file:" + file.getFilePath() + " " + ce.getMessage());
			} else {
				notifier.notifyThrowable(ce);
			}
		} catch(Throwable t) {
			log.error(t);
			notifier.notifyThrowable(t);
		}			
	}
}
