package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;

public class UserManagerFactory {
	static UserManager instance = new DatabaseUserManager();
	
	public static UserManager getInstance() {
		return instance;
	}

	public static void setInstance(UserManager _instance) {
		instance = _instance;
	}
}

