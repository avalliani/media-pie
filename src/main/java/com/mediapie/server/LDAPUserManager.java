package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;

public class LDAPUserManager implements UserManager {
	static org.apache.commons.logging.Log log = Logger.getLog(LDAPUserManager.class);

	static final String HOST = "localhost";
	
	public void resetPassword(String username, String oldPassword, String newPassword)  {
		try
		{
			String hashedPassword = EncryptionUtils.convertLDAPSHA(newPassword);
			String hashedPasswordSamba = EncryptionUtils.convertLDAPMD4(newPassword);
			
			String DN = "uid=" + username + ",ou=Users,dc=mediapie, dc=lan";
			String CN = "cn=" + username + ",ou=Users,dc=mediapie, dc=lan";
			String url = "ldap://" + HOST;

			// Set up the environment for creating the initial context
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, url);

			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, DN); 
			env.put(Context.SECURITY_CREDENTIALS, oldPassword);

			LdapContext ctx =new InitialLdapContext(env,null);
			ModificationItem[] mods = new ModificationItem[2];
			byte[] newUnicodePassword = hashedPassword.getBytes("UTF-16LE");

			mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
										   new BasicAttribute("userPassword", hashedPassword));
			mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
										   new BasicAttribute("sambaNTPassword", hashedPasswordSamba));

			// Perform the update
			ctx.modifyAttributes(DN, mods);
			if (log.isDebugEnabled())
				log.debug("Changed Password for successfully");			
		}
		catch (Exception e)
		{
			if (log.isInfoEnabled())
				log.info("Failed password change:", e);
			throw new MediaPieException(e);
		}				
	}

	public boolean authenticate(String username, String password) {
		try
		{
			String DN = "uid=" + username + ",ou=Users,dc=mediapie, dc=lan";
			
			// Set up the environment for creating the initial context
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, "ldap://" + HOST);
			
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.SECURITY_PRINCIPAL, DN); 
			env.put(Context.SECURITY_CREDENTIALS, password);

			// Create the initial context
			DirContext ctx = new InitialDirContext(env);
			boolean result = ctx != null;

			if(ctx != null)
				ctx.close();

			return result;
		}
		catch (Exception e)
		{
			if (log.isInfoEnabled())
				log.info("Failed authentication:", e);
			return false;
		}		
	}
}

