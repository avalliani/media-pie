package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class DeletePool extends Pool {

	LinkedBlockingQueue<CloudFile> queue = null;
	Cloud cloud;
	
	public DeletePool(PoolWatcher watcher,
					  Cloud cloud,
					  LinkedBlockingQueue<CloudFile> queue,
					  int threads) {

		super(threads, watcher);
		this.queue = queue;
		this.cloud = cloud;
	}
	
	public void doStart() throws Exception {
		while(true) {
			CloudFile file = (CloudFile) queue.poll(5, TimeUnit.SECONDS);
			if (file != null) {
				execute(new CloudFileDeleteHandler(watcher.getNotifier(), cloud,file));
			} else
				break;			
		}
    }
}

