package com.mediapie.server;

import java.io.*;
import java.util.*;
import com.mediapie.model.*;
import com.mediapie.util.*;
import org.joda.time.*;

public class ProcessScheduler extends TimerTask {
	static org.apache.commons.logging.Log log = Logger.getLog(ProcessScheduler.class);

	static Timer timer = new Timer();
		
	public void run() {
		RootService service = new RootServiceImpl();
		try {
			if (!service.isRunning()) {
				if (log.isInfoEnabled())
					log.info("Scheduling process to run");
				
				service.startUpload();				
			} else {
				if (log.isInfoEnabled())
					log.info("Process is already running. Not scheduling a run.");
			}
		} catch(Exception e) {
			log.error("Error scheduling process:", e);
		}
	}
	
	public static void cancelScheduler() {
		timer.cancel();
	}
	
	public static void startScheduler() {
		//cancel if timer is running and create a newone
		try {
			timer.cancel();
		} catch(Exception e) {
			log.info("Cancel time exception:", e);
		}
		
		timer = new Timer();
		
		RootService service = new RootServiceImpl();
		Configuration config = service.getConfiguration();

		if (config == null || config.getFrequency() == null)
			return;			

		final Schedule schedule = Schedule.byValue(config.getFrequency());
		
		DateTime nowTime = DateTime.now();
		DateTime scheduledTime = nowTime;
		int intervalHours = 0;
		
		switch(schedule) {
			case NEVER:
			{
				break;
			}
			
			case RUN_ONCE:
			{
				//do nothing
				break;
			}

			case EVERY_ONE_HOUR:
			{
				intervalHours = 1;
				break;
			}

			case EVERY_MORNING:
			{
				scheduledTime = nowTime.withHourOfDay(9).withMinuteOfHour(0).withSecondOfMinute(0);
				if (scheduledTime.getMillis() < nowTime.getMillis())
					scheduledTime = scheduledTime.plusDays(1);					
				intervalHours = 24;
				break;
			}

			case EVERY_MIDNIGHT:
			{
				scheduledTime = nowTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(0);
				intervalHours = 24;
				break;
			}
		}

		// Schedule to run every Sunday in midnight

		if (log.isInfoEnabled())
			log.info("Scheduler to be run at the frequency of :" + schedule.name());
		
		if (schedule == Schedule.NEVER) {
			return;
		}
		else if (schedule == Schedule.RUN_ONCE) {
			timer.schedule(new ProcessScheduler(),
						   scheduledTime.getMillis()
						  );		
		} else {
			timer.scheduleAtFixedRate(new ProcessScheduler(),
									  scheduledTime.getMillis(),
									  intervalHours * 60 * 60 * 1000
									 );		
		}	 			
	}
}

