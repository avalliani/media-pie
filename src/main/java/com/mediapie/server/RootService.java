package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;
import javax.jws.*;

@WebService
public interface RootService {
	public void startUpload();

	public void startDownload();

	public Boolean isRunning();

	public void stopProcess();
	
	public Configuration getConfiguration();

	public void saveConfiguration(@WebParam(name="accessKey") String accessKey,
								  @WebParam(name="secretKey") String secretKey,
								  @WebParam(name="encryptionkey") String encryptionkey,
								  @WebParam(name="bucketName") String bucketName);

	public void saveRestoreDirectories(@WebParam(name="targetDirectory") String targetDirectory,
									   @WebParam(name="sourceDirectory") String sourceDirectory);
	
	public void checkConnection();

	public List<String> getCloudDirectories(@WebParam(name="level") int level);

	public List<Directory> getBackupDirectories();

	public void saveBackupDirectories(@WebParam(name="dirs") String[] dirs, @WebParam(name="cloudDirs") String[] cloudDirs);
	
	public ProgressStatus getProgress();

	public boolean checkValidFolder(@WebParam(name="folder") String folder);

	public boolean validEncryptionKey(@WebParam(name="key") String key);

	public String generateEncryptionKey();	

	public boolean login(@WebParam(name="username") String username, @WebParam(name="password") String password);

	public boolean changePassword(@WebParam(name="username") String username,
								  @WebParam(name="oldPassword") String oldPassword,
								  @WebParam(name="newPassword") String newPassword);

	public List<MediaFile> getFoldersRoots();

	public List<MediaFile> getFolders(@WebParam(name="path") String path);

	public void saveScheduleFrequency(@WebParam(name="frequency") Integer frequency);
	
}

