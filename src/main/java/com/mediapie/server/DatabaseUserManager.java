package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;

public class DatabaseUserManager implements UserManager {	
	public void save(User user) {
		user.setPassword(EncryptionUtils.bcrypt(user.getPassword()));
		
		Persistance.saveUser(user);
	}

	public void resetPassword(String username, String oldPassword, String newPassword)  {
		String passwordHash = EncryptionUtils.bcrypt(newPassword);
		User user = getUser(username); 
		Persistance.resetUserPassword(user, passwordHash);
	}

	User getUser(String userId) {
		return Persistance.getUser(userId);
	}
	
	public boolean authenticate(String userId, String password) {		
		User user = Persistance.getUser(userId);

		if (user == null) return false;

		return EncryptionUtils.bcryptCheck(password, user.getPassword());
	}
}

