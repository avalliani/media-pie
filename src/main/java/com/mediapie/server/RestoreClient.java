package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class RestoreClient implements ProcessClient {
	
	int threads = 10;
	File directory;
	String sourceDirectory;
	Properties cloudProperties;
	CloudLogger cloudLogger = null;
	DownloadPool downloadPool = null;
	static org.apache.commons.logging.Log log = Logger.getLog(RestoreClient.class);
	
	public RestoreClient(final int threads,
						 final File directory,
						 final String sourceDirectory,
						 final Properties cloudProperties) {

		this.threads = threads;
		this.directory = directory;
		this.cloudProperties = cloudProperties;
		this.sourceDirectory = sourceDirectory;

		cloudLogger = new CloudLogger() {
			public void log(String category, String log) {										
				if ("file".equals(category))
					Progress.setFileName("Cloud file:" + log );
				else
					Progress.writeLog(log);										
			}
		};		
    }

	public void stop() {
		cloudLogger.stop();

		try {
			if (downloadPool != null)
				downloadPool.shutdown();
		} catch(Exception e) {
			log.error(e);
		}					
	}

	public void run() {
		Progress.reset();		
		
		//read the files on the cloud
		Cloud cloud = CloudFactory.newInstance(cloudProperties);
		CloudFileList cloudFileList = cloud.getFileList(
			(new CloudFileListIdentifier()).withPrefix(sourceDirectory),
			cloudLogger);
		
		//start uploads
		downloadPool = new DownloadPool(new PoolWatcher(), cloud, cloudFileList, sourceDirectory, directory, threads);
		downloadPool.start();
	}
}

