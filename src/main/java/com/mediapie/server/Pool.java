package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public abstract class Pool {

    protected ExecutorService pool = null;
	protected int threads;
	protected PoolWatcher watcher;
	static org.apache.commons.logging.Log log = Logger.getLog(Pool.class);
	
	public Pool(int threads, PoolWatcher watcher) {
		this.threads = threads;
		this.watcher = watcher;
	}

	public abstract void doStart() throws Exception;

	public void execute(Runnable runnable) {
		pool.execute(runnable);
	}
	
    public void start() {
		pool = Executors.newFixedThreadPool(threads);
		
		if (watcher != null) {
			PoolNotifier notifier = new PoolNotifier();
			watcher.setPool(this);
			watcher.setNotifier(notifier);
			watcher.setName(this.getClass().getName());
			watcher.start();
		}
		
		try {
			doStart();
			
			shutDownAndAwaitPoolTasksToFinish();
		} catch (Exception ex) {
			shutdown();;
		}
    }

    public void shutDownAndAwaitPoolTasksToFinish() {
		pool.shutdown();
		try {
			while(true) {
				if (pool.awaitTermination(60, TimeUnit.SECONDS)) {
					break;
				}
			}
		} catch(InterruptedException ie) {
			log.error("Thread interrupted:", ie);
		}
    }

	public void shutdownNow() {
		pool.shutdownNow();
	}
	
    public void shutdown() {
		pool.shutdown(); // Disable new tasks from being submitted
		try {
			// Wait a while for existing tasks to terminate
			if (!pool.awaitTermination(10, TimeUnit.SECONDS)) {
				pool.shutdownNow(); // Cancel currently executing tasks
				// Wait a while for tasks to respond to being cancelled
				if (!pool.awaitTermination(60, TimeUnit.SECONDS))
					log.error("Pool did not terminate");
			}
		} catch (InterruptedException ie) {
			// (Re-)Cancel if current thread also interrupted
			pool.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
    }    
}

