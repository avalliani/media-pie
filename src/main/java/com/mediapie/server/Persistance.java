package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
import com.mediapie.model.*;
import com.mediapie.launcher.Mode;
import java.sql.*;
import com.avaje.ebean.*;
import com.avaje.ebean.config.*;
import org.flywaydb.core.Flyway;

public class Persistance {
	static Connection conn = null;
	static org.apache.commons.logging.Log log = Logger.getLog(Persistance.class);
	static boolean configured = false;
	static String dbNamePrefix = "mediapie";
	static String dbName = null;
	static String dbUrl = null;
	static String dbUsername = "sa";
	static String dbPassword = "";
	
	public static void startServer(Mode mode ) {
		org.hsqldb.Server hsqlServer = null;
		Connection conn = null;
		
		try {
			String homeDirectory = FileUtils.getHomeDirectory();
			String suffix = "";
			
			if (mode == mode.SERVER)
				suffix = "_server";
			else
				suffix = "_client";

			dbName = dbNamePrefix + suffix;
			dbUrl = "jdbc:hsqldb:file:" + homeDirectory + File.separator + dbName + File.separator + dbName;
			/*
			dbUrl = "jdbc:hsqldb:hsql://localhost/" + dbName;
			
			hsqlServer = new org.hsqldb.Server();

			hsqlServer.setLogWriter(null);
			hsqlServer.setSilent(true);

			String file = "file:" + homeDirectory + File.separator + dbName + File.separator + dbName;
			
			hsqlServer.setDatabaseName(0, dbName);
			hsqlServer.setDatabasePath(0, file);

			hsqlServer.start();
			*/
			checkConnection(dbUrl, dbUsername, dbPassword);
		} catch(Exception e) {
			log.error("Error starting database", e);
			throw new MediaPieException(e);
		}			
	}

	public static boolean checkConnection(String url, String username, String password) {
		Connection conn = null;

		try {
			conn = DriverManager.getConnection(url, username, password);
			
			return true;
		} catch(SQLException sqe) {
			log.error("Coud not connect to database", sqe);
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch(SQLException se) {
				log.error("Error closing connection:", se);
			}
		}

		return false;
	}
	
	public static synchronized void configureEbean(Mode mode) {
		if (configured) return;

		startServer(mode);
				
		ServerConfig config = new ServerConfig();  
		config.setName("media-pie");  

		// Define DataSource parameters  
		DataSourceConfig db = new DataSourceConfig();  
		db.setDriver("org.hsqldb.jdbc.JDBCDriver");  
		db.setUsername(dbUsername);  
		db.setPassword(dbPassword);  
		db.setUrl(dbUrl);  
		
		config.setDataSourceConfig(db);  

		// set DDL options...  
		//config.setDdlGenerate(true);  
		//config.setDdlRun(true);
		config.addClass(com.mediapie.model.Configuration.class);
		config.addClass(com.mediapie.model.Directory.class);
		config.addClass(com.mediapie.model.ProgressStatus.class);
		config.addClass(com.mediapie.model.User.class);
		
		config.setDefaultServer(true);  
		config.setRegister(true);
		if (log.isDebugEnabled())
			config.setDebugSql(true);
		
		// create the EbeanServer instance  
		EbeanServer server = EbeanServerFactory.create(config);

		//now upgrade database
		Flyway flyway = new Flyway();
		flyway.setDataSource(dbUrl, dbUsername, dbPassword);
		flyway.setInitOnMigrate(true);
		flyway.migrate();

		configured = true;
	}

	/*
	public static String getUrl() {
		Connection conn = null;
		String url = null;
		String dbname = "mediapie";
		
		//test lock.. if locked, then try another url for connection
		try {
			String homeDirectory = FileUtils.getHomeDirectory();
			String dbFile = "mediapie";
			url = "jdbc:hsqldb:file:" + homeDirectory + File.separator + dbname + File.separator + dbFile;
			conn = DriverManager.getConnection(url, "sa", "");
		} catch(SQLException  he) {
			int errorCode = he.getErrorCode();

			log.error("Error with HQLDB url:" + url + " with error code:" + errorCode);
			
			//lock acquisition failure
			if (errorCode == -451) {
				url = "jdbc:hsqldb:hsql://localhost/" + dbname;
				try {
					conn = DriverManager.getConnection(url, "sa", "");
				} catch(SQLException se) {
					throw new MediaPieException("Error in connecting to database using url:" + url, se);
				}
			} else {
				throw new MediaPieException("Error in connecting to database with error code:" + errorCode, he);
			}
		} finally {
			try {
				if (conn != null)
					conn.close();
			} catch(SQLException e) {
				log.error("Error closing test connection", e);
				throw new MediaPieException(e);
			}
		}

		return url;
	}
	*/
	
	public static String getGuidOrGenerateNew() {
		final EbeanUtils.TransactionResult guid = new EbeanUtils.TransactionResult();

		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				Configuration config = Ebean.find(Configuration.class, "main");
				
				if (config == null) {
					String hostName = NetworkUtils.getHostName();
					String guid = GuidUtils.create();
					if (hostName != null) hostName = hostName.toLowerCase();
					config = new Configuration();
					config.setId("main");
					config.setGuid(((hostName != null) ? hostName + "-" + guid: guid));
					Ebean.save(config);
				}

				guid.value = config.getGuid();				
			}
		});

		if (guid.value != null) {
			return (String) guid.value;
		}

		return null;
	}

	public static void saveKeys(final String accessKey,
								final String secretKey,
								final String encryptionKey,
								final String bucketName) {
		
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				Configuration config = Ebean.find(Configuration.class, "main");

				if (config == null) {
					config = new Configuration();
					config.setId("main");
					config.setGuid(bucketName);
				}
				config.setAccessKey(accessKey);
				config.setSecretKey(secretKey);
				config.setEncryptionKey(encryptionKey);
				config.setGuid(bucketName);

				Ebean.save(config);
			}
		});
	}
	
	public static void saveKeys(final String accessKey, final String secretKey, final String encryptionKey) {
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				Configuration config = Ebean.find(Configuration.class, "main");

				if (config == null) {
					config = new Configuration();
					config.setId("main");
					config.setGuid(GuidUtils.create());
				}
				config.setAccessKey(accessKey);
				config.setSecretKey(secretKey);
				config.setEncryptionKey(encryptionKey);

				Ebean.save(config);
			}
		});
	}
	
	public static void saveDirectories(final String[] directories, final String[] cloudDirectories) {
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				List<Directory> dirs = Ebean.find(Directory.class).findList();

				if (dirs == null || dirs.size() == 0) {
					dirs = new ArrayList<Directory>();
					for (int i=0; i < directories.length; i++) {
						Directory dir = new Directory();
						dir.setId(i+1);
						dir.setName(directories[i]);
						if (cloudDirectories != null && cloudDirectories.length > i) {
							dir.setCloudName(cloudDirectories[i]);
						}
						dirs.add(dir);
					}
				} else {
					for (Directory dir : dirs) {
						int id = dir.getId().intValue();
						if (directories.length >= (id + 1)) {
							dir.setName(directories[id -1]);
							if (cloudDirectories != null && cloudDirectories.length >= id) {
								dir.setCloudName(cloudDirectories[id - 1]);
							}						
						}
					}
				}

				Ebean.save(dirs);
			}
		});
	}

	public static void saveProgress(final ProgressStatus status) {
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				Ebean.save(status);
			}
		});
	}

	public static void resetProgress() {
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				ProgressStatus status = Ebean.find(ProgressStatus.class, "main");

				if (status != null) {
					status.setId("main");
					status.setPercentage(0.0);
					status.setFileName(null);
					status.setTotalFileCount(null);
					status.setCurrentFileCount(null);
					status.setFinished(null);
				} else {
					status.setId("main");
				}

				Ebean.save(status);
			}
		});
	}

	public static ProgressStatus getProgress() {
		final EbeanUtils.TransactionResult result = new EbeanUtils.TransactionResult();
		
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				ProgressStatus status = Ebean.find(ProgressStatus.class, "main");

				if (status == null) {
					status = new ProgressStatus();
					status.setId("main");
					Ebean.save(status);
				}
				
				result.value = status;
			}
		});

		if (result.value != null) {
			return (ProgressStatus) result.value;
		}

		return null;		
	}


	public static void saveUser(final User user) {
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				Ebean.save(user);
			}
		});
	}

	public static void resetUserPassword(final User user, final String password) {
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				user.setPassword(password);
				Ebean.save(user);
			}
		});
	}	


	public static User getUser(final String userId) {
		final EbeanUtils.TransactionResult result = new EbeanUtils.TransactionResult();
		EbeanUtils.wrapTransaction(TxIsolation.READ_COMMITED, new TransactionWrap() {
			public void run(Transaction t) {
				User user = Ebean.find(User.class)
							  .where()
							  .eq("id", userId)
							.findUnique();
				result.value = user;
			}
		});
		
		return (User) result.value;
	}	
	
	
}

