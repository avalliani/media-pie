package com.mediapie.server;

import java.io.*;
import java.util.concurrent.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;

public class CloudDownloadFileHandler implements Runnable {
	CloudFile file;
	Cloud cloud;
	File directory;
	PoolNotifier notifier;
	static org.apache.commons.logging.Log log = Logger.getLog(CloudDownloadFileHandler.class);
	
	public CloudDownloadFileHandler(PoolNotifier notifier,
									Cloud cloud,
									CloudFile cloudFile,
								    File directory) {
		
		this.file = cloudFile;
		this.cloud = cloud;
		this.directory = directory;
		this.notifier = notifier;
	}

	public void run() {
		String targetFileName = FilePaths.replaceFilePath(
			file.getFilePath(),
			directory.getAbsolutePath());
		
		try {			

			Progress.writeLog("Downloading file to:" + targetFileName);
			
			FileUtils.createDirectoryFile(targetFileName);
			
			Progress.setFileName(targetFileName);

			if (!file.isDirectory())
				cloud.download(file, new File(targetFileName));

			Progress.incrementFileCount();
		} catch(CloudException ce) {
			//it must not be a system specific issue but
			//a file specific issue. 
			if (!ce.isAborted())
				log.error(ce);

			if (ce.isFileSpecific()) {
				Progress.writeLog("Error with file:" + targetFileName + " " + ce.getMessage());
				Progress.incrementErrorCount();
			} else {
				if (ce.isAborted()) {
					Progress.writeLog("File download aborted:" + targetFileName + "->" + ce.getMessage());
				}
				notifier.notifyThrowable(ce);
			}
		} catch(Throwable t) {
			log.error(t);
			notifier.notifyThrowable(t);
		}
	}
}
