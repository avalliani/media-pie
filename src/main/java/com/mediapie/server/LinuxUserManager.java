package com.mediapie.server;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import com.mediapie.*;
import com.mediapie.util.*;
import com.mediapie.cloud.*;
import com.mediapie.model.*;
//import net.sf.jpam.*;
import org.jvnet.libpam.PAM;
import org.jvnet.libpam.UnixUser;
import org.jvnet.libpam.PAMException;

public class LinuxUserManager implements UserManager {	
	public void resetPassword(String username, String oldPassword, String newPassword) {
		try {
			UnixUtils.changePassword(username, newPassword);
		} catch(IOException ie) {
			throw new MediaPieException(ie);
		}
	}

	public boolean authenticate(String username, String password) {
		UnixUser user = null;
		try {
			user = new PAM("pam service").authenticate(username,password);
			return true;
		} catch (PAMException ex){
			return false;
		}
		/*
		Pam pam = new Pam();
		boolean authenticated = pam.authenticateSuccessful(username, password);
		return authenticated;
*/
	}
}

