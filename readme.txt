INSTRUCTIONS:
==================================================================
1. SETUP:
   Please download maven to get the dependencies

2. TO COMPILE:
   mvn compile

3. TO RUN:
   mvn compile exec:java or 
   scripts/run_client.bat or scripts/run_server.bat

4. TO CREATE INSTALLER:

   scripts/install-win.bat
   
   EXTERNAL SOFTWARE:
   - please make sure to download and include paths to 
     WIX (http://wix.codeplex.com/releases/view/115492) and 
     INNO (http://www.jrsoftware.org/isdl.php)
   
   Please don't add them to a folder with spaces like "program files". Keep the folders simple.

   Add the following environment variables and set them to the folders created above:

   %WIX_HOME%
   %INNO_HOME%
